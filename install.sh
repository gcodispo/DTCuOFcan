#!/bin/sh

BASE=${PWD}



cat <<< "
#!/bin/sh

export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\${IOCANPATH}/lib/
export PATH=\${PATH}:\${IOCANPATH}/bin/
export TWO_TASK=cms_omds_lb

" > dtcanenv.sh

cat <<< "
#!/bin/sh

IOCANPATH=${BASE}
export LD_LIBRARY_PATH=\${LD_LIBRARY_PATH}:\${IOCANPATH}/lib/
export PATH=\${PATH}:\${IOCANPATH}/bin/
export TWO_TASK=cms_omds_lb

\${IOCANPATH}/bin/iocan.exe \${@}

" > iocan

cp iocan bin/
chmod +x iocan
chmod +x bin/iocan
chmod +x dtcanenv.sh


