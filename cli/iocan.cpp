#include <stdio.h>
#include "Ctcpux.h"
#include <iostream>

#include "CuOFCommand.h"
#include "CuOFThreadCommand.h"


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h> 
#include <dlfcn.h>
#include <algorithm>

const std::string canConfFile("/nfshome0/dtdqm/DTCuOFcan/config/iocanconf.txt");
const std::string boardConfFile("/nfshome0/dtdqm/DTCuOFcan/config/iocanboards.txt");
const std::string adcConfFile("/nfshome0/dtdqm/DTCuOFcan/config/iocanmap.txt");
const std::string dbConf("CUOF_START");
//const std::string dbConf("CUOF_BASE");


//////////////////////////////////
/// CLI oriented functions
//////////////////////////////////


void help( const std::string & progname )
{
  std::cout << std::endl << "Usage : " << progname << " [OPTIONS]... [COMMAND]"
	    << std::endl << std::endl;
  std::cout << "List of mandatory COMMAND:" << std::endl;

  std::string board_opt = "";
  std::string onet_opt = "{ [MEZZANINE] { [ONET] } }";
  std::string wr_onet_opt = "{ [MEZZANINE] { [ONET] } } [MOD] [BIAS]";
  std::string adc_opt = "{ [ADC] { [CHAN] } }";
  std::string actel_opt = "[EXE] [ACTION] [DAT_FILE]";
  std::string silent_opt = "[read / enable / disable]";
  std::string timer_opt = "[ / VALUE ]";
  std::string reg_opt = "[ REG ]";
 
  std::map<std::string, std::string> commands;
  commands["--actel"] = actel_opt;
  commands["--adc"] = onet_opt;
  commands["--adcconf"] = adc_opt;
  commands["--onet"] = onet_opt;
  commands["--writeonet"] = wr_onet_opt;
  commands["--configureOnets"] = wr_onet_opt;

  commands["--compareramtoflash"] = board_opt;
  commands["--flashtoram"] = board_opt;
  commands["--help"] = board_opt;
  commands["--killMaster"] = board_opt;
  commands["--ramblockatstartup"] = board_opt;
  commands["--ramtoflash"] = board_opt;
  commands["--ramtoonet"] = board_opt;
  commands["--restoreFromFlash"] = board_opt;
  commands["--readId"] = board_opt;
  commands["--reset"] = board_opt;
  commands["--restoreMaster"] = board_opt;
  commands["--silent"] = silent_opt;
  commands["--statusBackup"] = board_opt;
  commands["--statusMaster"] = board_opt;
  commands["--timer"] = timer_opt;
  commands["--version"] = board_opt;
  commands["--readCanRegisters"] = board_opt;
  commands["--readCanCounters"] = board_opt;
  commands["--resetCanCounters"] = board_opt;
  commands["--diffOnetsWithDB"] = board_opt;
  commands["--configOnetsFromDB"] = board_opt;
  commands["--dumpOnetsFromDB"] = board_opt;
  commands["--onetsFromRamToDB"] = timer_opt;

  std::map<std::string, std::string>::const_iterator it = commands.begin();
  std::map<std::string, std::string>::const_iterator itend = commands.end();
  for ( ; it != itend; ++it ) {
    std::cout << "  " << it->first << "  " << it->second << std::endl;
  }

  std::cout << std::endl
	    << "  Arguments inside {} are optional.\n"
	    << "    Valid [MEZZANINE] from 0 to 3\n"
	    << "    Valid [ONET] from 0 to 7\n"
	    << "    Valid [ADC] from 0 to 7\n"
	    << "    Valid [CHAN] from 0 to 11"
	    << "\n\n"
	    << "List of OPTION:\n"
	    << "  --crates [list of crates]\n"
	    << "  --wheels [list of wheels]\n"
	    << "  --board [boardname/ro/trg/all : reduce to a single board or the readout or trigger only subset]\n"
	    << "  --db read configuration parameters from db [DEFAULT]\n"
	    << "  --files read configuration parameters from files\n"
	    << "  --2nd : force access throug secondary bus and slave FPGA; note that adc and onet are accessible through secondary only after a killMaster\n\n"
	    << "More info : https://twiki.cern.ch/twiki/bin/view/CMS/DTCuOFcan_v1"
	    << std::endl << std::endl;
}




//////////////////////////////////
/// MAIN
//////////////////////////////////
int main(int argc, char **argv)
{

  int start = getExeTime();

  std::cout << "\n? **************************************"
	    << "******************************** ?\n"
	    << "? *******  *******  *******   YES, "
	    << " IOCAN!!!   *******  *******   ****** ?\n"
	    << "? *******  *******               **"
	    << "*****                *******   ****** ?\n"
	    << "? ******   "
    "https://twiki.cern.ch/twiki/bin/view/CMS/DTCuOFcan_v1"
	    << "   ***** ?\n"
	    << "? **************************************"
	    << "******************************** ?\n\n";
  if ( argc == 1 ) {
    printf( "ERROR : unknown argument, possible commands are:\n" );
    help( argv[0] );
    return -1;
  }


 
  std::vector<std::string> goodcommands;
  goodcommands.push_back("--compareramtoflash");
  goodcommands.push_back("--flashtoram");
  goodcommands.push_back("--help");
  goodcommands.push_back("--killMaster");
  goodcommands.push_back("--ramblockatstartup");
  goodcommands.push_back("--ramtoflash");
  goodcommands.push_back("--ramtoonet");
  goodcommands.push_back("--restoreFromFlash");
  goodcommands.push_back("--readId");
  goodcommands.push_back("--reset");
  goodcommands.push_back("--restoreMaster");
  goodcommands.push_back("--silent");
  goodcommands.push_back("--statusBackup");
  goodcommands.push_back("--statusMaster");
  goodcommands.push_back("--timer");
  goodcommands.push_back("--version");
  goodcommands.push_back("--readCanRegisters");
  goodcommands.push_back("--readCanCounters");
  goodcommands.push_back("--resetCanCounters");
  goodcommands.push_back("--configOnetsFromDB");
  goodcommands.push_back("--dumpOnetsFromDB");
  goodcommands.push_back("--onetsFromRamToDB");
  goodcommands.push_back("--diffOnetsWithDB");
  goodcommands.push_back("--cmd");
  goodcommands.push_back("--panic");

  std::vector<std::string> threadcommands;
  threadcommands.push_back("--writeonet");
  threadcommands.push_back("--onet");
  threadcommands.push_back("--configureOnets");
  threadcommands.push_back("--adcconf");
  threadcommands.push_back("--adc");
  threadcommands.push_back("--actel");

  std::vector<std::string> goodoptions;
  goodoptions.push_back("--wheels");
  goodoptions.push_back("--sector");
  goodoptions.push_back("--crates");
  goodoptions.push_back("--board");
  goodoptions.push_back("--2nd");
  goodoptions.push_back("--db");
  goodoptions.push_back("--files");

  std::map< std::string, std::vector<std::string> > commands;

  /// this is for options
  std::vector<std::string> args(argc-1, "");
  for ( int i = 1; i < argc; ++i ) {

    /// this is command
    std::string loc_arg = argv[i];
    commands[ loc_arg ];

    // this collects command options
    while ( ++i < argc ) {
      std::string arg = argv[i];
      if ( arg.substr( 0, 2 ) == "--" ) break;
      commands[ loc_arg ].push_back( argv[i] );
    }
    --i;
  }


  /// basic variables to be filled in order to execute
  std::vector<std::string> arguments;
  std::vector<std::string> options;
  std::string command;
  std::string board;
  dtcan::TYPE type = dtcan::ANY;
  dtcan::SELECTION selection = dtcan::ALL;
  bool usethreads = true;
  bool force2nd = false;
  bool useDb = true;
  bool useFiles = false;

  /// counters
  size_t ncoms = 0;
  size_t nthcoms = 0;
  size_t nsel = 0;
  size_t ntype = 0;
  // short sector= -1;
  /// filter commands
  std::map< std::string, std::vector<std::string> >::const_iterator it = commands.begin();
  std::map< std::string, std::vector<std::string> >::const_iterator itend = commands.end();
  for ( ; it != itend; ++it ) {

    if ( find( goodcommands.begin(), goodcommands.end(), it->first ) != goodcommands.end() ) {
      ++ncoms;
      command = it->first;
      arguments = it->second;
    } else if ( find( threadcommands.begin(), threadcommands.end(), it->first ) != threadcommands.end() ) {
      ++nthcoms;
      command = it->first;
      arguments = it->second;
    } else if ( it->first == "--crates" ) {
      ++nsel;
      selection = dtcan::CRATE;
      options = it->second;
    } else if ( it->first == "--wheels" ) {
      ++nsel;
      selection = dtcan::WHEEL;
      options = it->second;
//     } else if ( it->first == "--sector" ) {
//       if ( it->second.size() == 1 ) {
// 	sector = dtcan::convert( it->second.at(0) );
//       } else {
// 	std::cout << "ERROR : bad value for --sector option specified."
// 		  << std::endl;
// 	return -1;
//       }
    } else if ( it->first == "--board" ) {
      ++ntype;
      if ( it->second.size() == 1 ) {
	std::string val = it->second.at(0);
	if ( val == "ro" ) type = dtcan::RO;
	else if ( val == "trg" ) type = dtcan::TRG;
	else if ( val == "all" ) type = dtcan::ANY;
	else {
	  board = val;
	  usethreads = false;;
	}
      } else {
	  std::cout << "ERROR : too many values for --board option specified."
		    << "\nUse BOARDNAME or keyword all/ro/trg" << std::endl;
	  return -1;
      }
    } else if ( it->first == "--2nd" ) {
      force2nd = true;
    } else if ( it->first == "--db" ) {
      useDb = true;
    } else if ( it->first == "--files" ) {
      useFiles = true;
    } else {
      std::cout << "Unknown argument `" << it->first  << '`'
		<< std::endl << std::endl;
      return -2;
    }
  }

  if ( useDb && useFiles ) {
    std::cout << "WARNING: requested configuration from both DB and files.\n"
	      << "Using DB." << std::endl;
  }

  ///////////////////////////////////////////////////////////
  /// exactly one command requested!
  if ( ncoms + nthcoms > 1 ) {
    std::cout << "Too many commands issued" << std::endl << std::endl;
    return -3;
  } else if ( ncoms + nthcoms < 1 )  {
    std::cout << "No command issued!" << std::endl << std::endl;
    return -4;
  }

  ///////////////////////////////////////////////////////////
  /// Use only one between --crates and --wheels
  if ( nsel > 1 ) {
    std::cout << "Use only one between --crates and --wheels" << std::endl;
    return -5;
  } else if ( nsel == 1 && !board.empty() ) {
    std::cout << "bad arguments: the requested command " << command
	      << " cannot combine options --crate/--wheel with --board argument.\n\n";
    return -6;
  }

    ///////////////////////////////////////////////////////////
  /// Use only one between --crates and --wheels
  if ( !board.empty() && type != dtcan::ANY ) {
    std::cout << "Cannot combine board name in --board with --crates/--wheels."
	      << "Use `--board all/ro/trg` with --crates/--wheels."
	      << std::endl;
    return -7;
  }

  ///////////////////////////////////////////////////////////
  /// issue the proper command
  if ( nthcoms && usethreads ) {

    ///////////////////////////////////////////////////////////
    /// this is for complex commands in threaded format
    dtcan::CuOFThreadCommand cmd;
    if ( useDb ) cmd.configureFromDB( dbConf );
    else cmd.configure( canConfFile, boardConfFile, adcConfFile );
    
    cmd.applySelection( selection, options, force2nd );

    if ( command == "--actel" ) {
      cmd.actelCLI( type, arguments );
    } else if ( command == "--adc" ) {
      cmd.adcCLI( type );
    } else if ( command == "--adcconf" ) {
      cmd.adcconfCLI( type );
    } else if ( command == "--onet" ) {
      cmd.onetCLI( type );
    } else if ( command == "--configureOnets" ) {
      cmd.writeOnetCLI( type, arguments, true );
    } else if ( command == "--writeonet" ) {
      cmd.writeOnetCLI( type, arguments );
    }

  } else if ( nthcoms ) {

    ///////////////////////////////////////////////////////////
    /// this is for complex commands in non threaded format
    dtcan::CuOFCommand cmd;
    if ( useDb ) cmd.configureFromDB( dbConf );
    else cmd.configure( canConfFile, boardConfFile, adcConfFile );

    cmd.applySelection( selection, options, force2nd );

    if ( command == "--actel" ) {
      cmd.actelCLI( board, arguments, type );
    } else if ( command == "--adc" ) {
      cmd.queryCLI( board, type, arguments, false );
    } else if ( command == "--adcconf" ) {
      cmd.queryCLI( board, type, arguments, false, true );
    } else if ( command == "--onet" ) {
      cmd.queryCLI( board, type, arguments, true );
    } else if ( command == "--configureOnets" ) {
      cmd.writeOnetCLI( board, type, arguments, true );
    } else if ( command == "--writeonet" ) {
      cmd.writeOnetCLI( board, type, arguments );
    }

  } else if ( ncoms ) {

    ///////////////////////////////////////////////////////////
    /// this is for non threaded commands accepting just board
    if ( board == "all" ) board = "";
    dtcan::CuOFCommand cmd;
    if ( useDb ) cmd.configureFromDB( dbConf );
    else cmd.configure( canConfFile, boardConfFile, adcConfFile );
    cmd.applySelection( selection, options, force2nd );

    if ( command == "--cmd" ) {
      cmd.cmd( board, arguments, type );
    } else if ( command == "--panic" ) {
      cmd.panic( selection, options, arguments );
    } else if ( command == "--compareramtoflash" ) {
      cmd.compareRamToFlash( board, type );
    } else if ( command == "--flashtoram" ) {
      cmd.writeFlashToRam( board, type );
    } else if ( command == "--help" ) {
      help( argv[0] );
    } else if ( command == "--killMaster" ) {
      cmd.killMaster( board, type );
    } else if ( command == "--ramblockatstartup" ) {
      cmd.readRamBlockAtStartup( board, type );
    } else if ( command == "--ramtoflash" ) {
      cmd.writeRamToFlash( board, type );
    } else if ( command == "--ramtoonet" ) {
      cmd.writeRamToOnet( board, type );
    } else if ( command == "--restoreFromFlash" ) {
      cmd.restoreFromFlash( board, type );
    } else if ( command == "--dumpOnetsFromDB" ) {
      cmd.dumpOnetsFromDB();
    } else if ( command == "--configOnetsFromDB" ) {
      cmd.queryCLI( board, type, arguments, true, true );
    } else if ( command == "--diffOnetsWithDB" ) {
      cmd.diffOnetsWithDB( board, type );
    } else if ( command == "--onetsFromRamToDB" ) {
      cmd.onetsFromRamToDB( arguments );
    } else if ( command == "--readId" ) {
      cmd.readId( board, type );
    } else if ( command == "--reset" ) {
      cmd.reset( board, type );
    } else if ( command == "--silent" ) {
      cmd.silent( board, arguments, type );
    } else if ( command == "--timer" ) {
      cmd.timer( board, arguments, type );
    } else if ( command == "--readCanRegisters" ) {
      cmd.readCanRegisters( board, type );
    } else if ( command == "--readCanCounters" ) {
      cmd.readCanCounters( board, type );
    } else if ( command == "--resetCanCounters" ) {
      cmd.resetCanCounters( board, type );
    } else if ( command == "--restoreMaster" ) {
      cmd.restoreMaster( board, type );
    } else if ( command == "--statusBackup" ) {
      cmd.statusBackup( board, type );
    } else if ( command == "--statusMaster" ) {
      cmd.statusMaster( board, type );
    } else if ( command == "--version" ) {
      cmd.version( board, type );
    }
    //  else if ( command == "--writeId" ) {
    //  cmd.writeId( board );
    //  }
  }

  // } catch ( std::exception & e ) {
  //   std::cout <<  e.what() << std::endl;
  // }

  int stop = getExeTime();
  std::cout << std::endl
	    << "elapsed time "  << stop - start << "ms" << std::endl;
 
}
