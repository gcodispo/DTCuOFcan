#include <iostream>

#include "CuOFCan.h"
#include "CuOFCommand.h"
#include "CuOFThreadCommand.h"

const std::string canConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanconf.txt");
const std::string boardConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanboards.txt");
const std::string adcConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanmap.txt");

int main ()
{

  dtcan::CuOFCan can_( canConfFile, boardConfFile, adcConfFile );

//   std::cout << can_["YBm1_TRG9-10"].readRamBlockAtStartup() << std::endl;
//   std::cout << can_["YBm1_TRG9"].readRamBlockAtStartup() << std::endl;
//   can_["YBm1_TRG9-10"].setRamBlockAtStartup( 0 );
//   can_["YBm1_TRG9-10"].writeRamToOnet();
//   can_["YBm1_TRG9-10"].writeRamToOnet(0 );

  char mezz = 3;
  std::string board = "YBm1_TRG9-10";
  dtcan::onet params[dtcan::CuOFBoard::nonets];

//   can_[board].readMezzanineOnets(mezz, params);
//   for ( size_t i = 0; i < dtcan::CuOFBoard::nonets; ++i ) {
//     if ( can_[board].isActiveMezzanine( params[i] ) )
//       std::cout << board << '\t' << params[i];
//   }
//   can_[board].writeOnetParamsToRam(params, dtcan::CuOFBoard::nonets);

  std::cout << "WRITE DIRECT" << std::endl;
  char mod = 110;
  char bias = 25;
  for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
    size_t idx = j;
    params[idx].mezzanine = mezz;
    params[idx].nonet = j;
    params[idx].control = 0xd2;
    params[idx].modulation = mod;
    params[idx].bias = bias;
    params[idx].equalizer = 0x0;
  }
  can_[board].writeOnetDirect(params, dtcan::CuOFBoard::nonets);

  dtcan::onet reply[dtcan::CuOFBoard::nonets];
  for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
    size_t idx = mezz * 8 + j;
    reply[idx].mezzanine = mezz;
    reply[idx].nonet = j;
    reply[idx].control = 0xff;
    reply[idx].modulation = 0xff;
    reply[idx].bias = 0xff;
    reply[idx].equalizer = 0xff;
  }

  can_[board].readMezzanineOnets(mezz, reply);
  for ( size_t i = 0; i < dtcan::CuOFBoard::nonets; ++i ) {
    if ( can_[board].isActiveMezzanine( reply[i] ) )
      std::cout << board << '\t' << reply[i];
  }
//   can_["YBm1_TRG9-10"].readRamBlockAtStartup();

}
