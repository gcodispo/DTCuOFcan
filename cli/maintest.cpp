#include "CuOFThreadCommand.h"

const std::string canConfFile("/nfshome0/gcodispo/TestCan/iocanconf.txt");
const std::string boardConfFile("/nfshome0/gcodispo/TestCan/iocanboards.txt");
const std::string adcConfFile("/nfshome0/gcodispo/TestCan/iocanmap.txt");

void usage( const char * pname )
{
  std::cout << "Bad usage!\n"
	    << "\t./" << pname << "adc all\n"
	    << "\t./" << pname << "adc alltrg\n"
	    << "\t./" << pname << "adc all\n"
	    << "\t./" << pname << "onet allro\n"
	    << "\t./" << pname << "onet alltrg\n"
	    << "\t./" << pname << "onet allro\n"
	    << "\t./" << pname << "adcconf all\n"
	    << "\t./" << pname << "adcconf alltrg\n"
	    << "\t./" << pname << "adcconf allro\n"
	    << std::endl;
}


int main(int argc, char **argv)
{

  if ( argc != 3 ) {
    usage( argv[0] );
    return -1;
  }

  std::string act( argv[1] );
  std::string opt( argv[2] );

  dtcan::CuOFThreadCommand cmd( canConfFile, boardConfFile, adcConfFile );

  if ( act == "adc" ) {
    if ( opt == "all" ) {
      cmd.readAllAdc();
      cmd.printAdcMap();
    } else if ( opt == "alltrg" ) {
      cmd.readAllTrgAdc();
      cmd.printAdcMap( true );
    } else if ( opt == "allro" ) {
      cmd.readAllRoAdc();
      cmd.printAdcMap( false );
    } else {
      usage( argv[0] );
      return -1;
    }

  } else if ( act == "onet" ) {
    if ( opt == "all" ) {
      cmd.readAllOnets();
      cmd.printOnetMap();
    } else if ( opt == "alltrg" ) {
      cmd.readAllTrgOnets();
      cmd.printOnetMap( true );
    } else if ( opt == "allro" ) {
      cmd.readAllRoOnets();
      cmd.printOnetMap( false );
    } else {
      usage( argv[0] );
      return -1;
    }

  } else if ( act == "adcconf" ) {
    if ( opt == "all" ) {
      cmd.readAdcConf();
      cmd.printAdcConfigMap();
    } else if ( opt == "alltrg" ) {
      cmd.readAdcTrgConf();
      cmd.printAdcConfigMap( true );
    } else if ( opt == "allro" ) {
      cmd.readAdcRoConf();
      cmd.printAdcConfigMap( false );
    } else {
      usage( argv[0] );
      return -1;
    }
  } else {
    usage( argv[0] );
    return -1;
  }


}
