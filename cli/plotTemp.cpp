#include <stdio.h>
#include "Ctcpux.h"
#include <iostream>

#include "CuOFCan.h"
#include "CuOFBoard.h"
#include "canerrors.h"


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h> 

#include "TH1I.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"

const std::string canConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanconf.txt");
const std::string boardConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanboards.txt");
const std::string adcConfFile("/nfshome0/dtdqm/DTCuOFcan/iocanmap.txt");


/// read all ADC
int main()
{
  dtcan::CuOFCan my( canConfFile, boardConfFile, adcConfFile );

  std::map< std::string, dtcan::CuOFBoard >::iterator it = my.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = my.end();

  TH1I * tempI = new TH1I( "T_counts", "T_counts", 34, 110, 144 );
  TH1F * tempF = new TH1F( "T_val", "T_val", 40, 20, 60 );
  TH2F * tempS = new TH2F( "T_boards", "T_boards", 25, 110, 125, 40, 20, 60 );

  TH1F * tempF_1 = new TH1F( "T_val_1", "T_val 2.048", 100, -50, 50 );
  TH1F * tempF_2 = new TH1F( "T_val_2", "T_val 4.096", 100, 50, 150 );

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;

    dtcan::adconet reply[dtcan::CuOFBoard::ntotonets];
    thisboard.readAllAdc( reply );

    for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
      if ( thisboard.isActiveMezzanine( reply[i] ) ) {
	dtcan::adconet & adc = reply[i];
	tempI->Fill( it->second.base(), 0xff & adc.tempi );
	tempF->Fill( adc.temp );

	tempS->Fill( it->second.base(), adc.temp );

	tempF_1->Fill( ( adc.tempi * 2.048 / 256  - 1.140 ) / 0.0082 );
	tempF_2->Fill( ( (0xff &adc.tempi) * 4.096 / 256  - 1.140 ) / 0.0082 );
      }
    }
  }

  TFile * outfile = TFile::Open( "temps.root", "RECREATE" );
  tempI->Write();
  tempF->Write();
  tempS->Write();
  tempF_1->Write();
  tempF_2->Write();
  outfile->Close();
  delete outfile;
  // std::cout << my[board].readAdc( adc, 12 ) << '\n';
}
