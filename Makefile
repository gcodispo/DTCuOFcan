
######### Destination directories
SRC     = src
INCLUDE = include
BINDIR  = bin
LIBDIR  = lib
BUILD   = common
CLIDIR  = cli

######### Commands definition
CXX = g++

AR = ar

LD = $(CXX)

CXXFLAGS = -Wall -fPIC -I$(INCLUDE) $(INCEXT) -O2 -g #-pg 

LDFLAGS = $(CXXFLAGS) -Wl,-E -pthread -lrt -lodbc

MKARLIB = $(AR) r 

DEPEND = $(CXX) $(CXXFLAGS) -M -MG

MKOBJ = $(CXX) $(CXXFLAGS) -c

MKLIB = $(LD) $(LDFLAGS) -shared

MKBIN = $(CXX) $(LDFLAGS)


#########  root commands
MKOBJR = $(CXX) $(CXXFLAGS) -c `root-config --cflags`
MKLIBR = $(LD)  $(LDFLAGS)  -shared `root-config  --libs --cflags`
MKBINR = $(CXX) $(LDFLAGS) `root-config  --libs --cflags`


######### 
# define sources to be all file with extention .cc
SOURCES := $(wildcard $(SRC)/*.cc)
INCLUDES := $(wildcard $(INCLUDE)/*.h)

#define objects 
OBJECTS := $(addprefix $(BUILD)/,$(notdir $(SOURCES:.cc=.o)))

######### Actual targets

all : $(LIBDIR)/libCuOF.so iocan $(BINDIR)/maintest $(BINDIR)/test

$(LIBDIR)/libCuOF.so : $(OBJECTS)
	@mkdir -p lib
	$(MKLIB) $^ -o $@
#	echo $(OBJECTS) $(CLIDIR)/iocan.cpp

iocan : $(CLIDIR)/iocan.cpp $(LIBDIR)/libCuOF.so
	@mkdir -p bin
	$(MKBIN) $< -o $(BINDIR)/$@.exe -L$(LIBDIR) -lCuOF
	./install.sh

$(BINDIR)/test : $(CLIDIR)/test.cpp $(LIBDIR)/libCuOF.so
	$(MKBIN) $< -o $@ -L$(LIBDIR) -lCuOF

$(BINDIR)/maintest : $(CLIDIR)/maintest.cpp $(LIBDIR)/libCuOF.so
	$(MKBIN) $< -o $@ -L$(LIBDIR) -lCuOF

$(BINDIR)/plotTemp : $(CLIDIR)/plotTemp.cpp $(LIBDIR)/libCuOF.so
	$(MKBINR) $< -o $@ -L$(LIBDIR) -lCuOF

clean :
	rm -fv $(BUILD)/* $(BINDIR)/* $(LIBDIR)/* iocan dtcanenv.sh

# creates dependencies file
vpath %.cc $(SRC)
vpath %.o $(BUILD)
vpath %.d $(BUILD)
vpath %.h $(INCLUDE)

# compile a .cc
$(BUILD)/%.o : %.cc
	$(MKOBJ) -o $@ $<

# build dirs
.PHONY : mkdir -p $(BUILD) $(LIBDIR)
