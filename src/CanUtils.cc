#include "CanUtils.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdio.h>

void dtcan::readFile( const std::string & path,
		      std::vector< std::vector<std::string> > & res )
{

  std::ifstream ifile( path.c_str() );
  std::string line;

  while(getline(ifile, line)) {

    size_t len = line.size();
    if ( ! len ) continue;
    size_t i = 0;

    /// get rid of spaces
    while ( isspace( line[i] ) ) ++i;
    /// get rid of comments
    if ( line[i] == '#' ) {
      continue;
    }

    /// parse
    std::vector<std::string> entry;
    std::string tmp;
    while ( i < len ) {
      if ( isspace( line[i] ) ) {
        if ( !tmp.empty() ) {
// 	  std::cout << "'" << tmp << "'\n";
	  entry.push_back( tmp );
	  tmp = "";
	}
      } else {
        tmp += line[i];
      }
      ++i;
    }
    if ( !tmp.empty() ) entry.push_back( tmp );
    res.push_back( entry );
  }
}

int dtcan::convert( const char* val )
{
  int ret;
  if ( val[1]=='x' ) sscanf( val, "%x", &ret );
  else sscanf( val, "%d", &ret );
  return ret;
}




int getExeTime()
{
  struct timeval tp;
  gettimeofday(&tp, NULL);
  int tick=tp.tv_sec*1000 + tp.tv_usec/1000;
  return tick;
}

int getExeTime2()
{
  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);
  return start.tv_nsec;
}

std::vector<std::string> execute( const std::string & command )
{

  std::vector< std::string> cmdout;
  FILE* pipe = popen( command.c_str(), "r" );
  if ( pipe ) {
  char buffer[128];
  while ( !feof(pipe) ) {
    if ( fgets( buffer, 128, pipe ) != NULL ) {
      cmdout.push_back( buffer );
    }
  }
  if ( pclose(pipe) ) cmdout.push_back( "ERROR closing pipe" );
  } else {
    cmdout.push_back( "ERROR" );
  }
  return cmdout;
}



void execute( const std::string & command, std::ostream & out )
{

  FILE* pipe = popen( command.c_str(), "r" );
  if ( pipe ) {
    char buffer[128];
    while ( !feof(pipe) ) {
      if ( fgets( buffer, 128, pipe ) != NULL ) {
	out << buffer ;
      }
    }
    if ( pclose(pipe) ) out << "ERROR closing pipe\n";
  } else {
    out << "ERROR\n";
  }
}
