#include "CuOFCan.h"
#include "CuOFBoard.h"
#include "CanErrors.h"
#include "DbObject.h"
#include <fstream>


bool dtcan::operator<( const CtcpSocket & c1, const CtcpSocket & c2 )
{
  int cmp = strcmp ( c1.host, c2.host );
  if ( cmp ) return ( cmp < 0 );

  return ( c1.port < c2.port );
};



bool dtcan::operator==( const CtcpSocket & c1, const CtcpSocket & c2 )
{
  return ( c1.port == c2.port && strcmp ( c1.host, c2.host ) == 0 );
};




dtcan::CtcpSocket * dtcan::CuOFCan::getSocket( const std::string & name )
{
  std::map<std::string, dtcan::CtcpSocket *>::iterator end = cansock_.end();
  std::map<std::string, dtcan::CtcpSocket *>::iterator it = cansock_.find( name );
  if ( it == end ) {
    throw dtcanerror::badsocket( name.c_str() );
  }

  return it->second;

}

void dtcan::CuOFCan::configureFromDB( const std::string & key )
{
  std::cout << "============== Reading from DB with key '" << key << "'\n";
  CuOFDb::DbObject db("drifttube", "cms_dt_elec_conf", "cmsonr2008");
  fillAdcMapFromDB( db );
  fillParamsFromDB( db );
  fillBoardParamsFromDB( db, key );
  fillMezzanineParamsFromDB( db, key );
}


void dtcan::CuOFCan::fillParamsFromDB( CuOFDb::DbObject & db )
{

  char  CRATE         [256];
  char  WHEEL         [4];
  char  PRIMARY_IP    [16];
  char  PRIMARY_PORT  [8];
  char  SECONDARY_IP  [16];
  char  SECONDARY_PORT[8];
  int rows=0, fields=0;
  char query[] = "SELECT CRATE,WHEEL,PRIMARY_IP,PRIMARY_PORT,SECONDARY_IP,SECONDARY_PORT FROM CUOF_CRATE";
  db.sqlQuery( query, rows, fields);

  size_t km_offset = rows;

  // db stuff
  int counter=0;
  while ( db.fetchrow() == SQL_SUCCESS ) {
    ++counter;
    int count = 0;
    db.returnfield(   count, CRATE          );
    db.returnfield( ++count, WHEEL          );
    db.returnfield( ++count, PRIMARY_IP     );
    db.returnfield( ++count, PRIMARY_PORT   );
    db.returnfield( ++count, SECONDARY_IP   );
    db.returnfield( ++count, SECONDARY_PORT );

    std::string wheel;
    short wh = convert( WHEEL );
    switch ( wh ) {
    case -2 : wheel = "YBm2"; break;
    case -1 : wheel = "YBm2";	break;
    case  0 : wheel = "YB2";	break;
    case  1 : wheel = "YBp2";	break;
    case  2 : wheel = "YBp2"; break;
    default : throw dtcanerror::badconfig( 0, 0 );
    }

    std::string name( CRATE );
    short port = convert( PRIMARY_PORT );
    std::string host( PRIMARY_IP );
    short port2 = convert( SECONDARY_PORT );
    std::string host2( SECONDARY_IP );

    cansock_.insert( std::pair<std::string, dtcan::CtcpSocket*>( name, new dtcan::CtcpSocket( nSockets_, port, host, wh, wheel, name) ) );

    std::string dest2 = "KM_" + name;
    cansock_.insert( std::pair<std::string, dtcan::CtcpSocket*>( dest2, new dtcan::CtcpSocket( km_offset + nSockets_, port2, host2, wh, wheel, name, false) ) );

    /// increase counter
    ++nSockets_;
    
  }

}



void dtcan::CuOFCan::fillAdcMapFromDB( CuOFDb::DbObject & db )
{
  char REGISTER [5];
  char ONET     [2];
  char ADC      [2];
  char CHANNEL  [2];
  int rows=0, fields=0;
  char query[] = "SELECT REGISTER,ONET,ADC,CHANNEL FROM CUOF_ADCS";
  db.sqlQuery( query, rows, fields);

  // db stuff
  int counter=0;
  std::vector< std::vector < std::string > > res; 
  while ( db.fetchrow() == SQL_SUCCESS ) {
    ++counter;
    int count = 0;
    db.returnfield(   count, REGISTER );
    db.returnfield( ++count, ONET     );
    db.returnfield( ++count, ADC      );
    db.returnfield( ++count, CHANNEL  );

    dtcan::CuOFBoard::fillAdcMap( std::string( REGISTER ), convert( ONET ),
				  convert( ADC ), convert( CHANNEL ) );
//     std::cout << "============== "
//     	      << std::string( REGISTER ) << '\t'
//     	      << convert( ONET ) << '\t'
//     	      << convert( ADC ) << '\t'
//     	      << convert( CHANNEL ) << '\n';
    

  }
}






void dtcan::CuOFCan::fillBoardParamsFromDB( CuOFDb::DbObject & db,
					    const std::string & key )
{

  char  CUOF_KEY   [256];
  char  BOARD      [256];
  char  CRATE      [256];
  char  WHEEL        [2];
  char  TYPE         [1];
  char  BASE_ADDRESS [5];
  char  MASK         [5];

  snprintf( CUOF_KEY, 256, "%s", key.c_str() );
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR, CUOF_KEY, strlen(CUOF_KEY) );
  char query[] = "SELECT CUOF_KEY,BOARD,CRATE,WHEEL,TYPE,BASE_ADDRESS,MASK FROM CUOF_BOARD WHERE CUOF_KEY=?";
  db.sqlQueryBind( query, bindSet, 1 );

  // db stuff
  int counter=0;

  while ( db.fetchrow() == SQL_SUCCESS ) {
    ++counter;
    int count = 0;
    db.returnfield(   count, CUOF_KEY     );
    db.returnfield( ++count, BOARD        );
    db.returnfield( ++count, CRATE        );
    db.returnfield( ++count, WHEEL        );
    db.returnfield( ++count, TYPE         );
    db.returnfield( ++count, BASE_ADDRESS );
    db.returnfield( ++count, MASK         );

    std::string name( BOARD );
    std::string id;
    int wheel = convert( WHEEL );
    switch ( wheel ) {
    case -2 : id = "YBm2_" + name; break;
    case -1 : id = "YBm1_" + name; break;
    case  0 : id = "YB0_"  + name;  break;
    case  1 : id = "YBp1_" + name; break;
    case  2 : id = "YBp2_" + name; break;
    default : {
      std::cout << "'" << CUOF_KEY
		<< "' '" << BOARD
		<< "' '" << CRATE
		<< "' '" << WHEEL
		<< "'\t";
      std::cout << std::endl;
      throw dtcanerror::badwheel( wheel );
    }
    }

    if ( TYPE[0] != 'T' && TYPE[0] != 'R' ) dtcanerror::badwheel( 0 );
    char trg = ( TYPE[0] == 'T' );

    std::string crate( CRATE );
    dtcan::CtcpSocket * cansock = getSocket( crate );
    dtcan::CtcpSocket * cansockKM = getSocket( "KM_" + crate );
    char base = convert( BASE_ADDRESS );
    char mask = convert( MASK );
    boards_[id] = CuOFBoard( base, mask, trg, name, cansock, cansockKM );

//     std::cout << "*** " << id << " : " << std::hex << int(0xff & base)
// 	      << " (" << std::hex << int(0xff & mask) << ")"
// 	      << std::dec << std::endl;

  }
}



void dtcan::CuOFCan::fillMezzanineParamsFromDB( CuOFDb::DbObject & db,
						const std::string & key )
{

  char CUOF_KEY   [256];
  char BOARD      [256];
  char WHEEL        [2];
  char MEZZANINE    [2];
  char MODULATION_0 [3];
  char BIAS_0       [3];
  char MODULATION_1 [3];
  char BIAS_1       [3];
  char MODULATION_2 [3];
  char BIAS_2       [3];
  char MODULATION_3 [3];
  char BIAS_3       [3];
  char MODULATION_4 [3];
  char BIAS_4       [3];
  char MODULATION_5 [3];
  char BIAS_5       [3];
  char MODULATION_6 [3];
  char BIAS_6       [3];
  char MODULATION_7 [3];
  char BIAS_7       [3];

  snprintf( CUOF_KEY, 256, "%s", key.c_str() );
  binder bindSet [1];
  bindSet[0] = binder(SQL_C_CHAR, SQL_CHAR, CUOF_KEY, strlen(CUOF_KEY) );
  char query[] = "SELECT CUOF_KEY,BOARD,WHEEL,MEZZANINE,MODULATION_0,BIAS_0,MODULATION_1,BIAS_1,MODULATION_2,BIAS_2,MODULATION_3,BIAS_3,MODULATION_4,BIAS_4,MODULATION_5,BIAS_5,MODULATION_6,BIAS_6,MODULATION_7,BIAS_7 FROM CUOF_MEZZANINE WHERE CUOF_KEY=?";
  db.sqlQueryBind( query, bindSet, 1 );

  // db stuff
  int counter=0;

  while ( db.fetchrow() == SQL_SUCCESS ) {
    ++counter;
    int count = 0;
    db.returnfield(   count, CUOF_KEY     );
    db.returnfield( ++count, BOARD        );
    db.returnfield( ++count, WHEEL        );
    db.returnfield( ++count, MEZZANINE    );
    db.returnfield( ++count, MODULATION_0 );
    db.returnfield( ++count, BIAS_0       );
    db.returnfield( ++count, MODULATION_1 );
    db.returnfield( ++count, BIAS_1       );
    db.returnfield( ++count, MODULATION_2 );
    db.returnfield( ++count, BIAS_2       );
    db.returnfield( ++count, MODULATION_3 );
    db.returnfield( ++count, BIAS_3       );
    db.returnfield( ++count, MODULATION_4 );
    db.returnfield( ++count, BIAS_4       );
    db.returnfield( ++count, MODULATION_5 );
    db.returnfield( ++count, BIAS_5       );
    db.returnfield( ++count, MODULATION_6 );
    db.returnfield( ++count, BIAS_6       );
    db.returnfield( ++count, MODULATION_7 );
    db.returnfield( ++count, BIAS_7       );

    std::string id( BOARD );
    int wheel = convert( WHEEL );
    switch ( wheel ) {
    case -2 : id = "YBm2_" + id; break;
    case -1 : id = "YBm1_" + id; break;
    case  0 : id = "YB0_" + id;  break;
    case  1 : id = "YBp1_" + id; break;
    case  2 : id = "YBp2_" + id; break;
    default : {
      std::cout << "'" << CUOF_KEY
		<< "' '" << BOARD
		<< "' '" << WHEEL
		<< "' '" << MEZZANINE
		<< "'\t";
      std::cout << std::endl;
      throw dtcanerror::badwheel( wheel );
    }
    }

//     std::cout << "*** " << id << " : "
// 	      << " " << BOARD
// 	      << " " << WHEEL
// 	      << " " << MEZZANINE
// 	      << " " << convert( MODULATION_0) << ' ' << convert( BIAS_0 )
// 	      << std::endl;

    char mezz = convert( MEZZANINE );
    CuOFBoard & thisBoard = boards_[id];
    thisBoard.setOnetSettings( mezz, 0, convert( MODULATION_0), convert( BIAS_0 ) );
    thisBoard.setOnetSettings( mezz, 1, convert( MODULATION_1), convert( BIAS_1 ) );
    thisBoard.setOnetSettings( mezz, 2, convert( MODULATION_2), convert( BIAS_2 ) );
    thisBoard.setOnetSettings( mezz, 3, convert( MODULATION_3), convert( BIAS_3 ) );
    thisBoard.setOnetSettings( mezz, 4, convert( MODULATION_4), convert( BIAS_4 ) );
    thisBoard.setOnetSettings( mezz, 5, convert( MODULATION_5), convert( BIAS_5 ) );
    thisBoard.setOnetSettings( mezz, 6, convert( MODULATION_6), convert( BIAS_6 ) );
    thisBoard.setOnetSettings( mezz, 7, convert( MODULATION_7), convert( BIAS_7 ) );

  }
}





void dtcan::CuOFCan::fillParams( const std::string & path )
{

  std::cout << "============== Parsing ECBs param file " << path << "\n";
  std::vector< std::vector < std::string > > res; 
  readFile( path, res );

  std::vector< std::vector< std::string > >::const_iterator vit = res.begin();
  std::vector< std::vector< std::string > >::const_iterator vitend = res.end();
  size_t km_offset = res.size();

  for ( ; vit != vitend; ++vit ) {

    if ( vit->size() == 6 ) {

      std::string name = vit->at(0);
      short wh = convert( vit->at(1).c_str() );
      short port = convert( vit->at(2).c_str() );
      std::string host = vit->at(3);
      short port2 = convert( vit->at(4).c_str() );
      std::string host2 = vit->at(5);

      //std::cout << vit->at(1).c_str() << ' ' << wh << std::endl;
      std::string wheel;
      switch ( wh ) {
      case -2 : wheel = "YBm2"; break;
      case -1 : wheel = "YBm2";	break;
      case  0 : wheel = "YB2";	break;
      case  1 : wheel = "YBp2";	break;
      case  2 : wheel = "YBp2"; break;
      default : throw dtcanerror::badconfig( 0, 0 );
      }

      cansock_.insert( std::pair<std::string, dtcan::CtcpSocket*>( name, new dtcan::CtcpSocket( nSockets_, port, host, wh, wheel, name ) ) );
      
      std::string dest2 = "KM_" + name;
      cansock_.insert( std::pair<std::string, dtcan::CtcpSocket*>( dest2, new dtcan::CtcpSocket( km_offset + nSockets_, port2, host2, wh, wheel, name, false) ) );

      /// increase counter
      ++nSockets_;

    } else {
      throw dtcanerror::badconfig( vit->size(), 6 );
    }
  }
}






void dtcan::CuOFCan::fillBoardParams( const std::string & path )
{

  std::cout << "============== Parsing Boards param file " << path << "\n";
  std::vector< std::vector < std::string > > res; 
  readFile( path, res );

  std::vector< std::vector< std::string > >::const_iterator vit = res.begin();
  std::vector< std::vector< std::string > >::const_iterator vitend = res.end();

  for ( ; vit != vitend; ++vit ) {

    if ( vit->size() == 9 ) {

      std::string name = vit->at(0);
      std::string id;

      int wheel = convert( vit->at(1).c_str() );

      if ( wheel == -2 )      id = "YBm2_" + name;
      else if ( wheel == -1 ) id = "YBm1_" + name;
      else if ( wheel == 0 )  id = "YB0_"  + name;
      else if ( wheel == 1 )  id = "YBp1_" + name;
      else if ( wheel == 2 )  id = "YBp2_" + name;
      else {
	size_t siz = vit->size();
	for ( size_t i = 0; i < siz; ++i ) std::cout << "'" << vit->at(i) << "'\t";
	std::cout << std::endl;
	throw dtcanerror::badwheel( wheel );
      }

      std::string crate = vit->at(2).c_str();

      char trg = convert( vit->at(3).c_str() );

      dtcan::CtcpSocket * cansock = getSocket( crate );
      dtcan::CtcpSocket * cansockKM = getSocket( "KM_" + crate );

      /// this need to be changed
      char base = convert( vit->at(4).c_str() );
      //base = 0x0600 + (0xff & base);

      char mask = 0;
      if ( vit->at(5) != "0" ) mask |= 1;
      if ( vit->at(6) != "0" ) mask |= 2;
      if ( vit->at(7) != "0" ) mask |= 4;
      if ( vit->at(8) != "0" ) mask |= 8;

      boards_[id] = CuOFBoard( base, mask, trg, name, cansock, cansockKM );

//       std::cout << "*** " << id << " : " << std::hex << int(0xff & base)
// 		<< " (" << std::hex << int(0xff & mask) << ")"
// 		<< std::dec << std::endl;

    } else {
      size_t siz = vit->size();
      for ( size_t i = 0; i < siz; ++i ) std::cout << "'" << vit->at(i) << "'\t";
      std::cout << std::endl;
      throw dtcanerror::badconfig( vit->size(), 9 );
    }
  }
}


void dtcan::CuOFCan::fillAdcMap( const std::string & path )
{

  std::cout << "============== Parsing ADC map " << path << "\n";
  std::vector< std::vector < std::string > > res; 
  readFile( path, res );

  std::vector< std::vector < std::string > >::const_iterator vit = res.begin();
  std::vector< std::vector < std::string > >::const_iterator vitend = res.end();
  
  for ( ; vit != vitend; ++vit ) {

    if ( vit->size() != 4 ) throw dtcanerror::badconfig( vit->size(), 4 );

    int onet = convert( vit->at(1).c_str() );
    int adc  = convert( vit->at(2).c_str() );
    int chan = convert( vit->at(3).c_str() );

    // std::cout << "============== "
    // 	      << vit->at(0) << '\t'
    // 	      << onet << '\t'
    // 	      << adc << '\t'
    // 	      << chan << '\t'
    // 	      << '\n';

    dtcan::CuOFBoard::fillAdcMap( vit->at(0), onet, adc, chan );

  }

  //   for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
  //     std::cout << adcmap_[i];
  //   }
}


void dtcan::CuOFCan::connect()
{
  std::map<std::string, dtcan::CtcpSocket *>::iterator end = cansock_.end();
  std::map<std::string, dtcan::CtcpSocket *>::iterator it = cansock_.begin();
  for ( ; it != end; ++it ) {
    // std::cout << it->first.host << ' ' << it->first.port << std::endl;
    it->second->releaseSocket();
  }
}


void dtcan::CuOFCan::release()
{
  std::map<std::string, dtcan::CtcpSocket *>::iterator end = cansock_.end();
  std::map<std::string, dtcan::CtcpSocket *>::iterator it = cansock_.begin();
  for ( ; it != end; ++it ) {
    // std::cout << it->first.host << ' ' << it->first.port << std::endl;
    it->second->getSocket();
  }
}



dtcan::CtcpSocket * dtcan::CuOFCan::connect( const std::string & name )
{
  dtcan::CtcpSocket * sock = getSocket( name );
  return sock;
}



dtcan::CtcpSocket * dtcan::CuOFCan::release( const std::string & name )
{
  dtcan::CtcpSocket * sock = getSocket( name );
  sock->releaseSocket();
  return sock;
}


dtcan::CtcpSocket * dtcan::CuOFCan::release( short id )
{

  std::map<std::string, dtcan::CtcpSocket *>::iterator end = cansock_.end();
  std::map<std::string, dtcan::CtcpSocket *>::iterator it = cansock_.begin();
  for ( ; it != end; ++it ) {
    if ( it->second->id == id ) {
      it->second->releaseSocket();
      return it->second;
    }
  }
  return 0;
}


// /// constructor
// dtcan::CuOFCan::CuOFCan( const short & port, char host[15],
// 			 const std::string & adcconf )
//   : port_(port), cansock_(0)
// {
//   CuOFBoard::fillAdcMap( adcconf );
//   snprintf( host_, 15, "%s", host );
//   cansock[port]_.Connect(host_, port_);
// };


void dtcan::CuOFCan::configure( const std::string & canconf,
				const std::string & boardconf,
				const std::string & adcconf )
{
  fillAdcMap( adcconf );
  fillParams( canconf );
  fillBoardParams( boardconf );
  // connect();
}


dtcan::CuOFBoard& dtcan::CuOFCan::operator[] ( const std::string & sid )
{
  std::map< std::string, CuOFBoard >::const_iterator end = boards_.end();
  std::map< std::string, CuOFBoard >::iterator bo = boards_.find( sid );
  if ( bo != end ) return bo->second;
  else throw dtcanerror::badboard( sid.c_str() );
}








//// Write to DB

int dtcan::CuOFCan::insertNewKey( const std::string & key )
{
  CuOFDb::DbObject db("drifttube", "cms_dt_elec_conf", "cmsonr2008");

  int row, col;
  char qstr[256];
  sprintf( qstr, "insert into CUOF_CONF ( CUOF_KEY, TIME ) values ( '%s', CURRENT_TIMESTAMP)", key.c_str() ) ;

  //   std::cout << qstr << std::endl;
//   return 0;

  int ret = db.sqlQuery( qstr, row, col );

  std::map< std::string, dtcan::CuOFBoard >::iterator it = begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator itend = end();
  for ( ; it != itend; ++it ) {
    ret += writeBoardParamsFromDB( db, key, it->second );
  }

  return ret;
}



int dtcan::CuOFCan::writeBoardParamsFromDB( CuOFDb::DbObject & db,
					    const std::string & key,
					    const CuOFBoard & board )
{


  char  CUOF_KEY   [256];
  snprintf( CUOF_KEY, 256, "%s", key.c_str() );

  char  BOARD      [256];
  snprintf( BOARD, 256, "%s", board.name().c_str() );

  char  CRATE      [256];
  snprintf( CRATE, 256, "%s", board.crate().c_str() );

  char  TYPE      [2];
  if ( board.isRo() ) snprintf( TYPE, 2, "R" );
  else snprintf( TYPE, 2, "T" );

  int  WHEEL        = board.iwheel();
  int  BASE_ADDRESS = int( 0xff & board.base() );
  int  MASK         = int( 0xff & board.mask() );

  char insquery[] = "insert into CUOF_BOARD ( CUOF_KEY, BOARD, CRATE, WHEEL, TYPE, BASE_ADDRESS, MASK ) values ( ?, ?, ?, ?, ?, ?, ? )";
  binder bindSetIns [7];
  bindSetIns[0] = binder(SQL_C_CHAR, SQL_CHAR,    CUOF_KEY, strlen(CUOF_KEY) );
  bindSetIns[1] = binder(SQL_C_CHAR, SQL_CHAR,    BOARD, strlen(BOARD) );
  bindSetIns[2] = binder(SQL_C_CHAR, SQL_CHAR,    CRATE, strlen(CRATE) );
  bindSetIns[3] = binder(SQL_C_LONG, SQL_INTEGER, &WHEEL, 0 );
  bindSetIns[4] = binder(SQL_C_CHAR, SQL_CHAR,    TYPE, strlen(TYPE) );
  bindSetIns[5] = binder(SQL_C_LONG, SQL_INTEGER, &BASE_ADDRESS, 0 );
  bindSetIns[6] = binder(SQL_C_LONG, SQL_INTEGER, &MASK, 0 );

  int ret = db.sqlQueryBind(insquery, bindSetIns, 7);

  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( board.isActiveMezzanine( mezz ) ) {
      // std::cout << key << ' ' << boardname << ' ' << mezz << std::endl;
      ret += writeMezzanineParamsFromDB( db, key, mezz, board );
    }
  }
  return ret;
}


int dtcan::CuOFCan::writeMezzanineParamsFromDB( CuOFDb::DbObject & db,
						const std::string & key,
						int mezz,
						const CuOFBoard & board )
{

  char  CUOF_KEY   [256];
  snprintf( CUOF_KEY, 256, "%s", key.c_str() );

  char  BOARD      [256];
  snprintf( BOARD, 256, "%s", board.name().c_str() );

  int WHEEL        = board.iwheel();
  int MEZZANINE    = mezz;
  int MODULATION_0 = board.getOnetModulation( mezz, 0 );
  int BIAS_0       = board.getOnetBias( mezz, 0 );
  int MODULATION_1 = board.getOnetModulation( mezz, 1 );
  int BIAS_1       = board.getOnetBias( mezz, 1 );
  int MODULATION_2 = board.getOnetModulation( mezz, 2 );
  int BIAS_2       = board.getOnetBias( mezz, 2 );
  int MODULATION_3 = board.getOnetModulation( mezz, 3 );
  int BIAS_3       = board.getOnetBias( mezz, 3 );
  int MODULATION_4 = board.getOnetModulation( mezz, 4 );
  int BIAS_4       = board.getOnetBias( mezz, 4 );
  int MODULATION_5 = board.getOnetModulation( mezz, 5 );
  int BIAS_5       = board.getOnetBias( mezz, 5 );
  int MODULATION_6 = board.getOnetModulation( mezz, 6 );
  int BIAS_6       = board.getOnetBias( mezz, 6 );
  int MODULATION_7 = board.getOnetModulation( mezz, 7 );
  int BIAS_7       = board.getOnetBias( mezz, 7 );


  char insquery[] = "insert into CUOF_MEZZANINE ( CUOF_KEY,BOARD,WHEEL,MEZZANINE,MODULATION_0,BIAS_0,MODULATION_1,BIAS_1,MODULATION_2,BIAS_2,MODULATION_3,BIAS_3,MODULATION_4,BIAS_4,MODULATION_5,BIAS_5,MODULATION_6,BIAS_6,MODULATION_7,BIAS_7 ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
  binder bindSetIns [20];
  bindSetIns[0]  = binder(SQL_C_CHAR, SQL_CHAR,    CUOF_KEY, strlen(CUOF_KEY) );
  bindSetIns[1]  = binder(SQL_C_CHAR, SQL_CHAR,    BOARD, strlen(BOARD) );
  bindSetIns[2]  = binder(SQL_C_LONG, SQL_INTEGER, &WHEEL, 0 );
  bindSetIns[3]  = binder(SQL_C_LONG, SQL_INTEGER, &MEZZANINE    , 0 );
  bindSetIns[4]  = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_0 , 0 );
  bindSetIns[5]  = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_0       , 0 );
  bindSetIns[6]  = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_1 , 0 );
  bindSetIns[7]  = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_1       , 0 );
  bindSetIns[8]  = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_2 , 0 );
  bindSetIns[9]  = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_2       , 0 );
  bindSetIns[10] = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_3 , 0 );
  bindSetIns[11] = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_3       , 0 );
  bindSetIns[12] = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_4 , 0 );
  bindSetIns[13] = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_4       , 0 );
  bindSetIns[14] = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_5 , 0 );
  bindSetIns[15] = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_5       , 0 );
  bindSetIns[16] = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_6 , 0 );
  bindSetIns[17] = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_6       , 0 );
  bindSetIns[18] = binder(SQL_C_LONG, SQL_INTEGER, &MODULATION_7 , 0 );
  bindSetIns[19] = binder(SQL_C_LONG, SQL_INTEGER, &BIAS_7       , 0 );

  return db.sqlQueryBind( insquery, bindSetIns, 20 );

}



