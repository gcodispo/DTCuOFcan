
#include "CuOFThreadCommand.h"
#include <algorithm>

void dtcan::CuOFThreadCommand::clearAdconetMap()
{

  std::map< std::string, dtcan::adconet * >::iterator it = adconet_map_.begin();
  std::map< std::string, dtcan::adconet * >::iterator end = adconet_map_.end();

  for ( ; it != end; ++it ) delete [] it->second;
  adconet_map_.clear();
}


void dtcan::CuOFThreadCommand::clearOnetMap()
{

  std::map< std::string, dtcan::onet * >::iterator it = onet_map_.begin();
  std::map< std::string, dtcan::onet * >::iterator end = onet_map_.end();

  for ( ; it != end; ++it ) delete [] it->second;
  onet_map_.clear();
}


void dtcan::CuOFThreadCommand::clearAdcMap()
{

  std::map< std::string, dtcan::adc * >::iterator it = adc_map_.begin();
  std::map< std::string, dtcan::adc * >::iterator end = adc_map_.end();

  for ( ; it != end; ++it ) delete [] it->second;
  adc_map_.clear();
}


void dtcan::CuOFThreadCommand::printAdcMap()
{

  dtcan::CuOFCan & can = *dtcan::CuOFThread::can_;

  std::map< std::string, dtcan::adconet * >::iterator it = adconet_map_.begin();
  std::map< std::string, dtcan::adconet * >::iterator end = adconet_map_.end();
  for ( ; it != end; ++it ) {
    for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
      if ( can[it->first].isActiveMezzanine( it->second[i] ) )
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}


void dtcan::CuOFThreadCommand::printAdcMap( bool trgOnly )
{

  dtcan::CuOFCan & can = *dtcan::CuOFThread::can_;

  std::map< std::string, dtcan::adconet * >::iterator it = adconet_map_.begin();
  std::map< std::string, dtcan::adconet * >::iterator end = adconet_map_.end();
  for ( ; it != end; ++it ) {

    bool isTrg = can[it->first].isTrg();
    if ( ( isTrg && ! trgOnly ) || ( ! isTrg && trgOnly ) ) continue;

    for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
      if ( can[it->first].isActiveMezzanine( it->second[i] ) )
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}



void dtcan::CuOFThreadCommand::printOnetMap()
{
  dtcan::CuOFCan & can = *dtcan::CuOFThread::can_;
  std::map< std::string, dtcan::onet * >::iterator it = onet_map_.begin();
  std::map< std::string, dtcan::onet * >::iterator end = onet_map_.end();

  for ( ; it != end; ++it ) {
    for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
      if ( can[it->first].isActiveMezzanine( it->second[i] ) )
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}


void dtcan::CuOFThreadCommand::printOnetMap( bool trgOnly )
{
  dtcan::CuOFCan & can = *dtcan::CuOFThread::can_;
  std::map< std::string, dtcan::onet * >::iterator it = onet_map_.begin();
  std::map< std::string, dtcan::onet * >::iterator end = onet_map_.end();

  for ( ; it != end; ++it ) {

    bool isTrg = can[it->first].isTrg();
    if ( ( isTrg && ! trgOnly ) || ( ! isTrg && trgOnly ) ) continue;

    for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
      if ( can[it->first].isActiveMezzanine( it->second[i] ) )
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}


void dtcan::CuOFThreadCommand::printAdcConfigMap()
{
  std::map< std::string, dtcan::adc * >::iterator it = adc_map_.begin();
  std::map< std::string, dtcan::adc * >::iterator end = adc_map_.end();

  for ( ; it != end; ++it ) {
    for ( size_t i = 0; i < dtcan::CuOFBoard::nadc; ++i ) {
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}


void dtcan::CuOFThreadCommand::printAdcConfigMap( bool trgOnly )
{
  dtcan::CuOFCan & can = *dtcan::CuOFThread::can_;
  std::map< std::string, dtcan::adc * >::iterator it = adc_map_.begin();
  std::map< std::string, dtcan::adc * >::iterator end = adc_map_.end();

  for ( ; it != end; ++it ) {

    bool isTrg = can[it->first].isTrg();
    if ( ( isTrg && ! trgOnly ) || ( ! isTrg && trgOnly ) ) continue;

    for ( size_t i = 0; i < dtcan::CuOFBoard::nadc; ++i ) {
	std::cout << it->first << '\t' << it->second[i];
    }
  }
}




size_t dtcan::CuOFThreadCommand::allThreads()
{

  tids_.clear();
  nThreads_ = 0;

  std::map< std::string, CtcpSocket * >::iterator it =
    dtcan::CuOFThread::can_->canBegin();

  std::map< std::string, CtcpSocket * >::iterator end =
    dtcan::CuOFThread::can_->canEnd();

  for ( ; it != end; ++ it ) {
      tids_.push_back( it->second->id );
      ++nThreads_;
  }
  return nThreads_;
}



void
dtcan::CuOFThreadCommand::applySelection( enum SELECTION sel,
					  const std::vector<std::string> & subset,
					  bool force2nd )
{

  tids_.clear();
  nThreads_ = 0;
  force2nd_ = force2nd;
  force2nd_ ? CuOFBoard::switchTo2nd() : CuOFBoard::switchTo1st();

  std::map< std::string, CtcpSocket * >::iterator it =
    dtcan::CuOFThread::can_->canBegin();

  std::map< std::string, CtcpSocket * >::iterator end =
    dtcan::CuOFThread::can_->canEnd();

  for ( ; it != end; ++ it ) {
    bool good  = it->second->primary;
    int tid = it->second->id;
    switch ( sel ) {
    case CRATE : {
      std::string name = it->first;
      std::find( subset.begin(), subset.end(), name );
      good &= ( std::find( subset.begin(), subset.end(), name ) != subset.end() );
      break; }
    case WHEEL : {
      std::string crate = it->second->wheel;
      good &= ( std::find( subset.begin(), subset.end(), crate ) != subset.end() );
      break; }
    case ALL : {
      break; }
    }
    if ( good ) {

      std::cout << tid << ' ' << it->first << ' ' << it->second->wheel << std::endl;
      tids_.push_back( tid );
      ++nThreads_;
    }
  }
  if ( nThreads_ == 0 )
    throw dtcanerror::CanError("invalid crate/wheel selection requested");
}


void dtcan::CuOFThreadCommand::readAllAdc( dtcan::TYPE type )
{

  dtcan::readAllAdc * thread[NUM_THREADS];

  if ( tids_.empty() ) allThreads();
  for ( size_t iThread = 0; iThread < nThreads_; ++iThread ) {
    int tid = tids_.at( iThread );
    thread[iThread] = new dtcan::readAllAdc(tid);
    switch ( type)  {
    case RO : thread[iThread]->setRoOnly(); break;
    case TRG : thread[iThread]->setTrgOnly(); break;
    default : break;
    }
    thread[iThread]->start();
  }

  size_t iThread = nThreads_;
  do {
    --iThread;
    thread[iThread]->join();
  } while ( iThread );
  //pthread_exit(0);
  adconet_map_.clear();

  iThread = nThreads_;
  do {
    --iThread;
    std::map< std::string, dtcan::adconet * >::iterator it = thread[iThread]->adconetmap_.begin();
    std::map< std::string, dtcan::adconet * >::iterator end = thread[iThread]->adconetmap_.end();

    for ( ; it != end; ++it ) {
      adconet_map_[ it->first ] = it->second;;
    }

    delete thread[iThread];
  } while ( iThread );

}




void dtcan::CuOFThreadCommand::readAllOnets( dtcan::TYPE type )
{
  if ( tids_.empty() ) allThreads();
  dtcan::readAllOnets * thread[NUM_THREADS];


  for ( size_t iThread = 0; iThread < nThreads_; ++iThread ) {
    int tid = tids_.at( iThread );
    thread[iThread] = new dtcan::readAllOnets(tid);
    switch ( type)  {
    case RO : thread[iThread]->setRoOnly(); break;
    case TRG : thread[iThread]->setTrgOnly(); break;
    default : break;
    }
    thread[iThread]->start();
  }

  size_t iThread = nThreads_;
  do {
    --iThread;
    thread[iThread]->join();
  } while ( iThread );
  //pthread_exit(0);
  onet_map_.clear();

  iThread = nThreads_;
  do {
    --iThread;
    std::map< std::string, dtcan::onet * >::iterator it = thread[iThread]->onetmap_.begin();
    std::map< std::string, dtcan::onet * >::iterator end = thread[iThread]->onetmap_.end();

    for ( ; it != end; ++it ) {
      onet_map_[ it->first ] = it->second;;
    }

    delete thread[iThread];
  } while ( iThread );

}



void dtcan::CuOFThreadCommand::readAdcConf( dtcan::TYPE type )
{
  dtcan::readAdcConf * thread[NUM_THREADS];

  if ( tids_.empty() ) allThreads();
  for ( size_t iThread = 0; iThread < nThreads_; ++iThread ) {
    int tid = tids_.at( iThread );
    thread[iThread] = new dtcan::readAdcConf(tid);
    switch ( type)  {
    case RO : thread[iThread]->setRoOnly(); break;
    case TRG : thread[iThread]->setTrgOnly(); break;
    default : break;
    }
    thread[iThread]->start();
  }

  size_t iThread = nThreads_;
  do {
    --iThread;
    thread[iThread]->join();
  } while ( iThread );
  //pthread_exit(0);
  adc_map_.clear();

  iThread = nThreads_;
  do {
    --iThread;
    std::map< std::string, dtcan::adc * >::iterator it = thread[iThread]->adcmap_.begin();
    std::map< std::string, dtcan::adc * >::iterator end = thread[iThread]->adcmap_.end();

    for ( ; it != end; ++it ) {
      adc_map_[ it->first ] = it->second;;
    }

    delete thread[iThread];
  } while ( iThread );

}






void dtcan::CuOFThreadCommand::writeAllOnets( dtcan::TYPE type,
					      char mod, char bias,
					      bool isFullConfig )
{
  printf("Thread... mod=%02hhx, bias=%02hhx\n",  mod, bias);

  dtcan::writeAllOnets * thread[NUM_THREADS];

  if ( tids_.empty() ) allThreads();
  for ( size_t iThread = 0; iThread < nThreads_; ++iThread ) {
    int tid = tids_.at( iThread );
    thread[iThread] = new dtcan::writeAllOnets(tid, isFullConfig);
    switch ( type)  {
    case RO : thread[iThread]->setRoOnly(); break;
    case TRG : thread[iThread]->setTrgOnly(); break;
    default : break;
    }
    thread[iThread]->setParams ( mod, bias );
    thread[iThread]->start();
  }

  size_t iThread = nThreads_;
  do {
    --iThread;
    thread[iThread]->join();
  } while ( iThread );
  onet_map_.clear();

  iThread = nThreads_;
  do {
    --iThread;
    std::map< std::string, dtcan::onet * >::iterator it = thread[iThread]->onetmap_.begin();
    std::map< std::string, dtcan::onet * >::iterator end = thread[iThread]->onetmap_.end();

    for ( ; it != end; ++it ) {
      onet_map_[ it->first ] = it->second;;
    }

    delete thread[iThread];
  } while ( iThread );

}


////////////////////////////////////////////
/// this is the set of commands doing popen
/// for firmware related issues
////////////////////////////////////////////

void dtcan::CuOFThreadCommand::actel( const std::string & exec,
				      const std::string & action,
				      const std::string & file_dat,
				      enum TYPE type )
{

  dtcan::actel * thread[NUM_THREADS];

  if ( tids_.empty() ) allThreads();
  for ( size_t iThread = 0; iThread < nThreads_; ++iThread ) {
    int tid = tids_.at( iThread );
    thread[iThread] = new dtcan::actel(tid, exec, action, file_dat, force2nd_ );
    switch ( type)  {
    case RO : thread[iThread]->setRoOnly(); break;
    case TRG : thread[iThread]->setTrgOnly(); break;
    default : break;
    }

    thread[iThread]->start();
  }

  size_t iThread = nThreads_;
  do {
    --iThread;
    thread[iThread]->join();
  } while ( iThread );

  iThread = nThreads_;
  do {
    --iThread;
    delete thread[iThread];
  } while ( iThread );
  //pthread_exit(0);

}



void
dtcan::CuOFThreadCommand::actelCLI( dtcan::TYPE type, 
				    const std::vector<std::string> & args )
{
  if ( args.size() != 3 ) {
    std::cout << "ERROR : passed bad option, expected:\n"
	      << "        ./iocan --actel <executable> <action> <dat_file>\n"
	      << std::endl;
    return;
  }

  this->actel( args.at(0), args.at(1), args.at(2), type );
}


void dtcan::CuOFThreadCommand::adcCLI( dtcan::TYPE type )
{

  switch ( type ) {
  case dtcan::RO : {
    this->readAllRoAdc();
    this->printAdcMap( false );
    break; }
  case dtcan::TRG : {
    this->readAllTrgAdc();
    this->printAdcMap( true );
    break; }
  case dtcan::ANY : {
    this->readAllAdc();
    this->printAdcMap();
    break; }
  default : {
    std::cout << "Passed bad --board option" << std::endl;
  }
  }

}



void dtcan::CuOFThreadCommand::onetCLI( dtcan::TYPE type )
{

  switch ( type ) {
  case dtcan::RO : {
    this->readAllRoOnets();
    this->printOnetMap( false );
    break; }
  case dtcan::TRG : {
    this->readAllTrgOnets();
    this->printOnetMap( true );
    break; }
  case dtcan::ANY : {
    this->readAllOnets();
    this->printOnetMap();
    break; }
  default : {
    std::cout << "Passed bad --board option" << std::endl;
  }
  }

}



void
dtcan::CuOFThreadCommand::writeOnetCLI( dtcan::TYPE type,
					const std::vector<std::string> & args,
					bool isFullConfig )
{

  //  printf ( "ARGUMENTS : %02hhx %02hhx \n", dtcan::convert(a1), dtcan::convert(a2) );
  if ( args.size() != 2 ) {
    std::cout << "Error: valid format for selection requires two parameters:\n"
	      << "   [MOD] [BIAS]\n"
	      << std::endl;
    return;
  }

  const char * a1 = args.at(0).c_str();
  const char * a2 = args.at(1).c_str();

  switch ( type ) {
  case dtcan::RO : {
    this->writeAllRoOnets( dtcan::convert(a1), dtcan::convert(a2), isFullConfig );
    this->printOnetMap( false );
    break; }
  case dtcan::TRG : {
    this->writeAllTrgOnets( dtcan::convert(a1), dtcan::convert(a2), isFullConfig );
    this->printOnetMap( true );
    break; }
  case dtcan::ANY : {
    this->writeAllOnets( dtcan::convert(a1), dtcan::convert(a2), isFullConfig );
    this->printOnetMap();
    break; }
  default : {
    std::cout << "Passed bad --board option" << std::endl;
  }
  }

}


void
dtcan::CuOFThreadCommand::adcconfCLI( dtcan::TYPE type )
{

  switch ( type ) {
  case dtcan::RO : {
    this->readAdcRoConf();
    this->printAdcConfigMap( false );
    break; }
  case dtcan::TRG : {
    this->readAdcTrgConf();
    this->printAdcConfigMap( true );
    break; }
  case dtcan::ANY : {
    this->readAdcConf();
    this->printAdcConfigMap();
    break; }
  default : {
    std::cout << "Passed bad --board option" << std::endl;
  }
  }

}




