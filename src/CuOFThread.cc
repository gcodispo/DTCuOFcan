
#include "CuOFThread.h"

dtcan::CuOFCan * dtcan::CuOFThread::can_ = 0;

void dtcan::CuOFThread::init( const std::string & canconf,
			      const std::string & boardconf,
			      const std::string & adcconf )
{
  can_ = new dtcan::CuOFCan( canconf, boardconf, adcconf );
}


void dtcan::CuOFThread::init( const std::string & dbKey )
{
  can_ = new dtcan::CuOFCan( dbKey );
}


void dtcan::CuOFThread::destroy()
{
  delete can_;
  can_ = 0;
}


void dtcan::readAllAdc::ThreadWork()
{
  printf("Thread %d starting...\n", id_);

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_->begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_->end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( thisboard.getSocketId() != id_ ) continue;
    if ( ( thisboard.isTrg() && roOnly_ )
	 || ( thisboard.isRo() && trgOnly_ ) ) continue;

    adconetmap_[it->first] = new dtcan::adconet[dtcan::CuOFBoard::ntotonets];

    try {
      thisboard.readAllAdc( adconetmap_[it->first] );
    } catch ( const std::exception & t ) {
      std::cout << it->first << " : " << t.what() << std::endl;
    }

  }

  can_->release( id_ );
}



void dtcan::readAdcConf::ThreadWork()
{
  printf("Thread %d starting...\n", id_);

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_->begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_->end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( thisboard.getSocketId() != id_ ) continue;
    if ( ( thisboard.isTrg() && roOnly_ )
	 || ( thisboard.isRo() && trgOnly_ ) ) continue;

    adcmap_[it->first] = new dtcan::adc[dtcan::CuOFBoard::nadc];

    for ( size_t i = 0; i < dtcan::CuOFBoard::nadc; ++i ) {

      try {
	thisboard.readAdcConf( i, (adcmap_[it->first])[i] );
      } catch ( const std::exception & t ) {
	std::cout << it->first << " : " << t.what() << std::endl;
      }

    }
  }

  can_->release( id_ );
}




void dtcan::readAllOnets::ThreadWork()
{
  printf("Thread %d starting...\n", id_);

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_->begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_->end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( thisboard.getSocketId() != id_ ) continue;
    if ( ( thisboard.isTrg() && roOnly_ )
	 || ( thisboard.isRo() && trgOnly_ ) ) continue;

    onetmap_[it->first] = new dtcan::onet [dtcan::CuOFBoard::ntotonets];

    try {
      thisboard.readAllOnets( onetmap_[it->first] );
    } catch ( const std::exception & t ) {
      std::cout << it->first << " : " << t.what() << std::endl;
    }
    
  }

  can_->release( id_ );
}



void dtcan::writeAllOnets::ThreadWork()
{
  printf("Thread %d starting...\n", id_);

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_->begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_->end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( thisboard.getSocketId() != id_ ) continue;
    if ( ( thisboard.isTrg() && roOnly_ )
	 || ( thisboard.isRo() && trgOnly_ ) ) continue;

    onetmap_[it->first] = new dtcan::onet [dtcan::CuOFBoard::ntotonets];
    dtcan::onet * params = onetmap_[it->first];

    for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
      if ( thisboard.isActiveMezzanine( i ) ) {
	for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
	  size_t idx = i * 8 + j;
	  params[idx].mezzanine = i;
	  params[idx].nonet = j;
	  params[idx].control = 0xd2;
	  params[idx].modulation = mod_;
	  params[idx].bias = bias_;
	  params[idx].equalizer = 0x0;
	}
      }
    }
    
    try {
      if ( isFullConfig_ )
	thisboard.configureOnets( params, dtcan::CuOFBoard::ntotonets );
      else
	thisboard.configOnetsFromRam( params, dtcan::CuOFBoard::ntotonets );
    } catch ( const std::exception & t ) {
      std::cout << it->first << " : " << t.what() << std::endl;
    }

  }

  can_->release( id_ );
}


////////////////////////////////////////////
/// this is the set of commands doing popen
/// for firmware related issues
////////////////////////////////////////////


void dtcan::actel::ThreadWork()
{
  printf("Thread %d starting...\n", id_);

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_->begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_->end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( thisboard.getSocketId() != id_ ) continue;
    if ( ( thisboard.isTrg() && roOnly_ )
	 || ( thisboard.isRo() && trgOnly_ ) ) continue;

    try {
//       std::cout << ( thisboard.isTrg() ? " trg " : " ro ")
// 		<< (thisboard.isRo() ? " ro " : " trg ")
// 		<< (force2nd_ ? " 2nd " : " 1st ")
// 		<< it->first << std::endl;
      thisboard.actel( exec_, action_, file_dat_, force2nd_ );
    } catch ( const std::exception & t ) {
      std::cout << it->first << " : " << t.what() << std::endl;
    }
    
  }

  can_->release( id_ );
}
