#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include "CuOFBoard.h"
#include "CuOFCan.h"
#include "CuOFCommand.h" 
#include "CanErrors.h"
#include <iterator>

using namespace dtcan;
std::string CuOFCommand::result[4]  = { "", "READ", "WRITE", "COMPARE" };
std::string CuOFCommand::master[4]  = { "MASTER",
					"BACKUP, KillMaster disabled",
					"BACKUP, KillMaster enabled",
					"ERROR" };




void dtcan::CuOFCommand::selectionError()
{
  std::cout << "Error: valid format for selection is:\n"
	    << "   { [MEZZANINE] { [ONET] } }\n"
	    << "    Valid <MEZZANINE> from 0 to 3\n"
	    << "    Valid <ONET> from 0 to 7\n"
	    << std::endl;
}



void dtcan::CuOFCommand::writeError()
{
  std::cout << "Error: valid format for selection is:\n"
	    << "   { [MEZZANINE] { [ONET] } } [MOD] [BIAS]\n"
	    << "    Valid <MEZZANINE> from 0 to 3\n"
	    << "    Valid <ONET> from 0 to 7\n"
	    << std::endl;
}



void dtcan::CuOFCommand::channelError()
{
  std::cout << "Error: valid format for selection is:\n"
	    << "   { [ADC] { [CHAN] } }\n"
	    << "    Valid <ADC> from 0 to 7\n"
	    << "    Valid <CHAN> from 0 to 11\n"
	    << std::endl;
}

void
dtcan::CuOFCommand::applySelection( enum SELECTION sel,
				    const std::vector<std::string> & subset,
				    bool force2nd )
{

  sids_.clear();
  nSockets_ = 0;
  force2nd_ = force2nd;
  force2nd_ ? CuOFBoard::switchTo2nd() : CuOFBoard::switchTo1st();

  std::map< std::string, CtcpSocket * >::iterator it = can_.canBegin();
  std::map< std::string, CtcpSocket * >::iterator end = can_.canEnd();

  for ( ; it != end; ++ it ) {
    int tid = it->second->id;
    bool good  = true;
    switch ( sel ) {
    case CRATE : {
      std::string name = it->first;
      std::find( subset.begin(), subset.end(), name );
      good = ( std::find( subset.begin(), subset.end(), name ) != subset.end() );
      break; }
    case WHEEL : {
      std::string crate = it->second->wheel;
      good = ( std::find( subset.begin(), subset.end(), crate ) != subset.end() );
      break; }
    case ALL : {
      good = true;
      break; }
    }
    if ( good ) {
      sids_.push_back( tid );
      ++nSockets_;
    }
  }
}

bool dtcan::CuOFCommand::select( const dtcan::CuOFBoard & thisboard,
				 enum TYPE type )
{
  bool good = ( nSockets_ ) ? 
    ( std::find(sids_.begin(), sids_.end(), thisboard.getSocketId()) != sids_.end() )
    :
    true;
  
  if ( good ) {
    switch ( type)  {
    case RO : good &= thisboard.isRo(); break;
    case TRG : good &= thisboard.isTrg(); break;
    default : break;
    }
  }
  return good;
}


void dtcan::CuOFCommand::queryCLI( const std::string & board,
				   dtcan::TYPE type,
				   const std::vector<std::string> & args,
				   bool isOnet, bool isConf ) 
{

  size_t nargs = args.size();
  if ( nargs > 3 ) {
    printf( "ERROR : passed bad BOARD specifications\n" );
    selectionError();
    return;
  }

  // std::cout << nargs <<  ' ' << (isOnet ? 'Y' :'N' )
  // 	    <<  ' ' << (isConf ? 'Y' :'N' )
  // 	    <<  ' ' << (isConf ? 'Y' :'N' )
  // 	    <<  ' ' << board
  // 	    << std::endl;

  if ( nargs == 0 ) {
    if ( isOnet ) {
      if ( isConf ) this->configOnetsFromDB( board );
      else this->readAllOnets( board );
    } else {
      if ( isConf ) this->readAdcConf( board );
      else this->readAllAdc( board );
    }
  } else if ( nargs == 1 ) {
    const char * a1 = args.at(0).c_str();
    if ( isdigit( a1[0] ) ) {
      if ( isOnet ) {
	if ( isConf ) this->configOnetsFromDB( board, dtcan::convert(a1) );
	else this->readMezzanineOnets( board, dtcan::convert(a1) );
      } else {
	if ( isConf ) this->readAdcConf( board, dtcan::convert(a1) );
	else this->readMezzanineAdc( board, dtcan::convert(a1) );
      }
    } else {
      printf( "ERROR : passed bad mezzanine/ONET number\n" );
      selectionError();
    }
  } else if ( nargs == 2 ) {
    const char * a1 = args.at(0).c_str();
    const char * a2 = args.at(1).c_str();
    if ( isdigit( a1[0] ) && isdigit( a2[0] ) ) {
      if (isOnet ) {
	if ( isConf ) this->configOnetFromDB( board, dtcan::convert(a1), dtcan::convert(a2) );
	else this->readOnet( board, dtcan::convert(a1), dtcan::convert(a2) );
      } else {
	if ( isConf ) this->readAdcChan( board, dtcan::convert(a1), dtcan::convert(a2) );
	else this->readOnetAdc( board, dtcan::convert(a1), dtcan::convert(a2) );
      }
    } else {
      printf( "ERROR : passed bad mezzanine/ONET number\n" );
      selectionError();
    }
  }
  
}


void dtcan::CuOFCommand::writeOnetCLI( const std::string & board,
				       dtcan::TYPE type,
				       const std::vector<std::string> & args,
				       bool isFullConfig ) 
{

  //  printf ( "%02hhx %02hhx \n", dtcan::convert(a1), dtcan::convert( a2) );


  size_t nargs = args.size();
  if ( nargs < 2  || nargs > 4 ) {
    printf( "ERROR : passed bad BOARD specifications\n" );
    selectionError();
    return;
  }

  bool bad = false;
  //std::cout << "ARGUMENTS " << n << std::endl;
  //  printf ( "ARGUMENTS : %02hhx %02hhx \n", dtcan::convert(a1), dtcan::convert(a2) );

  if ( nargs == 2 ) {
    const char * a1 = args.at(0).c_str();
    const char * a2 = args.at(1).c_str();
    if ( isdigit( a1[0] ) && isdigit( a2[0] ) ) {
      this->writeBoardOnets( board, dtcan::convert(a1),
			     dtcan::convert(a2), isFullConfig );
    } else {
      bad = true;
    }
  } else if ( nargs == 3 ) {
    const char * a1 = args.at(0).c_str();
    const char * a2 = args.at(1).c_str();
    const char * a3 = args.at(2).c_str();
    if ( isdigit( a1[0] ) && isdigit( a2[0] ) && isdigit( a3[0] ) ) {
      this->writeMezzanineOnets( board, dtcan::convert(a1),
				 dtcan::convert(a2), dtcan::convert(a3),
				 isFullConfig );
    } else {
      bad = true;
    }
  } else if ( nargs ==  4 ) {

    const char * a1 = args.at(0).c_str();
    const char * a2 = args.at(1).c_str();
    const char * a3 = args.at(2).c_str();
    const char * a4 = args.at(3).c_str();
    if ( isdigit( a1[0] ) && isdigit( a2[0] ) && isdigit( a3[0] ) && isdigit( a4[0] ) ) {
      this->writeOnet( board, dtcan::convert(a1), dtcan::convert(a2),
		       dtcan::convert(a3), dtcan::convert(a4),
		       isFullConfig );
    } else {
      bad = true;
    }
  } else {
    bad = true;
  }

  if ( bad ) {
    printf( "ERROR : passed bad arguments\n" );
    writeError();
  }

}





//////////////////////////////////
/// ADC monitoring
//////////////////////////////////

/// read single adc chanel
void CuOFCommand::readAdcChan( const std::string & board, char nadc, char nchan )
{
  int ret = can_[board].readAdcChan( nadc, nchan );
  std::cout << "*** readAdcChan for board " << board
	    << " returned : " << ret  << std::endl;
}

/// read single adc
void CuOFCommand::readOnetAdc( const std::string & board, char mezz, char onet )
{
  dtcan::adconet reply;
  can_[board].readOnetAdc( mezz, onet, reply );
  std::cout << board << '\t' << reply;
}

/// read single adc
void CuOFCommand::readMezzanineAdc( const std::string & board, char mezz )
{
  dtcan::adconet reply[dtcan::CuOFBoard::ntotonets];
  can_[board].readMezzanineAdc( mezz, reply );
  for ( size_t i = 0; i < dtcan::CuOFBoard::nonets; ++i ) {
    if ( can_[board].isActiveMezzanine( reply[i] ) ) {
      std::cout << board << '\t' << reply[i];
    } else {
      std::cout << board << " mezzanine " << (0xff & mezz )
		<< "  is not active. Skipping." <<std::endl;
    }
  }
}


/// read all ADC
void CuOFCommand::readAllAdc( const std::string & board )
{
  dtcan::adconet reply[dtcan::CuOFBoard::ntotonets];
  can_[board].readAllAdc( reply );
  for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
    if ( can_[board].isActiveMezzanine( reply[i] ) )
      std::cout << board << '\t' << reply[i];
  }
  // std::cout << can_[board].readAdc( adc, 12 ) << '\n';
}


/// read all ADC
void CuOFCommand::readAllAdc2( const std::string & board )
{
  dtcan::adconet reply;
  for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
    for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
      try {
	can_[board].readOnetAdc( i, j, reply );
	std::cout << board << '\t' << reply;
      } catch ( const std::exception & e ) {
	std::cout << board << " : " << e.what() << std::endl;
      }
    }
  }
}


/// read all ADC
void CuOFCommand::readAllAdc( enum TYPE type )
{

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( select( thisboard, type ) ) {
      dtcan::adconet reply[dtcan::CuOFBoard::ntotonets];

      try {
	thisboard.readAllAdc( reply );
      } catch ( const std::exception & t ) {
	std::cout << it->first << " : " << t.what() << std::endl;
      }

      // thisboard.readAllTemp( reply );
      for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
	if ( thisboard.isActiveMezzanine( reply[i] ) )
	  std::cout << it->first << '\t' << reply[i];
      }
    }
  }
  // std::cout << can_[board].readAdc( adc, 12 ) << '\n';
}


//////////////////////////////////
// read ADC chan conf
void CuOFCommand::readAdcConf( const std::string & board, char nadc )
{
  dtcan::adc reply;
  can_[board].readAdcConf( nadc, reply );
  std::cout << board << " [" << nadc << "]\t" << reply << '\n';
}

// read ADC chan conf
void CuOFCommand::readAdcConf( const std::string & board )
{
  for ( size_t i = 0; i < dtcan::CuOFBoard::nadc; ++i ) {
    dtcan::adc reply;
    can_[board].readAdcConf( i, reply );
    std::cout << board << " [" << i << "]\t" << reply << '\n';
  }
}


// read ADC chan conf
void CuOFCommand::readAdcConf( enum TYPE type )
{

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( select( thisboard, type ) ) {

      for ( size_t i = 0; i < dtcan::CuOFBoard::nadc; ++i ) {
	dtcan::adc reply;

	try {
	  it->second.readAdcConf( i, reply );
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}

	std::cout << it->first << " [" << i << "]\t" << reply << '\n';
      }
    }
  }

}


//////////////////////////////////
/// ONETS monitoring
//////////////////////////////////

void CuOFCommand::readOnet( const std::string & board, char mezz, char nonet )
{
  dtcan::onet reply;
  can_[board].readOnet( mezz, nonet, reply );
  std::cout << board << '\t' << reply;
}



void CuOFCommand::readMezzanineOnets( const std::string & board, char mezz )
{
  dtcan::onet reply[dtcan::CuOFBoard::nonets];
  can_[board].readMezzanineOnets(mezz, reply);

  for ( size_t i = 0; i < dtcan::CuOFBoard::nonets; ++i ) {
    if ( can_[board].isActiveMezzanine( reply[i] ) )
      std::cout << board << '\t' << reply[i];
  }
}



void CuOFCommand::readAllOnets( const std::string & board )
{
  dtcan::onet reply[dtcan::CuOFBoard::ntotonets];
  can_[board].readAllOnets(reply);

  for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
    if ( can_[board].isActiveMezzanine( reply[i] ) )
      std::cout << board << '\t' << reply[i];
  }
}


void CuOFCommand::readAllOnets2( const std::string & board )
{
  
  can_[board].flashToRam();

  dtcan::onet reply;
  for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
    for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
      try {
	can_[board].readOnet( i, j, reply );
	std::cout << board << '\t' << reply;
      } catch ( const std::exception & e ) {
	std::cout << board << " : " << e.what() << std::endl;
      }
    }
  }
}


void CuOFCommand::readAllOnets( enum TYPE type )
{

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( select( thisboard, type ) ) {

      dtcan::onet reply[dtcan::CuOFBoard::ntotonets];

      try {
	thisboard.readAllOnets( reply );
      } catch ( const std::exception & t ) {
	std::cout << it->first << " : " << t.what() << std::endl;
      }

      // thisboard.readAllTemp( reply );
      for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
	if ( thisboard.isActiveMezzanine( reply[i] ) )
	  std::cout << it->first << '\t' << reply[i];
      }
    }
  }

}




//////////////////////////////////
/// ONETS monitoring
//////////////////////////////////

void CuOFCommand::configOnetFromDB( const std::string & board,
				    char mezz, char nonet )
{
  can_[board].configOnetFromDB( mezz, nonet );
}



void CuOFCommand::configOnetsFromDB( const std::string & board, char mezz )
{
  can_[board].configOnetsFromDB( mezz );
}


void CuOFCommand::configOnetsFromDB( const std::string & board,
				     enum TYPE type )
{

  if ( board.empty() ) {
    std::cout << "########### configOnetsFromDB" << std::endl;
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

    for ( ; it != end; ++it ) {
    
      std::cout << "########### " << it->first << std::endl;
      dtcan::CuOFBoard & thisboard = it->second;
      if ( select( thisboard, type ) ) {
	try {
	  thisboard.configOnetsFromDB();
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    can_[board].configOnetsFromDB();
  }

}


// void CuOFCommand::compareAllOnets( enum TYPE type )
// {

//   std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
//   std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

//   for ( ; it != end; ++it ) {

//     dtcan::CuOFBoard & thisboard = it->second;
//     if ( select( thisboard, type ) ) {

//       dtcan::onet reply[dtcan::CuOFBoard::ntotonets];

//       try {
// 	thisboard.readAllOnets( reply );
//       } catch ( const std::exception & t ) {
// 	std::cout << it->first << " : " << t.what() << std::endl;
//       }

//       // thisboard.readAllTemp( reply );
//       for ( size_t i = 0; i < dtcan::CuOFBoard::ntotonets; ++i ) {
// 	if ( thisboard.isActiveMezzanine( reply[i] ) )
// 	  std::cout << it->first << '\t' << reply[i];
//       }
//     }
//   }

// }

//////////////////////////////////
/// ONETS config
//////////////////////////////////


void CuOFCommand::writeOnet( const std::string & board, char mezz, char nonet,
			     short mod, short bias, bool isFullConfig )
{

  dtcan::CuOFBoard & thisboard = can_[board];

  if ( ! thisboard.isActiveMezzanine( mezz ) ) {
    std::cout << board << " mezzanine " << (0xff & mezz )
	      << "  is not active. Skipping." <<std::endl;
    return;
  }

  
  dtcan::onet params[1];
  params[0].mezzanine = mezz;
  params[0].nonet = nonet;
  params[0].control = 0xd2;
  params[0].modulation = mod;
  params[0].bias = bias;
  params[0].equalizer = 0x0;

  std::cout << "write onet parameters : \n";
  std::cout << board << '\t' << params[0] << std::endl;
  if ( isFullConfig )
    thisboard.configureOnets( params, 1 );
  else 
    thisboard.configOnetsFromRam( params, 1 );

  // thisboard.readOnet( mezz, nonet, params[0] );
  // std::cout << "read onet parameters : \n";
  // std::cout << board << '\t' << params[0] << std::endl;

}




void CuOFCommand::writeMezzanineOnets( const std::string & board, char mezz,
				       short mod, short bias, bool isFullConfig )
{
  
  dtcan::onet params[dtcan::CuOFBoard::nonets];
  dtcan::CuOFBoard & thisboard = can_[board];

  if ( ! thisboard.isActiveMezzanine( mezz ) ) {
    std::cout << board << " mezzanine " << (0xff & mezz )
	      << "  is not active. Skipping." << std::endl;
    return;
  }

  size_t idx = 0;
  for ( size_t i = 0; i < dtcan::CuOFBoard::nonets; ++i ) {
    params[idx].mezzanine = mezz;
    params[idx].nonet = i;
    params[idx].control = 0xd2;
    params[idx].modulation = mod;
    params[idx].bias = bias;
    params[idx].equalizer = 0x0;
    ++idx;
  }


  std::cout << "write onet parameters : \n";
  std::cout << board << '\t' << params[0] << std::endl;
  if ( isFullConfig )
    thisboard.configureOnets( params, idx );
  else 
    thisboard.configOnetsFromRam( params, idx );

  //   thisboard.readMezzanineOnets( mezz, params );
  //   std::cout << "read onet parameters : \n";

}


void CuOFCommand::writeBoardOnets( const std::string & board, short mod,
				   short bias, bool isFullConfig )
{

  dtcan::onet params[dtcan::CuOFBoard::ntotonets];
  dtcan::CuOFBoard & thisboard = can_[board];

  size_t idx = 0;

  for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
    if ( thisboard.isActiveMezzanine( i ) ) {
      for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
	// size_t idx = i * 8 + j;
	params[idx].mezzanine = i;
	params[idx].nonet = j;
	params[idx].control = 0xd2;
	params[idx].modulation = mod;
	params[idx].bias = bias;
	params[idx].equalizer = 0x0;
	++idx;
      }
    }
  }

  std::cout << "writing onet parameters : " << board << "..." << std::endl;
  if ( isFullConfig )
    thisboard.configureOnets( params, idx );
  else 
    thisboard.configOnetsFromRam( params, idx );

}


void CuOFCommand::writeAllOnets( const std::string & board, short mod,
				 short bias, bool isFullConfig )
{
  
  dtcan::onet params[dtcan::CuOFBoard::ntotonets];

  for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
    if ( can_[board].isActiveMezzanine( i ) ) {
      for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
	size_t idx = i * 8 + j;
	params[idx].mezzanine = i;
	params[idx].nonet = j;
	params[idx].control = 0xd2;
	params[idx].modulation = mod;
	params[idx].bias = bias;
	params[idx].equalizer = 0x0;
      }
    }
  }

  std::cout << "write onet parameters : \n";
  if ( isFullConfig )
    can_[board].configureOnets( params, dtcan::CuOFBoard::ntotonets );
  else 
    can_[board].configOnetsFromRam( params, dtcan::CuOFBoard::ntotonets );

  //   can_[board].readAllOnets( params );
  //   std::cout << "read onet parameters : \n";

}

void CuOFCommand::writeAllOnets( short mod, short bias, enum TYPE type,
				 bool isFullConfig )
{

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();

  for ( ; it != end; ++it ) {

    dtcan::CuOFBoard & thisboard = it->second;
    if ( select( thisboard, type ) ) {
      dtcan::onet params[dtcan::CuOFBoard::ntotonets];
      size_t idx = 0;

      for ( size_t i = 0; i < dtcan::CuOFBoard::nmezzanines; ++i ) {
	if ( thisboard.isActiveMezzanine( i ) ) {
	  for ( size_t j = 0; j < dtcan::CuOFBoard::nonets; ++j ) {
	    // size_t idx = i * 8 + j;
	    params[idx].mezzanine = i;
	    params[idx].nonet = j;
	    params[idx].control = 0xd2;
	    params[idx].modulation = mod;
	    params[idx].bias = bias;
	    params[idx].equalizer = 0x0;
	    ++idx;
	  }
	}
      }

      std::cout << "writing onet parameters : " << it->first << "..." << std::endl;

      try {
	if ( isFullConfig )
	  thisboard.configureOnets( params, idx );
	else 
	  thisboard.configOnetsFromRam( params, idx );
      } catch ( const std::exception & t ) {
	std::cout << it->first << " : " << t.what() << std::endl;
      }
    }
  }

}


//////////////////////////////////
/// ONETS config
//////////////////////////////////

void CuOFCommand::writeRamToFlash( const std::string & name, dtcan::CuOFBoard & board )
{
  try {
    int ret = board.ramToFlash();
    std::cout << "*** writeRamToFlash for board " << name
	      << " returned : " << result[ret] << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** writeRamToFlash for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::writeRamToFlash( const std::string & board,
				   enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) writeRamToFlash( it->first, it->second );
    }
  } else {
    writeRamToFlash( board, can_[board] );
  }
}


void CuOFCommand::writeFlashToRam( const std::string & name,
				   dtcan::CuOFBoard & board )
{
  try {
    int ret = board.flashToRam();
    std::cout << "*** writeFlashToRam for board " << name
	      << " returned : " << result[ret] << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** writeFlashToRam for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::writeFlashToRam( const std::string & board,
				   enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it )  {
      if ( select( it->second, type ) ) writeFlashToRam( it->first, it->second );
    }
  } else {
    writeFlashToRam( board, can_[board] );
  }
}



void CuOFCommand::diffOnetsWithDB( const std::string & name,
				   dtcan::CuOFBoard & board )
{
  try {
    int ret = board.diffOnetsWithDB();
    std::cout << "*** diffOnetWithDB for board " << name
	      << " returned : " << ret << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** diffOnetWithDB for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::diffOnetsWithDB( const std::string & board,
				  enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it )  {
      if ( select( it->second, type ) ) diffOnetsWithDB( it->first, it->second );
    }
  } else {
    diffOnetsWithDB( board, can_[board] );
  }
}



void CuOFCommand::onetsFromRamToDB( const std::vector<std::string> & args )
{
  
  if ( args.size() > 1 ) {
    std::cout << "Bad option for --onetsFromRamToDB: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  [DBKEY] : new DB key"
	      << std::endl;
    return;
  }

  const std::string keyname = args.at(0);

  std::cout << "Reading from RAM...." << std::endl;
  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
  for ( ; it != end; ++it ) it->second.loadOnetFromRam();

  std::cout << "Saving to DB...." << std::endl;
  can_.insertNewKey( keyname );

}



void CuOFCommand::dumpOnetsFromDB()
{

  std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
  std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
  for ( ; it != end; ++it ) {

    for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
      dtcan::CuOFBoard & thisboard = it->second;
      if ( thisboard.isActiveMezzanine( mezz ) ) {
	for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	  std::cout << it->first << ' ' << int( mezz ) << ' ' << int( nonet )
		    << ' ' << int( thisboard.getOnetModulation( mezz, nonet ) )
		    << ' ' << int( thisboard.getOnetBias( mezz, nonet ) )
		    << std::endl;
	}
      }
    }
  }
}

void CuOFCommand::restoreFromFlash( const std::string & name,
				    dtcan::CuOFBoard & board )
{
  try {
    board.restoreFromFlash();
    std::cout << "*** restoreFromFlash for board " << name
	      << " returned " << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** restoreFromFlash for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::restoreFromFlash( const std::string & board,
				    enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it )  {
      if ( select( it->second, type ) ) restoreFromFlash( it->first, it->second );
    }
  } else {
    restoreFromFlash( board, can_[board] );
  }
}



void CuOFCommand::compareRamToFlash( const std::string & name, dtcan::CuOFBoard & board )
{
  try {
    int ret = board.compareRamToFlash();
    std::cout << "*** compareRamToFlash for board " << name
	      << " returned : " << result[ret] << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** compareRamToFlash for board " << name
	      << " returned Network time out..." << std::endl;
  }
}



void CuOFCommand::compareRamToFlash( const std::string & board,
				     enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) compareRamToFlash( it->first, it->second );
    }
  } else {
    compareRamToFlash( board, can_[board] );
  }
}





void CuOFCommand::writeRamToOnet( const std::string & name, dtcan::CuOFBoard & board )
{
  try {
    int ret = board.writeRamToOnet();
    std::cout << "*** writeRamToOnet for board " << name
	      << " returned : " << ( ret ? "OK" : "FAIL" ) << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** writeRamToOnet for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::writeRamToOnet( const std::string & board,
				  enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) writeRamToOnet( it->first, it->second );
    }
  } else {
    writeRamToOnet( board, can_[board] );
  }
}




void CuOFCommand::readRamBlockAtStartup( const std::string & name, dtcan::CuOFBoard & board )
{
  try {
    board.setRamBlockAtStartup();
    int ret = board.readRamBlockAtStartup();
    std::cout << "*** readRamBlockAtStartup for board " << name
	      << " returned : " << result[ret] << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** setRamBlockAtStartup for board " << name
	      << " returned " << t.what() << std::endl;
  }
}



void CuOFCommand::readRamBlockAtStartup( const std::string & board,
					 enum TYPE type )
{
  
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) readRamBlockAtStartup( it->first, it->second );
    }
  } else {
    readRamBlockAtStartup( board, can_[board] );
  }
}


void CuOFCommand::version( const std::string & name, dtcan::CuOFBoard & board )
{
  try {
    dtcan::v_time fw = board.readFwVersion();
    dtcan::v_time sw = board.readSwVersion();
    std::cout << "*** " << name << " *** Firmware version returned : "
	      << std::hex << fw.day   << "/"
	      << fw.month << "/"
	      << fw.year  << " "
	      << fw.hour
	      << std::endl;
    std::cout << "*** " << name << " *** Software version returned : "
	      << std::hex << sw.day   << "/"
	      << sw.month << "/"
	      << sw.year  << " "
	      << sw.hour
	      << std::endl;
  } catch ( const std::exception & t ) {
    std::cout << "*** version for board " << name
	      << " returned " << t.what() << std::endl;
  }
}


void CuOFCommand::version( const std::string & board,
			   enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) version( it->first, it->second );
    }
  } else {
    version( board, can_[board] );
  }
}




void CuOFCommand::writeId( const std::string & board )
{
  can_[board].writeId();
}



void CuOFCommand::readId( const std::string & board,
			  enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	int bid = -1;
	try {
	  bid = it->second.readId();
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
	std::cout << it->first << '\t' << bid << " ("
		  << std::hex << std::showbase << bid << ")"
		  << std::dec << std::noshowbase << std::endl;
      }
    }
  } else {
    int bid = can_[board].readId();
    std::cout << board << '\t' << bid << " ("
	      << std::hex << std::showbase << bid << ")"
	      << std::dec << std::noshowbase << std::endl;
  }
}





void CuOFCommand::killMaster( const std::string & board,
			      enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	int bid = -1;
	try {
	  bid = it->second.killMaster();
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
	std::cout << it->first << '\t' << CuOFCommand::master[bid] << std::endl;
      }
    }
  } else {
    std::cout << board << '\t' << CuOFCommand::master[can_[board].killMaster()] << std::endl;
  }
}


void CuOFCommand::restoreMaster( const std::string & board,
				 enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	int bid = -1;
	try {
	  bid = it->second.restoreMaster();
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
	std::cout << it->first << '\t' << CuOFCommand::master[bid] << std::endl;
      }
    }
  } else {
    std::cout << board << '\t' << CuOFCommand::master[can_[board].restoreMaster()] << std::endl;
  }
}


void CuOFCommand::statusBackup( const std::string & board,
				enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	int bid = -1;
	try {
	  bid = it->second.statusBackup();
	  std::cout << it->first << '\t' << CuOFCommand::master[bid] << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    std::cout << board << '\t' << CuOFCommand::master[can_[board].statusBackup()] << std::endl;
  }
}


void CuOFCommand::statusMaster( const std::string & board,
				enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	int bid = -1;
	try {
	  bid = it->second.statusMaster();
	  std::cout << it->first << '\t' << CuOFCommand::master[bid] << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    std::cout << board << '\t' << CuOFCommand::master[can_[board].statusMaster()] << std::endl;
  }
}



void CuOFCommand::reset( const std::string & board,
			 enum TYPE type )
{
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  it->second.reset();
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
	std::cout << it->first << " reset" << std::endl;
      }
    }
  } else {
    can_[board].reset();
    std::cout << board << " reset"<< std::endl;
  }
}
















void CuOFCommand::cmd( const std::string & board, 
		       const std::vector<std::string> & args,
		       enum TYPE type )
{

  if ( args.size() < 5 ) {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  read/write indexHigh indexLow subIndex data[0]"
	      << std::endl;
    return;
  }


  bool read = true;
  if ( args.at(0) == "read" ) read = true;
  else if ( args.at(0) == "write" ) read = false; 
  else {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  read/write indexHigh indexLow data[0]"
	      << std::endl;
    return;
  }
  
  unsigned short iHigh = dtcan::convert( args.at(1).c_str() );
  unsigned short iLow = dtcan::convert( args.at(2).c_str() );
  unsigned short subIndex = dtcan::convert( args.at(3).c_str() );
  unsigned short data = dtcan::convert( args.at(4).c_str() );
    
  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  std::vector<unsigned short> ret_val =
	    it->second.cmd( read, iHigh, iLow, subIndex, data );

	  std::vector<unsigned short>::const_iterator sit = ret_val.begin();
	  std::vector<unsigned short>::const_iterator sitend = ret_val.end();

	  std::cout << it->first << '\t';
	  for ( ; sit != sitend; ++sit ) printf("0x%02X\t", (*sit) );
	  printf("0x%02X", 1 );
	  std::cout << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    std::vector<unsigned short> ret_val =
	    can_[board].cmd( read, iHigh, iLow, subIndex, data );
    std::vector<unsigned short>::const_iterator sit = ret_val.begin();
    std::vector<unsigned short>::const_iterator sitend = ret_val.end();
    std::cout << board << "\t";
    for ( ; sit != sitend; ++sit ) printf("0x%02X\t", (*sit) );
    std::cout << std::endl;
    std::cout << std::endl;
  }
}






void CuOFCommand::panic( enum SELECTION sel,
			 const std::vector<std::string> & subset,
			 const std::vector<std::string> & args )
{

  if ( args.size() < 5 ) {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  read/write indexHigh indexLow subIndex data[0]"
	      << std::endl;
    return;
  }


  bool read = true;
  if ( args.at(0) == "read" ) read = true;
  else if ( args.at(0) == "write" ) read = false; 
  else {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  read/write indexHigh indexLow data[0]"
	      << std::endl;
    return;
  }


  unsigned short iHigh = dtcan::convert( args.at(1).c_str() );
  unsigned short iLow = dtcan::convert( args.at(2).c_str() );
  unsigned short subIndex = dtcan::convert( args.at(3).c_str() );
  unsigned short data = dtcan::convert( args.at(4).c_str() );

  std::string crate;
  if ( sel == CRATE && subset.size() == 1 ) {
    crate = subset.at(0);
  } else {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  read/write indexHigh indexLow data[0]"
	      << std::endl;
    return;
  }

  /// PANIC MODE
  std::string id = "PANIC";
  char trg = true;
  char base = 0xff;
  char mask = 255;
  dtcan::CtcpSocket * cansock = can_.getSocket( crate );
  dtcan::CtcpSocket * cansockKM = can_.getSocket( "KM_" + crate );
  dtcan::CuOFBoard board = CuOFBoard( base, mask, trg, id, cansock, cansockKM );
  if ( ! select( board, ANY ) ) {
    std::cout << "Bad option for --cmd: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Explicitely select one crate through --crates option"
	      << std::endl;
    return;
  }

  ////
    
  std::vector<unsigned short> ret_val =
    board.cmd( read, iHigh, iLow, subIndex, data );
  std::vector<unsigned short>::const_iterator sit = ret_val.begin();
  std::vector<unsigned short>::const_iterator sitend = ret_val.end();
  std::cout << "PANIC\t";
  for ( ; sit != sitend; ++sit ) printf("0x%02X\t", (*sit) );
  std::cout << std::endl;

}
















void CuOFCommand::silent( const std::string & board, 
			  const std::vector<std::string> & args,
			  enum TYPE type )
{

  if ( args.size() != 1 ) {
    std::cout << "Bad option for --silent: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  enable/disable/read"
	      << std::endl;
    return;
  }

  const std::string & action = args.at(0);
  bool read = false;
  bool enable = false;
  if ( action == "read" ) read = true;
  else if ( action == "enable" ) enable = true;
  else if ( action == "disable" ) enable = false;
  else {
    std::cout << "Bad option for --silent: " << action << std::endl
	      << "   Valid options are:  enable/disable/read"
	      << std::endl;
    return;
  }

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  std::cout << it->first;
	  if ( read ) {
	    parseSilent( it->second.silent() );
	  } else {
	    it->second.silent(enable);
	    std::cout << " Silent " << ( enable ? "Enabled" : "Disabled" );
	  }
	std::cout << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    if ( read ) {
      parseSilent( can_[board].silent() );
    } else {
      can_[board].silent(enable);
      std::cout << " Silent " << ( enable ? "Enabled" : "Disabled" );
    }
    std::cout << std::endl;
  }
}



void CuOFCommand::timer( const std::string & board, 
			 const std::vector<std::string> & args,
			 enum TYPE type )
{
  if ( args.size() > 1 ) {
    std::cout << "Bad option for --timer: " << std::endl;
    std::copy( args.begin(), args.end(),
	       std::ostream_iterator<std::string>(std::cout, "\t") );
    std::cout << std::endl
	      << "   Valid options are:  [EMPTY] : read current value"
	      << "                       [VALUE] : change value with VALUE"
	      << std::endl;
    return;
  }

  bool read = ( args.empty() ? true : false );
  unsigned short t_value = ( args.empty() ? 0
			     : dtcan::convert( args.at(0).c_str() ) );

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  unsigned short ret_time = ( read ? it->second.timer()
				      : it->second.timer( t_value ) );
	  std::cout << it->first << " " << ret_time << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    unsigned short ret_time = ( read ? can_[board].timer() :
				can_[board].timer( t_value ) );
    std::cout << board << " " << ret_time << std::endl;
  }
}








void CuOFCommand::readCanRegisters( const std::string & board, 
				    enum TYPE type )
{

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  std::vector<std::string> ret_val = it->second.readCanRegisters();
	  std::cout << it->first << "\n\t";
	  std::copy( ret_val.begin(), ret_val.end(),
		     std::ostream_iterator<std::string>(std::cout, "\n\t") );
	  std::cout << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    std::vector<std::string> ret_val = can_[board].readCanRegisters();
    std::cout << board << "\n\t";
    std::copy( ret_val.begin(), ret_val.end(),
	       std::ostream_iterator<std::string>(std::cout, "\n\t") );
    std::cout << std::endl;
  }
}


void CuOFCommand::readCanCounters( const std::string & board, 
				   enum TYPE type )
{

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  std::map<std::string, size_t> ret = it->second.readCanCounters();
	  std::cout << it->first << ' ';
	  std::for_each ( ret.begin(), ret.end(), printMapElement );
	  std::cout << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    std::map<std::string, size_t> ret = can_[board].readCanCounters();
    std::cout << board << ' ';
    std::for_each ( ret.begin(), ret.end(), printMapElement );
    std::cout << std::endl;
  }
}



void CuOFCommand::resetCanCounters( const std::string & board, 
				    enum TYPE type )
{

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	try {
	  it->second.resetCanCounters();
	  std::cout << it->first << " CAN Error Counters Reset" << std::endl;
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
      }
    }
  } else {
    can_[board].resetCanCounters();
    std::cout << board << " CAN Error Counters Reset" << std::endl;
  }
}















void dtcan::CuOFCommand::actelCLI( const std::string & board,
				   const std::vector<std::string> & args,
				   dtcan::TYPE type )
{
  if ( args.size() != 3 ) {
    std::cout << "ERROR : passed bad option, expected:\n"
	      << "        ./iocan --actel <executable> <action> <dat_file>\n"
	      << std::endl;
    return;
  }

  this->actel( board, args.at(0), args.at(1), args.at(2), type );
}


void dtcan::CuOFCommand::actel( const std::string & board,
				const std::string & exec,
				const std::string & action,
				const std::string & file_dat,
				enum TYPE type )
{

  if ( board.empty() ) {
    std::map< std::string, dtcan::CuOFBoard >::iterator it = can_.begin();
    std::map< std::string, dtcan::CuOFBoard >::iterator end = can_.end();
    for ( ; it != end; ++it ) {
      if ( select( it->second, type ) ) {
	std::string outfile;
	try {
	  outfile = it->second.actel( exec, action, file_dat );
	} catch ( const std::exception & t ) {
	  std::cout << it->first << " : " << t.what() << std::endl;
	}
	std::cout << it->first << " written " << outfile << std::endl;
      }
    }
  } else {
    std::string outfile = can_[board].actel( exec, action, file_dat, force2nd_ );
    std::cout << board << " written " << outfile << std::endl;
  }
}




