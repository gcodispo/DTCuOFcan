#include "DbObject.h"
#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <exception>
#include <CanErrors.h>


CuOFDb::DbObject::DbObject( const char *DataSource,
			    const char *UserName, const char *Password )
  : 
  //  henv ( SQL_NO_DATA ), hdbc ( SQL_NO_DATA ), hstmt ( SQL_NO_DATA ),
  connct_rc ( SQL_NO_DATA ), query_rc ( SQL_NO_DATA ), connct_time ( 0 )
{
  strncpy( datasource, DataSource, 64 );
  strncpy( user, UserName, 64 );
  strncpy( passwd, Password, 64 );

  // Connect to DATABASE
  connect();
}


CuOFDb::DbObject::DbObject( const std::string & DataSource,
			    const std::string & UserName, const std::string & Password )
  : 
  //  henv ( SQL_NO_DATA ), hdbc ( SQL_NO_DATA ), hstmt ( SQL_NO_DATA ),
  connct_rc ( SQL_NO_DATA ), query_rc ( SQL_NO_DATA ), connct_time ( 0 )
{
  strncpy( datasource, DataSource.c_str(), 64 );
  strncpy( user, UserName.c_str(), 64 );
  strncpy( passwd, Password.c_str(), 64 );

  // Connect to DATABASE
  connect();
}

CuOFDb::DbObject::~DbObject() {
  disconnect(); // Disconnect from DB
}



// Connect to DATABASE
void CuOFDb::DbObject::connect()
{

  char connectstr[256];
  int timeout=DB_TIMEOUT;
  SQLRETURN rc = 0; // return code
  // UCHAR info[STR_LEN]; // info string for SQLGetInfo
  // SQLSMALLINT cbInfoValue;

  if ( SQL_SUCCEEDED(connct_rc) ) {
    return; // already connected
  }

  if (( time(NULL)-connct_time) < DB_MIN_RECONNET_TIME ) {
    return; // Small time since last connection: return
  } else {
    connct_time = time(NULL); // Set to current time
  }

  try {
    SQLAllocHandle(SQL_HANDLE_ENV,SQL_NULL_HANDLE,&henv);
    SQLSetEnvAttr(henv,SQL_ATTR_ODBC_VERSION,(void*)SQL_OV_ODBC3,0); // ODBC3
    SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

    SQLSetConnectAttr(hdbc,SQL_LOGIN_TIMEOUT,(SQLPOINTER)(&timeout),0);
    SQLSetConnectAttr(hdbc,SQL_ATTR_CONNECTION_TIMEOUT,(SQLPOINTER)(&timeout),0);

    if ( strlen(user) != 0 && strlen(passwd) != 0 ) {
      sprintf(connectstr,"DSN=%s; UID=%s; PWD=%s", datasource, user, passwd);
    } else {
      sprintf(connectstr,"DSN=%s",datasource);
    }
    // printf("##### %s\n", connectstr);

    rc = SQLDriverConnect( hdbc,NULL,(SQLCHAR *)connectstr, SQL_NTS,
			   NULL, 0, NULL, SQL_DRIVER_NOPROMPT );

    if ( SQL_SUCCEEDED(rc) ) {
      rc = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);
    } else {
      SQLCHAR SQLState[6], MessageText[257];
      SQLSMALLINT TextLength;
      SQLINTEGER SQLCode;
      SQLGetDiagRec( SQL_HANDLE_DBC,hdbc, 1, SQLState, &SQLCode,
		     MessageText, 256, &TextLength );
      printf( "Connection Error: %s\n", MessageText );
    }
  } catch ( std::exception& e ) {
    printf( "Standard exception: %s\n", e.what() );
  }

  if ( ! SQL_SUCCEEDED(rc) ) {
    throw DbException( "Error in connecting to DB", rc );
  }
  connct_rc = rc;
}


// Disconnect from DATABASE
void CuOFDb::DbObject::disconnect() {
  try {
    // Free handles: otherwise some memory remains unfreed
    SQLFreeHandle(SQL_HANDLE_STMT,hstmt);
    SQLDisconnect(hdbc); // Needed, otherwise connections are not closed!!!
    SQLFreeHandle(SQL_HANDLE_DBC,hdbc);
    SQLFreeHandle(SQL_HANDLE_ENV,henv);
    
//     if ( SQL_SUCCEEDED(connct_rc) )
//       printf("Disconnected from database\n");

    connct_rc = SQL_NO_DATA;
  } catch (std::exception& e) {
    printf("Standard exception: %s\n",e.what());
  }
}

int CuOFDb::DbObject::sqlQuery(char *mquery, int &res_rows, int &res_cols)
{

  res_rows = res_cols = 0;

  // Discard results of the previous query:
  query_rc   = SQL_NO_DATA;
  while (SQLMoreResults(hstmt)!=SQL_NO_DATA) {}

  if (!SQL_SUCCEEDED(connct_rc)) {
    // Connection isn't open
    throw DbException( "Not connected to the database", connct_rc );
  }

  query_rc = SQLExecDirect(hstmt,(SQLCHAR*)mquery, strlen(mquery) ); // Send SQL query
  // printf("CDbObject::sqlquery -- query string \"%s\"\n",mquery);

  if ( checkQuery() ) return -1;

  SQLSMALLINT query_cols;
  SQLLEN query_rows;

  SQLNumResultCols(hstmt, &query_cols); // Number of read columns
  SQLRowCount(hstmt, &query_rows); // Number of rows. NB: with some drivers, query_rows assumes only 0 or 1 (even if more rows are read)

  res_cols = query_cols;
  res_rows = query_rows;
  // printf("CDbObject::sqlquery -- querydone. Found %d rows and %d cols.\n", query_rows, query_cols);

  return 0;

}


// Prepare sql statement, eventually using binding parameters
// Authors: Giuseppe Codispoti
// AP: Modified Feb 2009
int CuOFDb::DbObject::sqlQueryBind( char *mquery, binder *input,
				    unsigned int insize ) 
{

  // Discard results of the previous query:
  query_rc   = SQL_NO_DATA;
  while (SQLMoreResults(hstmt)!=SQL_NO_DATA) {};

  if (!SQL_SUCCEEDED(connct_rc)) {
    // Connection isn't open
    throw DbException( "Not connected to the database", connct_rc );
  }

  // Prepare statement
  query_rc = SQLPrepare(hstmt, (SQLCHAR*)mquery, strlen(mquery));

  if (!SQL_SUCCEEDED(query_rc)) { // Query error
    throw DbException( "QUERY FAILED", query_rc );
  }

  if ( insize != 0 && input != NULL ) {

    for (unsigned int i = 0; i < insize; ++i ) {

      // printf("IN %d %s %d\n", i, input[i].param, input[i].size );

      query_rc =+ SQLBindParameter( hstmt,
				    i+1,  // position of the parameter
				    SQL_PARAM_INPUT,
				    input[i].c_type,
				    input[i].sql_type,
				    input[i].size,
				    0,
				    input[i].param,
				    input[i].size,
				    NULL );
    }
  }

  if (!SQL_SUCCEEDED(query_rc)) { // Query error
    throw DbException( "BIND FAILED", query_rc );
  }

  query_rc = SQLExecute(hstmt);

  return 0;
}


// Execute prepared query, allows binding output buffers
// Returns only the first line of the query!!!
// Authors: Giuseppe Codispoti
int CuOFDb::DbObject::sqlFetchOne( binder *outBindSet, unsigned int outsize) {

  SQLCloseCursor(hstmt);

  query_rc = SQLExecute(hstmt);
  if (!SQL_SUCCEEDED(query_rc)) { // Query error
    throw DbException( "SQLExecute FAILED", query_rc);
  }


  if ( outsize != 0 && outBindSet != NULL ) {
    for ( unsigned int i = 0; i < outsize; ++i ) {

      // printf("OUT %d %d\n",i, outBindSet[i]->size);
      query_rc =+ SQLBindCol( hstmt,
			      i+1,
			      outBindSet[i].c_type,
			      outBindSet[i].param,
			      outBindSet[i].size,
			      NULL );

    }

    if (!SQL_SUCCEEDED(query_rc)) { // Query error
      throw DbException( "SQL bind FAILED", query_rc);
    }
  }

  if (SQLFetch(hstmt) != SQL_SUCCESS) {
    throw DbException( "SQL SQLFetch FAILED", query_rc);
  }
  return 0;
}


// int DbObject::sqlUpdate() {
//   return sqlInsert();
// }

// int DbObject::sqlInsert() {
  
//   SQLCloseCursor(hstmt);
  
//   query_rc = SQLExecute(hstmt);
//   if (!SQL_SUCCEEDED(query_rc)) { // Query error
//     printf("SQLExecute FAILED\n");
//     printf("  Returned code %d\n",query_rc);
//     return -1;
//   }
  
//   if (!SQL_SUCCEEDED(query_rc)) { // Query error
//     printf("SQL bind FAILED\n");
//     printf("  Returned code %d\n",query_rc);
//     return -2;
//   }

//   return 0;
// }



int CuOFDb::DbObject::checkQuery()
{

//   int DB_MAXERR = 10;
//   int errcount = 0;

  if ( !SQL_SUCCEEDED(query_rc)) { // Query error

//     if ( ++errcount >= DB_MAXERR ) { // Too many error: try to reconnect
//       disconnect();
//       connect();
//       if (SQL_SUCCEEDED(connct_rc)) {
//         printf("Reconnected to DB.\n");
//         errcount=0;
//       } else {
//         printf("Reconnection to DB failed.\n");
//       }
//     }

    throw DbException( "QUERY FAILED", query_rc );
  }

  return 0;
}


int CuOFDb::DbObject::fetchrow() {
  if ( SQL_SUCCEEDED(query_rc) )  {
    return SQLFetch(hstmt);
  }
  return SQL_NO_DATA;
}


int CuOFDb::DbObject::returnfield( int field, char *resu )
{
  SQLLEN cbValue;
  SQLRETURN rc;

  strcpy(resu, ""); // Reset "resu"
  field +=1; // ODBC starts counting from 1!!!

  if ( SQL_SUCCEEDED(query_rc) ) {
    rc = SQLGetData(hstmt, (SQLSMALLINT)(field), SQL_C_CHAR, (SQLCHAR*)resu,
		    FIELD_MAX_LENGTH, &cbValue); // Convert field into chars
    resu[(int)cbValue]='\0'; // Add line terminator
    if ( SQL_SUCCEEDED(rc) )
      return (int)cbValue; // Ok
  }
  return 0; // Error
}


//cms_omds_lb.cern.ch
