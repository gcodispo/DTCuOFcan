#include "CuOFBoard.h"
#include "CanErrors.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iterator>
#include "CanUtils.h"
#include <stdlib.h>
#include <unistd.h>


// #define DEBUG

const size_t dtcan::CuOFBoard::nmezzanines = 4;
const size_t dtcan::CuOFBoard::nonets = 8;
const size_t dtcan::CuOFBoard::ntotonets = 32;
const size_t dtcan::CuOFBoard::nregisters = 4;
const size_t dtcan::CuOFBoard::ndata = 128;
const size_t dtcan::CuOFBoard::nadc = 12;
bool dtcan::CuOFBoard::force1st_ = false;
bool dtcan::CuOFBoard::force2nd_ = false;

const char dtcan::CuOFBoard::writeCode = 0x2b;
const char dtcan::CuOFBoard::readCode = 0x40;
const char dtcan::CuOFBoard::markerRomProg = 0x80;
const char dtcan::CuOFBoard::bufActiveAtStartup = 0x81;
const char dtcan::CuOFBoard::timeRefreshL = 0x82;
const char dtcan::CuOFBoard::timeRefreshH = 0x83;
const char dtcan::CuOFBoard::adcBaseConf = 0x86;
const char dtcan::CuOFBoard::adcBaseSetup = 0x87;
const char dtcan::CuOFBoard::ramDefaultBlock = 0;
const char dtcan::CuOFBoard::readOkCode = 0x4b;
const char dtcan::CuOFBoard::writeOkCode = 0x60;
const short dtcan::CuOFBoard::headcode = 0x5aa5;
dtcan::adcmap dtcan::CuOFBoard::adcmap_[dtcan::CuOFBoard::ntotonets];


void dtcan::CuOFBoard::fillAdcMap( const std::string & reg,
				   int nonet, char nadc, char chan )
{
  
  adcmap_[nonet].onet = nonet;
  if ( reg == "TEMP" ) adcmap_[nonet].temp = adcpair( nadc, chan );
  else if ( reg == "MONB" ) adcmap_[nonet].monb = adcpair( nadc, chan );
  else if ( reg == "FLT"  ) adcmap_[nonet].flt  = adcpair( nadc, chan );
  else if ( reg == "MONP" ) adcmap_[nonet].monp = adcpair( nadc, chan );
}




/// streaming operator for onet
std::ostream& dtcan::operator<< (std::ostream& os, const dtcan::onet & ot )
{
  os << "mezz : " << dtcan::digiFiller << (int) (0xff & ot.mezzanine ) << '\t'
     << "onet : " << dtcan::digiFiller << (int) (0xff & ot.nonet      ) << '\t'
     << "ctrl : " << dtcan::digiFiller << (int) (0xff & ot.control   ) << '\t'
     << "mod  : " << dtcan::digiFiller << (int) (0xff & ot.modulation) << '\t'
     << "bias : " << dtcan::digiFiller << (int) (0xff & ot.bias      ) << '\t'
     << "eq   : " << dtcan::digiFiller << (int) (0xff & ot.equalizer ) << '\n';
  return os;
};


/// streaming operator for adcpair
std::ostream& dtcan::operator<< (std::ostream& os, const dtcan::adcpair & adc )
{
  os << "[adc : " << dtcan::digiFiller << (int) (0xff & adc.adc ) << "; "
     << "ch : "   << dtcan::digiFiller << (int) (0xff & adc.ch )  << "]";
  return os;
};


/// streaming operator for adcmap
std::ostream& dtcan::operator<< (std::ostream& os, const dtcan::adcmap & adc )
{
  os << "onet : " << dtcan::digiFiller << (int) (0xff & adc.onet ) << '\t'
     << adc.temp << '\t'
     << adc.monb << '\t'
     << adc.flt  << '\t'
     << adc.monp << '\n';
  return os;
};


/// streaming operator for adconet
std::ostream& dtcan::operator<< (std::ostream& os, const dtcan::adconet & adc )
{
  os << "mezz : " << dtcan::digiFiller << (int) (0xff & adc.mezzanine ) << '\t'
     << "onet : " << dtcan::digiFiller << (int) (0xff & adc.onet      ) << '\t'
     << "temp : " << adc.temp   << " [" << dtcan::digiFiller << (int) (0xff & adc.tempi ) << "]\t"
     << "monb : " << dtcan::digiFiller << (int) (0xff & adc.monb ) << '\t'
     << "flt  : " << dtcan::digiFiller << (int) (0xff & adc.flt  ) << '\t'
     << "monp : " << dtcan::digiFiller << (int) (0xff & adc.monp ) << '\n';
  return os;
};




/// streaming operator for adc
std::ostream& dtcan::operator<< (std::ostream& os, const dtcan::adc & adc )
{
  os << "adc    : " << dtcan::digiFiller << (int) (0xff & adc.n )      << '\t'
     << "config : " << dtcan::digiFiller << (int) (0xff & adc.config )	<< '\t'
     << "setup  : " << dtcan::digiFiller << (int) (0xff & adc.setup )	<< '\n';
  return os;
};

// dtcan::CuOFBoard::CuOFBoard( const short & base ) : base_(base), cansock_(0) {



/// consructor
dtcan::CuOFBoard::CuOFBoard( char base, char mask,
			     char isTrg, const std::string & name,
			     CtcpSocket * cansock, CtcpSocket * cansock_km )
  : base_(base), mask_(mask), isTrg_(isTrg), killmaster_(false), name_(name),
    cansocket_(cansock), cansocketKM_(cansock_km)
{
  /*     char mezz_o = -1; */
  /*     char onet_o = -1; */
  /*     char reg_o = -1; */
  /*     for ( size_t i = 0; i < dtcan::CuOFBoard::ndata; ++i ) { */
  /*       decode( i, mezz_o, onet_o, reg_o ); */
  /*       std::cout << i << '\t' << std::hex */
  /* 		<< int(mezz_o) << '\t' << short(onet_o) << '\t' << short(reg_o) */
  /* 		<< std::dec << std::endl ; */
  /*     } */
    

  /*     char max_adc = dtcan::CuOFBoard::adcBaseConf + 2*nadc; */
  /*     for ( char i = dtcan::CuOFBoard::adcBaseConf; i < max_adc; i+=2 ) { */
  /*       std::cout << std::hex << short(i) << '\t' << short(i+1) << std::dec << std::endl ; */
  /*     } */

  memset( &canTx_, int(0), sizeof(CANTxMessage) );
  canTx_.header.code = headcode;
  canTx_.header.size = 16;
  canTx_.body.SID = 0x0600 + (0xff & base_);
  canTx_.body.DLC = 0x05; // sizeof data

};




// /// consructor
// dtcan::CuOFBoard::CuOFBoard( char base, char mask,
// 			     char isTrg, char mod, char bias, 
// 			     CtcpSocket * cansock, CtcpSocket * cansock_km )
//   : base_(base), mask_(mask), isTrg_(isTrg), mod_(mod), bias_(bias),
//     killmaster_(false), cansocket_(cansock), cansocketKM_(cansock_km)
// {
//   memset( &canTx_, int(0), sizeof(CANTxMessage) );
//   canTx_.header.code = headcode;
//   canTx_.header.size = 16;
//   canTx_.body.SID = 0x0600 + (0xff & base_);
//   canTx_.body.DLC = 0x05; // sizeof data

// };





short dtcan::CuOFBoard::read ( size_t nframes )
{
  short retval = 0;

  try {
    retval = read( nframes, killmaster_ );
  } catch ( const dtcanerror::tcpTimeout & e ) {

    if ( killmaster_ || force2nd_ || force1st_ ) {
      throw e; 
    } else {
      std::cout << "\033[22;31m Trying secondary bus... \033[22;39m"
		<< std::endl;
      retval = read( nframes, true );
      killmaster_ = true;
    }

  }

  return retval;

}


short dtcan::CuOFBoard::read ( size_t nframes, bool killmaster )
{

#ifdef DEBUG
  printf("Send ");
  //   printf( "\nhead   %02hhx \n", canTx_.header.code );
  //   printf( "head   %02hhx \n", canTx_.header.code>>8 );
  //   printf( "size   %02hhx \n", canTx_.header.size );
  //   printf( "size   %02hhx \n", canTx_.header.size>>8 );
  //   printf( "SID:11 %02hhx \n", canTx_.body.SID );
  //   printf( "DLC:4  %02hhx \n", canTx_.body.DLC );
  //   printf( "RB0:1  %02hhx \n", canTx_.body.RB0 );
  //   printf( "RB1:1  %02hhx \n", canTx_.body.RB1 );
  //   printf( "RTR:1  %02hhx \n", canTx_.body.RTR );
  //   printf( "EID:18 %02hhx \n", canTx_.body.EID );
  //   printf( "IDE:1  %02hhx \n", canTx_.body.IDE );
  //   printf( "SRR:1  %02hhx \n", canTx_.body.SRR );
  char * myp = (char *)(&canTx_);
  int mysiz = sizeof( CANTxMessage );
  for ( int ii=0; ii< mysiz; ++ii ) printf("%02hhx ",*myp++);
  printf("\n");
#endif

  short mlen = -1;
  sizebuf_ = 0;
  size_t triesleft = 10;

  Ctcp * cansock = ( killmaster || force2nd_ )?
    cansocketKM_->getSocket() : cansocket_->getSocket();

  cansock->Send( (char*)(&canTx_), sizeof(CANTxMessage) );
 
  for ( size_t i = 0; i < nframes; ++i ) {

    short *msh = (short *)mbuf_;

    // int start = getExeTime();
    cansock->Receive(mbuf_,2);
    // int stop = getExeTime();
    // std::cout << std::endl << "SEND "  << stop - start << "ms" << std::endl;

    if ( *msh == headcode ) {

      cansock->Receive(mbuf_, 2);
      mlen =  *msh;

      for ( int aa = 0; aa < mlen; ++aa ) {
	cansock->Receive(&mbuf_[sizebuf_], 1);
	++sizebuf_;
      }
	
#ifdef DEBUG
      printf("Received         ");
      for ( int ii=0; ii < mlen; ++ii ) printf("%02hhx ", mbuf_[ii]);
      printf("\n");
#endif

    } else {
      throw( dtcanerror::badheader( *msh ) );
    }

    if ( mbuf_[9] == 0x63 && mbuf_[9] == 0x02 && triesleft ) {
      usleep( 10 );
      --i;
      --triesleft;
      continue;
    } else if ( canTx_.body.data[0] == getWriteCode() ) {
      checkWrite();
    } else {
      checkRead();
    }
  }

  return mlen;

}


std::vector<unsigned short>
dtcan::CuOFBoard::cmd( bool read_w, unsigned short iHigh,
		       unsigned short iLow, unsigned short subIndex,
		       unsigned short data )
{

  canTx_.body.data[0] = ( read_w ? getReadCode() : getWriteCode() );
  canTx_.body.data[1] = iHigh; // Index High
  canTx_.body.data[2] = iLow; // Index Low
  canTx_.body.data[3] = subIndex;  // data
  canTx_.body.data[4] = data;  // data
  this->read();

  std::vector<unsigned short> ret;
  ret.push_back( mbuf_[9] & 0xff );
  ret.push_back( mbuf_[10] & 0xff );
  ret.push_back( mbuf_[11] & 0xff );
  size_t data_idx = 12;
  do {
    ret.push_back( mbuf_[data_idx] & 0xff );
    ++data_idx;
  } while ( mbuf_[data_idx] );
  return ret;
}


//////////////////////////////////
/// ONETS config
//////////////////////////////////

int dtcan::CuOFBoard::writeOnetParamsToRam( dtcan::onet * params, size_t len )
{

  int retval = 0;
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x20; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[4] = 0x00;  // data

  for ( size_t i = 0; i < len; ++i ) {

    dtcan::onet & thisonet = params[i];

    // std::cout << "***** " << len << " *** " << i
    // 	      << " *** " << thisonet << std::endl;

    /// check if the mezzanine is active
    if ( ! isActiveMezzanine( thisonet.mezzanine ) )
      throw dtcanerror::badmezzanine( thisonet.mezzanine );

    /// do the job

    size_t reg = encode( thisonet.mezzanine, thisonet.nonet, 1 );
    canTx_.body.data[4] = thisonet.modulation;
    canTx_.body.data[3] = reg;
    retval += read();

    reg = encode( thisonet.mezzanine, thisonet.nonet, 2 );
    canTx_.body.data[4] = thisonet.bias;
    canTx_.body.data[3] = reg;
    retval += read();

    //     reg = encode( thisonet.mezzanine, thisonet.nonet, 0 );
    //     canTx_.body.data[4] = thisonet.control;
    //     canTx_.body.data[3] = reg;
    //     retval += read();

    //     reg = encode( thisonet.mezzanine, thisonet.nonet, 3 );
    //     canTx_.body.data[4] = thisonet.equalizer;
    //     canTx_.body.data[3] = reg;
    //     retval += read();

  }

  return retval;
  
}




int dtcan::CuOFBoard::writeOnetDirect( dtcan::onet * params, size_t len )
{

  int retval = 0;
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x20; // Index High
  canTx_.body.data[2] = 0x04; // Index Low
  canTx_.body.data[3] = 0x00; // Index Low
  canTx_.body.data[4] = 0x00;  // data

  for ( size_t i = 0; i < len; ++i ) {

    dtcan::onet & thisonet = params[i];

    std::cout << "***** " << len << " *** " << i
	      << " *** " << thisonet << std::endl;

    /// check if the mezzanine is active
    if ( ! isActiveMezzanine( thisonet.mezzanine ) )
      throw dtcanerror::badmezzanine( thisonet.mezzanine );

    /// do the job
    try {

      size_t reg = encode( thisonet.mezzanine, thisonet.nonet, 1 );
      canTx_.body.data[3] = 0;
      canTx_.body.data[4] = thisonet.modulation;
      std::cout << std::endl;
      for ( int ii=0; ii< 5; ++ii ) printf("%02hhx ", canTx_.body.data[ii]);
      std::cout << std::endl;
      retval += read();
      canTx_.body.data[3] = 1;
      canTx_.body.data[4] = reg;
      for ( int ii=0; ii< 5; ++ii ) printf("%02hhx ", canTx_.body.data[ii]);
      std::cout << std::endl;
      retval += read();

      reg = encode( thisonet.mezzanine, thisonet.nonet, 2 );
      canTx_.body.data[3] = 0;
      canTx_.body.data[4] = thisonet.bias;
      retval += read();
      canTx_.body.data[3] = 1;
      canTx_.body.data[4] = reg;
      retval += read();
    } catch ( std::exception & e ) {
      std::cout << "*** EXCEPTION : " << e.what() << std::endl;
    }

    //     reg = encode( thisonet.mezzanine, thisonet.nonet, 0 );
    //     canTx_.body.data[4] = thisonet.control;
    //     canTx_.body.data[3] = reg;
    //     retval += read();

    //     reg = encode( thisonet.mezzanine, thisonet.nonet, 3 );
    //     canTx_.body.data[4] = thisonet.equalizer;
    //     canTx_.body.data[3] = reg;
    //     retval += read();

  }

  return retval;
  
}





int dtcan::CuOFBoard::writeRamToOnet( dtcan::onet * params, size_t len)
{

  int retval = 0;
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x21; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[4] = 0x00;  // data

  for ( size_t i = 0; i < len; ++i ) {

    dtcan::onet & thisonet = params[i];

    /// check if the mezzanine is active
    if ( ! isActiveMezzanine( thisonet.mezzanine ) )
      throw dtcanerror::badmezzanine( thisonet.mezzanine );

    /// do the job
    int reg = encodeRam( thisonet.mezzanine, thisonet.nonet );
    canTx_.body.data[3] = reg;
    read( 2 );
    retval += ( mbuf_[11] == reg ) ? 1 : 0;
  }

  return retval;

}


int dtcan::CuOFBoard::writeRamToOnet( char mezz )
{

  /// check if the mezzanine is active
  if ( ! isActiveMezzanine( mezz ) )
    throw dtcanerror::badmezzanine( mezz );

  int reg = encodeRam( mezz );

  /// do the job
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x21; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = reg;
  canTx_.body.data[4] = 0x00;  // data
  read( 2 );
  return ( mbuf_[11] == reg ) ? 1 : 0;

}


int dtcan::CuOFBoard::writeRamToOnet()
{
  int reg = encodeRam();

  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x21; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = reg;
  canTx_.body.data[4] = 0x00;  // data
  read( 2 );
  return ( mbuf_[11] == reg ) ? 1 : 0;

}





dtcan::v_time dtcan::CuOFBoard::readFwVersion()
{

  dtcan::v_time ret = { -1, -1, -1, -1 };

  /// Year
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x04;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.year = ( 0xff & mbuf_[12] );

  /// Month
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x05;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.month = ( 0xff & mbuf_[12] );

  /// Day
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x06;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.day = ( 0xff & mbuf_[12] );

  /// Hour
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x07;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.hour = ( 0xff & mbuf_[12] );

  return ret;

}


dtcan::v_time dtcan::CuOFBoard::readSwVersion()
{

  dtcan::v_time ret = { -1, -1, -1, -1 };

  /// Year
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.year = ( 0xff & mbuf_[12] );

  /// Month
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x01;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.month = ( 0xff & mbuf_[12] );

  /// Day
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x02;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.day = ( 0xff & mbuf_[12] );

  /// Hour
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x10; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x03;
  canTx_.body.data[4] = 0x00;  // data
  read( 1 );
  ret.hour = ( 0xff & mbuf_[12] );

  return ret;

}



// int dtcan::CuOFBoard::writeRamToFlash( onet * params, size_t len, char)
// {

//   int retval = 0;

//   canTx_.body.data[0] = getWriteCode();
//   canTx_.body.data[1] = 0x22; // Index High
//   canTx_.body.data[2] = 0x00; // Index Low
//   canTx_.body.data[4] = 0x00;  // data

// //   for ( size_t i = 0; i < len; ++i ) {


// //     onet & thisonet = params[len];

// //     for  ( size_t j = 0; j < 4; ++j ) {
// //       size_t reg = encode( thisonet.mezzanine, thisonet.nonet, j );
// //       canTx_.body.data[3] = reg;
// //       retval += read();
// //     }
// //   }

//   return retval;

// }




int dtcan::CuOFBoard::configOnetsFromRam( dtcan::onet * params, size_t len )
{
  int retval = 0;

  std::cout << "$$$ writeOnetParamsToRam\n";
  retval = writeOnetParamsToRam( params, len);
  std::cout << "$$$ writeRamToOnet\n";
  // retval += writeRamToOnet( params, len);
  retval += writeRamToOnet();
  return retval;
}




int dtcan::CuOFBoard::configureOnets( dtcan::onet * params, size_t len )
{
  int retval = 0;
  bool f1 = force1st_;
  bool f2 = force2nd_;

  force1st_ = true;
  force2nd_ = false;

  std::cout << crate() << ' ' << name() << ' ';
  for ( size_t i = 0; i < len; ++i )
    std::cout << int(params[i].mezzanine) << ';';
  std::cout << " - to RAM 1st";
    
  retval = writeOnetParamsToRam( params, len);
  std::cout << " - to flash 1st";
  retval += ramToFlash();
  std::cout << " - to onet";
  // retval += writeRamToOnet( params, len);
  retval += writeRamToOnet();

  force1st_ = false;
  force2nd_ = true;
  std::cout << " - to RAM 2nd";
  retval += writeOnetParamsToRam( params, len);
  std::cout << " - to flash 2nd";
  retval += ramToFlash();
  std::cout << std::endl;

  force1st_ = f1;
  force2nd_ = f2;

  return retval;
}

int dtcan::CuOFBoard::flashToRam( char block )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x22; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = block;
  canTx_.body.data[4] = 0x01;  // data
  read( 2 );
  return mbuf_[12];
};


void dtcan::CuOFBoard::restoreFromFlash( char block )
{
  flashToRam( block );
  writeRamToOnet();
}

int dtcan::CuOFBoard::ramToFlash( char block )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x22; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = block;
  canTx_.body.data[4] = 0x02;  // data
  read( 2 );
  return mbuf_[12];
};


int dtcan::CuOFBoard::compareRamToFlash( char block )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x22; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = block;
  canTx_.body.data[4] = 0x03;  // data
  read( 2 );
  return mbuf_[12];
};



int dtcan::CuOFBoard::readRamBlockAtStartup()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
  return mbuf_[12];
};


int dtcan::CuOFBoard::setRamBlockAtStartup( char block )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x00; // Index Low
  canTx_.body.data[3] = block;
  canTx_.body.data[4] = 0x00;  // data
  read();
  return mbuf_[12];
};


int dtcan::CuOFBoard::writeId()
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x02; // Index Low
  canTx_.body.data[4] = base_;  // data
  canTx_.body.data[5] = 0x00;
  //  read( 2 );
  std::cout << "writeId DISABLED!!!" << std::endl;
  return mbuf_[12];
};


int dtcan::CuOFBoard::readId()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x02; // Index Low
  canTx_.body.data[3] = 0x00; // Index Low
  canTx_.body.data[4] = 0x00;  // data
  canTx_.body.data[5] = 0x00;

  read( 1 );

  return (0xff & mbuf_[11]);
};




dtcan::master dtcan::CuOFBoard::killMaster()
{
  killmaster_ = true;
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[3] = 0x01;
  canTx_.body.data[4] = 0x00;  // data
  read();
  if ( mbuf_[11] == 0 || mbuf_[11] == 1 ) return MASTER;
  else if ( mbuf_[11] == 0x40 ) return BACKUP;
  else if ( mbuf_[11] == 0x41 ) return BACKUP_KM;
  return ERROR;
};




dtcan::master dtcan::CuOFBoard::restoreMaster()
{
  killmaster_ = true;
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();

  killmaster_ = false;
  if ( mbuf_[11] == 0 || mbuf_[11] == 1 ) return MASTER;
  else if ( mbuf_[11] == 0x40 ) return BACKUP;
  else if ( mbuf_[11] == 0x41 ) return BACKUP_KM;
  return ERROR;
};


dtcan::master dtcan::CuOFBoard::statusMaster()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
  if ( mbuf_[11] == 0 || mbuf_[11] == 1 ) return MASTER;
  else if ( mbuf_[11] == 0x40 ) return BACKUP;
  else if ( mbuf_[11] == 0x41 ) return BACKUP_KM;
  return ERROR;
};



dtcan::master dtcan::CuOFBoard::statusBackup()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read( 1, true );
  if ( mbuf_[11] == 0 || mbuf_[11] == 1 ) return MASTER;
  else if ( mbuf_[11] == 0x40 ) return BACKUP;
  else if ( mbuf_[11] == 0x41 ) return BACKUP_KM;
  return ERROR;
};


unsigned short dtcan::CuOFBoard::timer( unsigned short t_value )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x24; // Index High
  canTx_.body.data[2] = 0;
  canTx_.body.data[3] = ( t_value & 0xff00 ) >> 8;
  canTx_.body.data[4] = t_value & 0x00ff;
  read();
  
  short retval = ( mbuf_[11] & 0xff ) << 8;
  retval |= ( 0x00ff & mbuf_[12] );
  return retval;
};


unsigned short dtcan::CuOFBoard::timer()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x24; // Index High
  canTx_.body.data[2] = 0x00;
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
  
  short retval = ( mbuf_[11] & 0xff ) << 8;
  retval |= ( 0x00ff & mbuf_[12] );
  return retval;
};


void dtcan::CuOFBoard::reset()
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x03; // Index Low
  canTx_.body.data[3] = 0x52;
  canTx_.body.data[4] = 0x00;  // data
  read();
};


unsigned short dtcan::CuOFBoard::silent()
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x04; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
  return (mbuf_[11] & 0xff);
};


void dtcan::CuOFBoard::silent( bool enable )
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x23; // Index High
  canTx_.body.data[2] = 0x04; // Index Low
  canTx_.body.data[3] = ( enable ? 0x01 : 0x00 );
  canTx_.body.data[4] = 0x00;  // data
  read();
};




/// read CAN Registers
unsigned short dtcan::CuOFBoard::readCanRegisters( unsigned short reg )
{
  if ( reg > 0x05 ) throw dtcanerror::badregister( reg );
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x27; // Index High
  canTx_.body.data[2] = reg; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
  //// FIXME!!!
  return (mbuf_[12] & 0xff);
}

/// read CAN Registers
std::vector<std::string> dtcan::CuOFBoard::readCanRegisters()
{

  std::vector<std::string> ret;
  ret.reserve( 10 );

  unsigned short val = readCanRegisters(0);
  if ( val & 0x01 ) ret.push_back( "rx: error passive" );
  else ret.push_back( "rx: error active (default)" );
  if ( val & 0x10  ) ret.push_back( "rx: bus off" );

  val = readCanRegisters(1);
  if ( val & 0x01 ) ret.push_back( "rx: CAN controller off" );
  else  ret.push_back( "rx: CAN controller on" );
  if ( val & 0x10  )     ret.push_back( "rx_err_gte96" );

  val = readCanRegisters(2);
  char errors[20];
  snprintf( errors, 20, "rx_errors=%d", val );
  ret.push_back( errors );

  val = readCanRegisters(3);
  if ( val & 0x01 ) ret.push_back( "tx: error passive" );
  else ret.push_back( "tx: error active (default)" );
  if ( val & 0x10  )     ret.push_back( "tx: bus off" );

  val = readCanRegisters(4);
  if ( val & 0x01 ) ret.push_back( "tx: CAN controller off" );
  else ret.push_back( "tx: CAN controller on" );
  if ( val & 0x10  )     ret.push_back( "tx_err_gte96" );

  val = readCanRegisters(5);
  snprintf( errors, 20, "tx_errors=%d", val );
  ret.push_back( errors );

  return ret;

}


/// read CAN Counters
unsigned short dtcan::CuOFBoard::readCanCounters( unsigned short count )
{
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x27; // Index High
  canTx_.body.data[2] = 0x06; // Index Low
  canTx_.body.data[3] = count;
  canTx_.body.data[4] = 0x00;  // data
  read();
  return (mbuf_[12] & 0xff); //// 12 is data field
}




/// read CAN Counters
std::map<std::string, size_t> dtcan::CuOFBoard::readCanCounters()
{
  std::map<std::string, size_t> ret;
  ret["crc"]         = readCanCounters(0);
  ret["form"]        = readCanCounters(1);
  ret["ack"]         = readCanCounters(2);
  ret["stuff"]       = readCanCounters(3);
  ret["bit"]         = readCanCounters(4);
  ret["arbitration"] = readCanCounters(5);
  ret["overload"]    = readCanCounters(6);
  return ret;
}



/// reset CAN Registers
void dtcan::CuOFBoard::resetCanCounters()
{
  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x27; // Index High
  canTx_.body.data[2] = 0x06; // Index Low
  canTx_.body.data[3] = 0x00;
  canTx_.body.data[4] = 0x00;  // data
  read();
}



//////////////////////////////////
/// ONETS monitoring
//////////////////////////////////

int dtcan::CuOFBoard::readOnet( dtcan::onet & reply )
{
  /// is onet number inside the range?
  testOnet( reply.nonet );

  /// check if the mezzanine is active
  if ( ! isActiveMezzanine( reply.mezzanine ) )
    throw dtcanerror::badmezzanine( reply.mezzanine );

  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x20; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[4] = 0x00;  // data

  int retval = 0;
  size_t reg = encode( reply.mezzanine, reply.nonet, 0 );
  
  //   std::cout << "*** " << std::hex << '\t' << mezz*4+nonet << std::endl ;
  //   std::cout << "*** " << std::hex
  // 	    << int(mezz) << '\t' << short(nonet) << '\t' << short(reg)
  // 	    << std::dec << std::endl ;

  /// do the job
  canTx_.body.data[3] = reg;
  retval += read();
  reply.control = mbuf_[12];

  canTx_.body.data[3] = reg+1;
  retval += read();
  reply.modulation = mbuf_[12];

  canTx_.body.data[3] = reg+2;
  retval += read();
  reply.bias = mbuf_[12];

  canTx_.body.data[3] = reg+3;
  retval += read();
  reply.equalizer = mbuf_[12];

  return retval;

  //       short cmdissued = mbuf_[8];
  //       short idxHissued = mbuf_[9];
  //       short idxLissued = mbuf_[10];
  //       char regread = mbuf_[11];

};


int dtcan::CuOFBoard::readOnet( char mezz, char nonet, dtcan::onet & reply )
{

  reply.mezzanine = mezz;
  reply.nonet = nonet;
  return readOnet( reply );

};


int dtcan::CuOFBoard::readOnets( dtcan::onet * reply, size_t len )
{

  int retval = 0;

  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x20; // Index High
  canTx_.body.data[2] = 0x01; // Index Low
  canTx_.body.data[4] = 0x00;  // data

  for ( size_t i = 0; i < len; ++i ) {
    dtcan::onet & thisonet = reply[i];
    retval += readOnet( thisonet );
  }

  return retval;
  
}



int dtcan::CuOFBoard::readMezzanineOnets( char mezz, dtcan::onet reply[dtcan::CuOFBoard::nonets] )
{

  int retval = 0;
  for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
    dtcan::onet & thisonet = reply[nonet];
    retval += readOnet( mezz, nonet, thisonet );
  }

  return retval;

};




int dtcan::CuOFBoard::readAllOnets( dtcan::onet reply[dtcan::CuOFBoard::ntotonets] )
{

  int retval = 0;
  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( isActiveMezzanine( mezz ) ) {
      for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	dtcan::onet & thisonet = reply[mezz*8+nonet];
	retval += readOnet( mezz, nonet, thisonet );
      }
    }
  }

  return retval;

};



void dtcan::CuOFBoard::loadOnetFromRam( )
{

  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( isActiveMezzanine( mezz ) ) {
      for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	dtcan::onet reply;
	readOnet( mezz, nonet, reply );
	setOnetSettings( mezz, nonet, reply.modulation, reply.bias );
      }
    }
  }

};

int dtcan::CuOFBoard::diffOnetsWithDB( )
{

  int retval = 0;
  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( isActiveMezzanine( mezz ) ) {
      for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	dtcan::onet reply;
	readOnet( mezz, nonet, reply );
	if ( ( reply.modulation != modulation_[mezz][nonet] )
	     || ( reply.bias != bias_[mezz][nonet] ) ) {
	  std::cout <<  int( mezz ) << ' ' << int( nonet )
		    << " DB '" << int(modulation_[mezz][nonet])
		    << "' '" << int(bias_[mezz][nonet])
		    << "' HW '" << int(reply.modulation)
		    << "' '" << int(reply.bias) << "'" << std::endl;
	  ++retval;
	}

      }
    }
  }

  return retval;

};

void dtcan::CuOFBoard::configOnetsFromDB()
{
  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet )
      configOnetFromDB( mezz, nonet );
  }
};

void dtcan::CuOFBoard::configOnetsFromDB( char mezz )
{
  for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
    configOnetFromDB(  mezz, nonet );
  }
};

void dtcan::CuOFBoard::configOnetFromDB( char mezz, char nonet )
{
  if ( isActiveMezzanine( mezz ) ) {
    dtcan::onet params[1];
    params[0].mezzanine = mezz;
    params[0].nonet = nonet;
    params[0].control = 0xd2;
    params[0].modulation = modulation_[int(mezz)][int(nonet)];
    params[0].bias = bias_[int(mezz)][int(nonet)];
    params[0].equalizer = 0x0;
    configureOnets( params, 1 );
  }
}


//////////////////////////////////
/// ADC monitoring
//////////////////////////////////


int dtcan::CuOFBoard::readAdcChan( char nadc, char chan )
{
  /// check if adc number is inside valid range
  testAdc( nadc );

  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x26; // Index High
  canTx_.body.data[2] = nadc;
  canTx_.body.data[3] = chan;
  canTx_.body.data[4] = 0x00; // data
  read();

  return ( 0xff & mbuf_[12] );
};


int dtcan::CuOFBoard::readOnetTemp( char mezz, char nonet, dtcan::adconet & reply )
{
  /// is onet number inside the range?
  testOnet( nonet );

  /// check if the mezzanine is active
  if ( ! isActiveMezzanine( mezz ) )
    throw dtcanerror::badmezzanine( mezz );

  int retval = 0;

  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x26; // Index High
  canTx_.body.data[4] = 0x00; // data

  reply.mezzanine = mezz;
  reply.onet = nonet;
  dtcan::adcmap & thisadcmap = adcmap_[mezz * 8 + nonet];
  //  std::cout << thisadcmap << std::endl;

  //   std::cout << "**** do " << mezz * 8 + onet << '\t' << thisadcmap << ": mezz " << int(reply.mezzanine)
  // 	    << " onet " << int(reply.nonet) << "**** " << std::endl;


  /// do the job
  reply.tempi = -1;
  reply.temp  = -1;
  reply.monb  = -1;
  reply.flt   = -1;
  reply.flt   = -1;

  canTx_.body.data[2] = thisadcmap.temp.adc;
  canTx_.body.data[3] = thisadcmap.temp.ch;
  retval += read();
  reply.tempi = ( 0xff & mbuf_[12] );
  reply.temp  = convertTemp( mbuf_[12] );

  return retval;

};



int dtcan::CuOFBoard::readAllTemp( dtcan::adconet reply[dtcan::CuOFBoard::ntotonets] )
{

  int retval = 0;

  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( isActiveMezzanine( mezz ) ) {

      for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	dtcan::adconet & thisonet = reply[mezz*8+nonet];
	retval += readOnetTemp( mezz, nonet, thisonet );
      }
    }
  }

  return retval; 
}



int dtcan::CuOFBoard::readOnetAdc( char mezz, char nonet, dtcan::adconet & reply )
{
  /// is onet number inside the range?
  testOnet( nonet );

  /// check if the mezzanine is active
  if ( ! isActiveMezzanine( mezz ) )
    throw dtcanerror::badmezzanine( mezz );

  int retval = 0;

  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x26; // Index High
  canTx_.body.data[4] = 0x00; // data

  reply.mezzanine = mezz;
  reply.onet = nonet;
  dtcan::adcmap & thisadcmap = adcmap_[mezz * 8 + nonet];
  //  std::cout << thisadcmap << std::endl;

  //   std::cout << "**** do " << mezz * 8 + onet << '\t' << thisadcmap << ": mezz " << int(reply.mezzanine)
  // 	    << " onet " << int(reply.nonet) << "**** " << std::endl;

  /// do the job
  canTx_.body.data[2] = thisadcmap.temp.adc;
  canTx_.body.data[3] = thisadcmap.temp.ch;
  retval += read();
  reply.tempi = ( 0xff & mbuf_[12] );
  reply.temp  = convertTemp( mbuf_[12] );

  canTx_.body.data[2] = thisadcmap.monb.adc;
  canTx_.body.data[3] = thisadcmap.monb.ch;
  retval += read();
  reply.monb = ( 0xff & mbuf_[12] );


  canTx_.body.data[2] = thisadcmap.flt.adc;
  canTx_.body.data[3] = thisadcmap.flt.ch;
  retval += read();
  reply.flt = ( 0xff & mbuf_[12] );

  canTx_.body.data[2] = thisadcmap.monp.adc;
  canTx_.body.data[3] = thisadcmap.monp.ch;
  retval += read();
  reply.monp = ( 0xff & mbuf_[12] );

  return retval;

};



int dtcan::CuOFBoard::readMezzanineAdc( char mezz, dtcan::adconet reply[dtcan::CuOFBoard::nonets] )
{

  int retval = 0;

  for ( size_t n = 0; n < dtcan::CuOFBoard::nonets; ++n ) {

    dtcan::adconet & thisonet = reply[n];
    retval += readOnetAdc( mezz, n, thisonet );
  }
     
  return retval; 
}



int dtcan::CuOFBoard::readAllAdc( dtcan::adconet reply[dtcan::CuOFBoard::ntotonets] )
{

  int retval = 0;

  for ( size_t mezz = 0; mezz < dtcan::CuOFBoard::nmezzanines; ++mezz ) {
    if ( isActiveMezzanine( mezz ) ) {

      for ( size_t nonet = 0; nonet < dtcan::CuOFBoard::nonets; ++nonet ) {
	dtcan::adconet & thisonet = reply[mezz*8+nonet];
	retval += readOnetAdc( mezz, nonet, thisonet );
      }
    }
  }

  return retval; 
}


//////////////////////////////////
/// ADC conf
//////////////////////////////////

int dtcan::CuOFBoard::readAdcConf( short n, dtcan::adc & reply )
{
  int retval = 0;
  canTx_.body.data[0] = getReadCode();
  canTx_.body.data[1] = 0x25; // Index High
  canTx_.body.data[2] = n; // Index Low
  canTx_.body.data[4] = 0x00; // data

  reply.n = n;

  canTx_.body.data[3] = adcConf( n );
  retval += read();
  reply.config = mbuf_[12];

  canTx_.body.data[3] = adcSetup( n );
  retval += read();
  reply.setup = mbuf_[12];
 
  return retval; 
};

int dtcan::CuOFBoard::writeAdcConf( const dtcan::adc & thisadc )
{

  int retval = 0;

  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x25; // Index High
  canTx_.body.data[2] = thisadc.n;

  canTx_.body.data[3] = adcConf( thisadc.n );
  canTx_.body.data[4] = thisadc.config;
  retval += read();

  canTx_.body.data[3] = adcSetup( thisadc.n );
  canTx_.body.data[4] = thisadc.setup;
  retval += read();
     
  return retval; 
}



int dtcan::CuOFBoard::writeAdcConf( dtcan::adc * params, size_t len )
{

  int retval = 0;

  canTx_.body.data[0] = getWriteCode();
  canTx_.body.data[1] = 0x25; // Index High
  canTx_.body.data[4] = 0x00; // data

  for ( size_t n = 0; n < len; ++n ) {

    retval += writeAdcConf( params[n] );

  }
     
  return retval; 
}

////////////////////////////////////////////
/// this is the set of commands doing popen
/// for firmware related issues
////////////////////////////////////////////

std::string dtcan::CuOFBoard::actel( const std::string & exec,
				     const std::string & action,
				     const std::string & file_dat,
				     bool use2nd )
{

  /// which bus to be used?
  bool waskm = killmaster_;
  killmaster_ = ( use2nd ? true : false );
  CtcpSocket *cansocket = ( use2nd ) ? cansocketKM_ : cansocket_;

  /// output log file
  std::ostringstream osfile;
  osfile << cansocket->wheel << "_" << action << "_" << cansocket->host
	 << '_' << cansocket->port << ".log";
  std::string outfile = osfile.str();
  std::ofstream canRegs;
  canRegs.open( outfile.c_str(), std::ofstream::out | std::ofstream::app );
  canRegs << "CHECKING BOARD " << int(0xff & base_)
	  << ( killmaster_ ? " using 2nd... " : "... " );

  /// check board
  try {
    this->readId();
    canRegs << "OK!" << std::endl;
    cansocket->releaseSocket(); /// this would avoid socket to be blocked
  } catch ( std::exception & e ) {
    canRegs << "*** EXCEPTION : " << e.what() << std::endl;
    std::cout << "*** EXCEPTION : " << e.what() << std::endl;
    killmaster_ = waskm;
    return outfile;
  }

  /// issue command
  std::ostringstream command;
  command << exec << " -a=" << action << " -p=" << cansocket->host
	  << ':' << cansocket->port << ' ' << file_dat
	  << " -b=" << int(0xff & base_) << " 2>&1";
  std::cout <<  command.str() << std::endl;
//   std::vector< std::string> cmdout = execute( command.str() );
//   std::copy( cmdout.begin(), cmdout.end(),
// 	     std::ostream_iterator<std::string>(canRegs, "\t" ) );
  execute( command.str(), canRegs );
  canRegs << std::endl;

  /// wait a while, then go ahead
  sleep( 20 );

  // read counters
  try {
    std::map<std::string, size_t> ret_map = this->readCanCounters();
    std::map<std::string, size_t>::const_iterator it = ret_map.begin();
    std::map<std::string, size_t>::const_iterator itend = ret_map.end();
    for ( ; it != itend; ++it )
      canRegs << '\t' << it->first << " : " << it->second << '\n';
  } catch ( std::exception & e ) {
    canRegs << "*** FAILED readCanCounters : " << e.what();
    killmaster_ = waskm;
  }
  canRegs << std::endl;


  // read registers
  usleep( 300 );
  try {
    canRegs << "\n\t";
    std::vector<std::string> ret_vec = this->readCanRegisters();
    std::copy( ret_vec.begin(), ret_vec.end(),
	       std::ostream_iterator<std::string>(canRegs, "\n\t") );
  } catch ( std::exception & e ) {
    canRegs << "*** FAILED readCanCounters : " << e.what();
    killmaster_ = waskm;
  }
  canRegs << std::endl;

  killmaster_ = waskm;
  return outfile;

}
