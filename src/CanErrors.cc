#include "CanErrors.h"
#include <cstring>
#include <stdio.h>

void dtcanerror::formatHwError( char code, char *msg, const char * type, int dev ) {

  switch ( code ) {
  case 0x20 : sprintf( msg, "%s %d HW Error : Bus error during MST or selected backup modes.", type, dev ); break ;
  case 0x24 : sprintf( msg, "%s %d HW Error : SLA + W has been transmitted; NACK has been wrongly received.", type, dev ); break ;
  case 0x26 : sprintf( msg, "%s %d HW Error : Data byte in Data Register has been transmitted; NACK has been wrongly received.", type, dev ); break ;
  case 0x27 : sprintf( msg, "%s %d HW Error : Arbitration lost in SLA + R/W or data bytes.", type, dev ); break ;
  case 0x29 : sprintf( msg, "%s %d HW Error : SLA + R has been transmitted; NACK has been wrongly received.", type, dev ); break ;
  case 0x2C : sprintf( msg, "%s %d HW Error : Own SLA + W has been received; ACK has been wrongly returned.", type, dev ); break ;
  case 0x2D : sprintf( msg, "%s %d HW Error : Arbitration lost in SLA + R/W as master; own SLA + W has been received, ACK wrongly returned.", type, dev ); break ;
  case 0x2E : sprintf( msg, "%s %d HW Error : General call address (00H) has been received; ACK has been returned.", type, dev ); break ;
  case 0x2F : sprintf( msg, "%s %d HW Error : Arbitration lost in SLA + R/W as master; general call address has been received, ACK returned.", type, dev ); break ;
  case 0x30 : sprintf( msg, "%s %d HW Error : Previously addressed with own SLV address; DATA has been received; ACK returned.", type, dev ); break ;
  case 0x31 : sprintf( msg, "%s %d HW Error : Previously addressed with own SLA; DATA byte has been received; NACK returned", type, dev ); break ;
  case 0x32 : sprintf( msg, "%s %d HW Error : Previously addressed with general call address; DATA has been received; ACK returned.", type, dev ); break ;
  case 0x33 : sprintf( msg, "%s %d HW Error : Previously addressed with general call address; DATA has been received; NACK returned.", type, dev ); break ;
  case 0x34 : sprintf( msg, "%s %d HW Error : A STOP condition or repeated START condition has been received.", type, dev ); break ;
  case 0x35 : sprintf( msg, "%s %d HW Error : Own SLA + R has been received; ACK has been returned", type, dev ); break ;
  case 0x36 : sprintf( msg, "%s %d HW Error : Arbitration lost in SLA + R/W as master; own SLA + R has been received; ACK has been returned.", type, dev ); break ;
  case 0x37 : sprintf( msg, "%s %d HW Error : Data byte has been transmitted; ACK has been received.", type, dev ); break ;
  case 0x38 : sprintf( msg, "%s %d HW Error : Data byte has been transmitted; NACK has been received.", type, dev ); break ;
  case 0x39 : sprintf( msg, "%s %d HW Error : Last data byte has transmitted; ACK has received.", type, dev ); break ;
  default : sprintf( msg, "%s %d HW Error :Unknown Error", type, dev );
  }
}



void dtcanerror::formatMsgSyntaxError( char code, char *msg, const char * type ) {
  switch ( code ) {
  case 0x01 : sprintf( msg, "%s Syntax Error : Wrong CAN message, wrong index, high byte", type ); break ;
  case 0x02 : sprintf( msg, "%s Syntax Error : Wrong CAN message, wrong index, low byte", type ); break ;
  case 0x03 : sprintf( msg, "%s Syntax Error : Wrong CAN message, wrong subindex", type ); break ;
  case 0x04 : sprintf( msg, "%s Syntax Error : Wrong CAN message, wrong data", type ); break ;
  case 0x05 : sprintf( msg, "%s Syntax Error : Wrong Command", type ); break ;
  default : sprintf( msg, "%s Syntax Error : Unknown Error", type );
  }
}


void dtcanerror::formatMsgBusyError( char code, char *msg, const char * type ) {
  switch ( code ) {
  case 0x01 : sprintf( msg, "%s Busy/Backup : RAM to ONET module busy", type ); break ;
  case 0x02 : sprintf( msg, "%s Busy/Backup : ROM to RAM module busy", type ); break ;
  case 0x03 : sprintf( msg, "%s Busy/Backup : Board is Backup", type ); break ;
  case 0x04 : sprintf( msg, "%s Busy/Backup : Board is Backup, settings are in memory", type ); break ;
  default : sprintf( msg, "%s Busy/Backup : Unknown Error", type );
  }
}


