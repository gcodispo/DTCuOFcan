#ifndef _DbObject_
#define _DbObject_

#include <sqlext.h>
#include <string>

#define STR_LEN 40
#define DB_TIMEOUT 10 // Timeout (sec) for connecting to DB
#define DB_MIN_RECONNET_TIME 30 // Not reconnect more than once every xx sec's
#define FIELD_MAX_LENGTH 4096 // Maximum dimension (bytes) of a DB field

struct binder {
  SQLSMALLINT c_type;   // program type for the conversion
  SQLSMALLINT sql_type; // SQL type of the column
  SQLPOINTER  param;    // binding parameter
  SQLUINTEGER size;     // size of the binding parameter
  binder (){};
  binder ( SQLSMALLINT cType, SQLSMALLINT sqlType, SQLPOINTER  parameter,
	   SQLUINTEGER psize=0 )
    : c_type(cType), sql_type(sqlType), param (parameter), size(psize) {};
};

namespace CuOFDb {
  class DbObject {

  private:
    SQLHENV henv;            // Environment handle
    SQLHDBC hdbc;            // Connection handle
    SQLHSTMT hstmt;          // Statement handle
    SQLRETURN connct_rc;     // Connection return code
    SQLRETURN query_rc;      // Query return code
    time_t connct_time;
    char datasource[64], user[64], passwd[64]; // Connection parameters
    int checkQuery();

  public:
    explicit DbObject( const char *DataSource,
		       const char *UserName, const char *Password); // Connect to the specified DB

    DbObject( const std::string & DataSource,
	      const std::string & UserName, const std::string & Password );
    ~DbObject(); // Disconnect from DB

    /// query preparation using binding parameters
    int sqlQuery(char *mquery, int & res_rows, int & res_cols);
    int sqlQueryBind(char *mquery, binder *input=NULL, unsigned int insize = 0);
    /// binded query execution, allows binding output buffers
    int sqlFetchOne( binder *outBindSet=NULL, unsigned int outsize=0);
    int queryCrates();
    int queryBoards( const std::string & key );

    void connect(); // Connect to DB
    void disconnect(); // Disconnect from DB
    int fetchrow();
    int returnfield(int field, char *resu);

  };
};

#endif

