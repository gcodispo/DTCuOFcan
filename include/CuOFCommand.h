#ifndef _CUOF_COMMAND_
#define _CUOF_COMMAND_

#include "CuOFCan.h"

namespace dtcan {

  inline void parseSilent( unsigned short test ) {
    if ( test & 128 ) std::cout << "TEST Silenced; ";
    if ( test & 64 )  std::cout << "ADCRD Silenced; ";
    //if ( test & 32 )  std::cout << "; ";
    if ( test & 16 )  std::cout << "JTAG Active; ";
    if ( test & 8 )   std::cout << "TestDisable pin status; ";
    if ( test & 4 )   std::cout << "TestDisable pin override; ";
    if ( test & 2 )   std::cout << "TestDisable software Disable; ";
    if ( test & 1 )   std::cout << "ADCRD software Disable; ";
  }

  class CuOFBoard;

  class CuOFCommand {

    dtcan::CuOFCan can_;
    bool force2nd_;
    static std::string result[4];
    static std::string master[4];
    size_t nSockets_;
    std::vector<int> sids_;

  public:

    /// default constructor for test apps: includes configuration from file
    CuOFCommand( const std::string & canconf,
		 const std::string & boardconf,
		 const std::string & adcconf )
      : can_( canconf, boardconf, adcconf ), force2nd_( false ) {};

    /// default constructor for test apps: includes configuration from file
    CuOFCommand( const std::string & dbKey)
      : can_( dbKey ), force2nd_( false ) {};

    /// default constructor for test apps: includes configuration from file
    CuOFCommand()
      : can_(), force2nd_( false ) {};

    void configure( const std::string & canconf,
		    const std::string & boardconf,
		    const std::string & adcconf ) {
      can_.configure( canconf, boardconf, adcconf );
    }

    void configureFromDB( const std::string & dbKey ) {
      can_.configureFromDB( dbKey );
    }

    size_t allCrates();
    void applySelection( enum SELECTION sel,
			 const std::vector<std::string> & subset,
			 bool force2nd=false );
    bool select( const dtcan::CuOFBoard & thisboard, enum TYPE type );

    /// error messages
    void selectionError();
    void channelError();
    void writeError();

    /// a cli selector for read adc/adcconf/onet
    void queryCLI( const std::string & board,
		   dtcan::TYPE type,
		   const std::vector<std::string> & args,
		   bool isOnet=false, bool isConf=false );

    /// a cli selector for read write onet
    void writeOnetCLI( const std::string & board,
		       dtcan::TYPE type,
		       const std::vector<std::string> & args,
		       bool isFullConfig=false );

    /// a cli selector for actel commands
    void actelCLI( const std::string & board,
		   const std::vector<std::string> & args,
		   dtcan::TYPE type );

    /// generic command
    void cmd( const std::string & board,
	      const std::vector<std::string> & args,
	      dtcan::TYPE type );

    /// generic command
    void panic( enum SELECTION sel, const std::vector<std::string> & subset,
		const std::vector<std::string> & args );

    /// print all adc monitoring params for a given onet
    void readAdcChan( const std::string & board, char nadc, char nchan );

    /// read single adc
    void readOnetAdc( const std::string & board, char mezz, char onet );

    /// print all adc monitoring params for mezzanine
    void readMezzanineAdc( const std::string & board, char mezz );

    /// print all adc monitoring params for a given board
    void readAllAdc( const std::string & board );

    /// print all adc monitoring params for a given board
    void readAllAdc2( const std::string & board );

    /// print all adc monitoring params from all boards
    void readAllAdc( enum TYPE type=ANY );

    /// print all adc monitoring params from all readout boards
    inline void readAllRoAdc() { readAllAdc( RO ); };

    /// print all adc monitoring params from all trigger boards
    inline void readAllTrgAdc() { readAllAdc( TRG ); };

    /// read configuration of a specific ADC in a given board
    void readAdcConf( const std::string & board, char nadc );

    /// read configuration of all the ADC in a given board
    void readAdcConf( const std::string & board );

    /// read configuration of all the ADC in all the boards
    void readAdcConf( enum TYPE type=ANY );

    /// read configuration of all the ADC in all the readout boards
    inline void readAdcRoConf() { readAdcConf( RO ); };

    /// read configuration of all the ADC in all the trigger boards
    inline void readAdcTrgConf() { readAdcConf( TRG ); };


    //////////////////////////////////
    /// ONETS monitoring
    //////////////////////////////////

    /// print onet related params for a given onet in a given mezzanine
    void readOnet( const std::string & board, char mezz, char nonet );

    /// print all onets related params for mezzanine
    void readMezzanineOnets( const std::string & board, char mezz );

    /// print all onets related params
    void readAllOnets2( const std::string & board );

    /// print all onets related params
    void readAllOnets( const std::string & board );

    /// print all onets related params from all boards
    void readAllOnets( enum TYPE ype=ANY );

    /// print all onets related params from all readout boards
    inline void readAllRoOnets() { readAllOnets( RO ); };

    /// print all onets related params from all trigger boards
    inline void readAllTrgOnets() { readAllOnets( TRG ); };


    //////////////////////////////////
    /// ONETS config
    //////////////////////////////////


    /// config onet from DB
    void configOnetFromDB( const std::string & board, char mezz, char nonet );

    /// config all mezzanine onets from DB
    void configOnetsFromDB( const std::string & board, char mezz );

    /// config all boards onets from DB
    void configOnetsFromDB( const std::string & board,
			    enum TYPE type=ANY );

    /// write modulation and bias for a specific onet [control= 0xd2; equalizer = 0x0;]
    void writeOnet( const std::string & board, char mezz, char nonet,
		    short mod, short bias, bool isFullConfig );

    /// write all onets related params for board
    void writeBoardOnets( const std::string & board, short mod, short bias,
			  bool isFullConfig );

    /// write modulation and bias for all onets in a mezzanine [control= 0xd2; equalizer = 0x0;]
    void writeMezzanineOnets( const std::string & board, char mezz,
			      short mod, short bias, bool isFullConfig );

    /// write modulation and bias for all onets in a board [control= 0xd2; equalizer = 0x0;]
    void writeAllOnets( const std::string & board, short mod, short bias,
			bool isFullConfig );

    /// write modulation and bias for all onets related params from all boards [control= 0xd2; equalizer = 0x0;]
    void writeAllOnets( short mod, short bias, enum TYPE type=ANY,
			bool isFullConfig=false );

    /// write modulation and bias for all onets related params from all readout boards [control= 0xd2; equalizer = 0x0;]
    void writeAllTrgOnets( short mod, short bias, bool isFullConfig ) {
      writeAllOnets( mod, bias, TRG, isFullConfig );
    };

    /// write modulation and bias for all onets related params from all trigger boards [control= 0xd2; equalizer = 0x0;]
    void writeAllRoOnets( short mod, short bias, bool isFullConfig ) {
      writeAllOnets( mod, bias, RO, isFullConfig );
    };

    //////////////////////////////////
    /// ONETS config
    //////////////////////////////////

    /// copy ram content to flash for a single board, default all the boards
    void writeRamToFlash( const std::string & board="",
			  enum TYPE type=ANY );

    /// copy ram content to flash
    void writeRamToFlash( const std::string & name, dtcan::CuOFBoard & board );

    /// copy flash content to ram for a single board, default all the boards
    void writeFlashToRam( const std::string & board="",
			  enum TYPE type=ANY );

    /// copy flash content to ram
    void writeFlashToRam( const std::string & name, dtcan::CuOFBoard & board );


    /// restore flash contento to onets
    void restoreFromFlash( const std::string & board="",
			   enum TYPE type=ANY );

    /// restore flash contento to onets
    void restoreFromFlash( const std::string & name,
			   dtcan::CuOFBoard & board );

    /// load
    void onetsFromRamToDB( const std::vector<std::string> & args );

    /// spoil difference in onet HW progamming with DB settings
    void diffOnetsWithDB( const std::string & board="",
			  enum TYPE type=ANY );

    ///  spoil difference in onet HW progamming with DB settings
    void diffOnetsWithDB( const std::string & name,
			  dtcan::CuOFBoard & board );

    ///  spoil difference in onet HW progamming with DB settings
    void dumpOnetsFromDB();

    /// compare flash content with ram content for a single board, default all the boards
    void compareRamToFlash( const std::string & board="",
			    enum TYPE type=ANY );

    /// compare flash content with ram content
    void compareRamToFlash( const std::string & name, dtcan::CuOFBoard & board );

    /// write onet configuration from ram for a single board, default all the boards
    void writeRamToOnet( const std::string & board="",
			 enum TYPE type=ANY );

    /// write onet configuration from ram
    void writeRamToOnet( const std::string & name, dtcan::CuOFBoard & board );

    /// print default ram block where flash is loaded at startup for a single board, default all the boards
    void readRamBlockAtStartup( const std::string & board="",
				enum TYPE type=ANY );

    /// print default ram block where flash is loaded at startup
    void readRamBlockAtStartup( const std::string & name, dtcan::CuOFBoard & board );

    /// print version
    void version( const std::string & name, dtcan::CuOFBoard & board );

    /// print version of a single board, default all the boards
    void version( const std::string & board="",
		  enum TYPE type=ANY );

    /// read Id of a given board
    void readId( const std::string & board,
		 enum TYPE type=ANY );

    /// writeId to a given board
    void writeId( const std::string & board );

    /// enable killMaster
    void killMaster( const std::string & board,
		     enum TYPE type=ANY );

    /// disable killMaster
    void restoreMaster( const std::string & board,
			enum TYPE type=ANY );

    /// status killMaster
    void statusMaster( const std::string & board,
		       enum TYPE type=ANY );

    /// status killMaster
    void statusBackup( const std::string & board,
		       enum TYPE type=ANY );

    /// reset fpga
    void reset( const std::string & board,
		enum TYPE type=ANY );

    /// enable silent mode
    void silent( const std::string & board,
		 const std::vector<std::string> & args,
		 enum TYPE type=ANY );

    /// read/set timer
    void timer( const std::string & board,
		const std::vector<std::string> & args,
		enum TYPE type=ANY );

    /// read CAN Registers
    void readCanRegisters( const std::string & board,
			   enum TYPE type=ANY );
    /// read CAN Counters
    void readCanCounters( const std::string & board,
			  enum TYPE type=ANY );
    /// reset CAN Registers
    void resetCanCounters( const std::string & board,
			   enum TYPE type=ANY );


    ////////////////////////////////////////////
    /// this is the set of commands doing popen
    /// for firmware related issues
    ////////////////////////////////////////////
    void actel( const std::string & board,
		const std::vector<std::string> & args,
		enum TYPE type=ANY );

    void actel( const std::string & board,
		const std::string & exec, const std::string & action,
		const std::string & file_dat,
		enum TYPE type=ANY );
  };

};


#endif
