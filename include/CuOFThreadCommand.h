#ifndef _CUOF_THREAD_COMMAND_
#define _CUOF_THREAD_COMMAND_

#include "CuOFThread.h"

namespace dtcan {

  ////////////////////////////
  /// Command class
  class CuOFThreadCommand {

    bool force2nd_;
    static std::string result[4];
    std::map< std::string, dtcan::adconet * > adconet_map_;
    std::map< std::string, dtcan::onet * > onet_map_;
    std::map< std::string, dtcan::adc * > adc_map_;

    size_t nThreads_;
    std::vector<int> tids_;

  public:

    /// default constructor for test apps: includes configuration from file
    CuOFThreadCommand( const std::string & canconf,
		       const std::string & boardconf,
		       const std::string & adcconf )
      : force2nd_( false )
    { dtcan::CuOFThread::init( canconf, boardconf, adcconf ); };



    /// default constructor for test apps: includes configuration from file
    CuOFThreadCommand( const std::string & dbKey)
      : force2nd_( false ) { dtcan::CuOFThread::init( dbKey ); };

    /// default constructor for test apps: includes configuration from file
    CuOFThreadCommand()
      : force2nd_( false ) {};

    /// configure from files
    void configure( const std::string & canconf,
		    const std::string & boardconf,
		    const std::string & adcconf ) {
      dtcan::CuOFThread::init( canconf, boardconf, adcconf );
    }

    /// configure from database
    void configureFromDB( const std::string & dbKey ) {
      dtcan::CuOFThread::init( dbKey );
    }


    /// destructor
    ~CuOFThreadCommand() {
      clearAdconetMap();
      dtcan::CuOFThread::destroy();
    };

    size_t allThreads();
    void applySelection( enum SELECTION sel,
			 const std::vector<std::string> & subset,
			 bool force2nd=false );

    /// a cli selector for read actel command
    void actelCLI( dtcan::TYPE type, const std::vector<std::string> & args );

    /// a cli selector for read adc
    void adcCLI( dtcan::TYPE type );

    /// a cli selector for read onet
    void onetCLI( dtcan::TYPE type );
    
    /// a cli selector for write onet
    void writeOnetCLI( dtcan::TYPE type,
		       const std::vector<std::string> & args,
		       bool isfullConfig=false );

    /// a cli selector for read adcconf
    void adcconfCLI( dtcan::TYPE type );

    /// base threading methods
    void readAdcConf( enum TYPE type );
    void readAllAdc( enum TYPE type );
    void readAllOnets( enum TYPE type );
    void configureAllOnets( enum TYPE type, char mod, char bias );
    void writeAllOnets( enum TYPE type, char mod, char bias, bool isFullConfig );

    /// print all adc monitoring params from all boards
    inline void readAdcConf() { readAdcConf( ANY ); };

    /// print all adc monitoring params from all boards
    inline void readAdcRoConf() { readAdcConf( RO ); };

    /// print all adc monitoring params from all boards
    inline void readAdcTrgConf() { readAdcConf( TRG ); };

    /// print all adc monitoring params from all boards
    inline void readAllAdc() { readAllAdc( ANY ); };

    /// print all adc monitoring params from all readout boards
    inline void readAllRoAdc() { readAllAdc( RO ); };

    /// print all adc monitoring params from all trigger boards
    inline void readAllTrgAdc() { readAllAdc( TRG ); };

    /// print all onets related params from all boards
    inline void readAllOnets() { readAllOnets( ANY ); };

    /// print all onets related params from all readout boards
    inline void readAllRoOnets() { readAllOnets( RO ); };

    /// print all onets related params from all trigger boards
    inline void readAllTrgOnets() { readAllOnets( TRG ); };

    /// write modulation and bias for all onets in a board [control= 0xd2; equalizer = 0x0;]
    inline void writeAllOnets( char mod, char bias, bool isFullConfig )
    { writeAllOnets( ANY, mod, bias, isFullConfig ); };

    /// write modulation and bias for all onets related params from all trigger boards [control= 0xd2; equalizer = 0x0;]
    inline void writeAllRoOnets( char mod, char bias, bool isFullConfig )
    {	writeAllOnets( RO, mod, bias, isFullConfig ); };

    /// write modulation and bias for all onets related params from all readout boards [control= 0xd2; equalizer = 0x0;]
    inline void writeAllTrgOnets( char mod, char bias, bool isFullConfig )
    {	writeAllOnets( TRG, mod, bias, isFullConfig ); };

    /// clear
    void clearAdconetMap();
    void clearOnetMap();
    void clearAdcMap();

    /// print
    void printAdcMap();
    void printAdcMap( bool trgOnly );
    void printOnetMap();
    void printOnetMap( bool trgOnly );
    void printAdcConfigMap();
    void printAdcConfigMap( bool trgOnly );


    /*     /// copy ram content to flash */
    /*     void writeRamToFlash(); */

    /*     /// copy flash content to ram */
    /*     void writeFlashToRam(); */

    /*     /// compare flash content with ram content */
    /*     void compareRamToFlash(); */

    /*     /// write onet configuration from ram */
    /*     void writeRamToOnet(); */

    /*     /// print default ram block where flash is loaded at startup */
    /*     void readRamBlockAtStartup(); */

    /*     /// print status */
    /*     void readStatus(); */

    /*     /// print version */
    /*     void version(); */

    ////////////////////////////////////////////
    /// this is the set of commands doing popen
    /// for firmware related issues
    ////////////////////////////////////////////
    void actel( const std::string & exec, const std::string & action,
		const std::string & file_dat, enum TYPE type=ANY );

  };

};

#endif
