#ifndef _ECB_CANERROR_
#define _ECB_CANERROR_

#include <exception>
#include <cstring>
#include <stdio.h>

namespace dtcanerror {


void formatHwError( char code, char *msg, const char * type, int dev );
void formatMsgSyntaxError( char code, char *msg, const char * type );
void formatMsgBusyError( char code, char *msg, const char * type );


class tcpExcp: public std::exception
{
  int errornumber;
 public:
  tcpExcp(int err) : errornumber(err){}
};

class tcpTimeout : public tcpExcp
{  
  char pmsg[21];
 public:
  tcpTimeout(int err): tcpExcp(err){
    sprintf( pmsg, "Network timed out..." );
  };
    virtual const char* what() const throw()
    {
    return pmsg;
    };
};


class badaddress: public std::exception
{
 public:
  char value;
  char * pmsg;
  char msg[50];
  badaddress( char v) : value(v), pmsg(msg) {};
};

class badsocket: public badaddress
{
 public:
  badsocket( const char * v ) : badaddress('0')
    {
      snprintf( pmsg, 50, "Wrong socket %s", v );
    }
    virtual const char* what() const throw()
    {
      return pmsg;
    }
};

class badconfig: public badaddress
{
 public:
  badconfig( char v, char exp ) : badaddress(v)
    {
      sprintf( pmsg, "Wrong mapping, only %d values, expected %d", value, exp );
    };
    virtual const char* what() const throw()
    {
      return pmsg;
    }
};


class badboard: public badaddress
{
 public:
  badboard( const char * v ) : badaddress('0')
    {
      snprintf( pmsg, 50, "Wrong board name %s", v );
    }
    virtual const char* what() const throw()
    {
      return pmsg;
    }
};

class badwheel: public badaddress
{
 public:
  badwheel( char v ) : badaddress(v){};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong wheel number %d", value );
      return pmsg;
    }
};


class badmezzanine: public badaddress
{
 public:
  badmezzanine( char v ) : badaddress(v){};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong mezzanine number %d", value );
      return pmsg;
    }
};



class badonet: public badaddress
{
 public:
  badonet( char v ) : badaddress(v){};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong onet number %d", value );
      return pmsg;
    }
};



class badregister: public badaddress
{
 public:
  badregister( char v ) : badaddress(v){};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong register number %d", value );
      return pmsg;
    }
};


class badadc: public badaddress
{
 public:
  badadc( char v ) : badaddress(v){};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong adc number %d", value );
      return pmsg;
    }
};




class badheader: public std::exception
{
 public:
  int value;
  char * pmsg;
  char msg[50];
  badheader( int v ) : value(v), pmsg(msg) {};
    virtual const char* what() const throw()
    {
      sprintf( pmsg, "Wrong header returned %d", value );
      return pmsg;
    }
};




class badmsg: public std::exception
{
 public:
  char error;
  char device;
  char code;
  char * pmsg;
  badmsg( char e, char d, char v )
    : error(e), device(d), code(v), pmsg(msg) {};

 protected:
  char msg[50];
  void format( const char * type ) {
    if ( error == 0x50 )  formatHwError( code, pmsg, type, device );
    else if ( error == 0x63 && device == 0x01 ) formatMsgSyntaxError( code, pmsg, type );
    else if ( error == 0x63 && device == 0x02 ) formatMsgBusyError( code, pmsg, type );
    else sprintf( pmsg, "Unknown %s Error in CAN write message %02hhx %02hhx %02hhx", type, error, device, code );
  };
};



class readerror: public badmsg
{
 public:
  readerror( char e, char d, char v )
    : badmsg( e, d, v ) {
      format( "Read" );
  };

    virtual const char* what() const throw() 
    {
      return pmsg;
    };
};


class writeerror: public badmsg
{
 public:
  char *buffer;
  writeerror( char e, char d, char v )
    : badmsg( e, d, v ) {
    format( "Write" );
  };
    virtual const char* what() const throw() 
    {
      return pmsg;
    };
};



class CanError: public std::exception
{
 public:
  char * pmsg;
  char msg[50];
 CanError( const char* v ) : pmsg(msg) {
    sprintf( pmsg, "%s", v );
  };
  virtual const char* what() const throw()
  {
    return pmsg;
  }
};

 
};

class DbException: public std::exception
{
 public:
  char * pmsg;
  char msg[50];
  int rc;
  DbException( const char* v, int val ) : pmsg(msg), rc(val) {
    sprintf( pmsg, "%s. Returned %d", v, rc );
  };
  virtual const char* what() const throw()
  {
    return pmsg;
  }
};

#endif

