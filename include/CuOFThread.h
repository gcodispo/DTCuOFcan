#ifndef _CUOF_THREAD_
#define _CUOF_THREAD_

#include "CuOFCan.h"
#include <pthread.h>

namespace dtcan {

  const int NUM_THREADS = 10;

  class CuOFThread
  {

  public:
    CuOFThread( int tid ) : id_(tid), trgOnly_(false), roOnly_(false) {}

    void setRoOnly() { trgOnly_ = false; roOnly_ = true; };
    void setTrgOnly() { trgOnly_ = true; roOnly_ = false; };
    virtual ~CuOFThread() {/* empty */}

    /** Returns true if the thread was successfully started, false if there was an error starting the thread */
    bool start()
    {
      /* Initialize and set thread detached attribute */
      //pthread_attr_t attr;
      //pthread_attr_init(&attr);
      //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
      //pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
      // return (pthread_create(&_thread, &attr, ThreadWorkFunc, this) == 0);
      return (pthread_create(&_thread, NULL, ThreadWorkFunc, this) == 0);
    }

    /** Will not return until the internal thread has exited. */
    void join()
    {
      //void *end;
      //(void) pthread_join(_thread, &end);
      (void) pthread_join(_thread, NULL);
    }

    /// config from files: this will allow sharing info
    static void init( const std::string & canconf,
		      const std::string & boardconf,
		      const std::string & adcconf );

    /// config from dataase: this will allow sharing info
    static void init( const std::string & dbKey );

    static void destroy();
    static CuOFCan * can_;
    static bool isInit;
    inline static int getNumberOfSockets() { return can_->getNumberOfSockets(); };

  protected:
    int id_;
    bool trgOnly_;
    bool roOnly_;
    /** Implement this method in your subclass with the code you want your thread to run. */
    virtual void ThreadWork() = 0;

  private:
    static void * ThreadWorkFunc(void * This) {
      ((CuOFThread *)This)->ThreadWork();
      return NULL;
    }

    pthread_t _thread;
  };



  ////////////////////////////
  /// Derived classes
  class readAllAdc : public CuOFThread
  {
  public:
    std::map< std::string, dtcan::adconet * > adconetmap_;
    virtual ~readAllAdc() {/* empty */}
    virtual void ThreadWork();
    readAllAdc( int tid ) : CuOFThread(tid) {/* empty */};
  };


  class readAdcConf : public CuOFThread
  {
  public:
    std::map< std::string, dtcan::adc * > adcmap_;
    virtual ~readAdcConf() {/* empty */}
    virtual void ThreadWork();
    readAdcConf( int tid ) : CuOFThread(tid) {/* empty */};
  };


  class readAllOnets : public CuOFThread
  {
  public:
    std::map< std::string, dtcan::onet * > onetmap_;
    virtual ~readAllOnets() {/* empty */}
    virtual void ThreadWork();
    readAllOnets( int tid ) : CuOFThread(tid) {/* empty */};
  };


  class writeAllOnets : public CuOFThread
  {
  public:
    std::map< std::string, dtcan::onet * > onetmap_;
    virtual ~writeAllOnets() {/* empty */}
    virtual void ThreadWork();
    writeAllOnets( int tid, bool isFullConfig )
      : CuOFThread(tid), isFullConfig_(isFullConfig) {/* empty */};
    inline void setParams ( char mod, char bias ) { mod_ = mod; bias_ = bias; }
  protected:
    bool isFullConfig_;
    char mod_;
    char bias_;
  };


  ////////////////////////////////////////////
  /// this is the set of commands doing popen
  /// for firmware related issues
  ////////////////////////////////////////////

  class actel : public CuOFThread
  {
  public:
    virtual ~actel() {/* empty */}
    virtual void ThreadWork();
    actel( int tid, const std::string & exec, const std::string & action,
	   const std::string & file_dat, bool force2nd )
      : CuOFThread(tid), exec_(exec), action_(action), file_dat_(file_dat), force2nd_(force2nd)
    {/* empty */};
  protected:
    std::string exec_;
    std::string action_;
    std::string file_dat_;
    bool force2nd_;
  };


};

#endif

