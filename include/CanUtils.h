#ifndef _CUOF_CAN_UTILS_
#define _CUOF_CAN_UTILS_

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sys/time.h>
#include <time.h>

namespace dtcan {
  void readFile( const std::string & path,
		 std::vector< std::vector<std::string> > & res );
  int convert( const char* val );
};

/// utility: print map pair
inline void printMapElement( std::pair< const std::string, size_t > & t )
{
  std::cout << t.first << " : " << t.second << ";   ";
};

int getExeTime();
int getExeTime2();
std::vector<std::string> execute( const std::string & command );
void execute( const std::string & command, std::ostream & out );


#endif
