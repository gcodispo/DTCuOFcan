#ifndef _CUOF_CAN_
#define _CUOF_CAN_

#include "CANmsg.h"
#include "Ctcpux.h"
#include "CanUtils.h"
#include <iostream>
#include <iomanip>
#include <map>
#include <vector>

#include "CtcpSocket.h"
#include "CuOFBoard.h"

namespace CuOFDb {
  class DbObject;
};

namespace dtcan {

  class CuOFBoard;

  enum TYPE { RO, TRG, ANY };
  enum SELECTION { CRATE, WHEEL, ALL };

  /// CuOFCan cass
  class CuOFCan {

    short nSockets_;
    // bool useDb_;
    std::map< std::string, CtcpSocket *> cansock_;
    std::map< std::string, CuOFBoard > boards_;
    std::map< short, std::vector<CuOFBoard * >  > crate_;

    void fillParams( const std::string & path );
    void fillBoardParams( const std::string & path );
    void fillAdcMap( const std::string & path );

    void fillParamsFromDB( CuOFDb::DbObject & db );
    void fillBoardParamsFromDB( CuOFDb::DbObject & db,
				const std::string & key );
    void fillMezzanineParamsFromDB( CuOFDb::DbObject & db,
				    const std::string & key );
    void fillAdcMapFromDB( CuOFDb::DbObject & db );

  public:
    /// default constructor: to be followed by a configure method
    CuOFCan() : nSockets_(0) {};

    ///  constructor: to be followed by a configure method from DB
    CuOFCan( const std::string & dbKey ) : nSockets_(0) {
      configureFromDB( dbKey );
    };

    /// default constructor for test apps: includes configuration from file
    CuOFCan( const std::string & canconf,
	     const std::string & boardconf,
	     const std::string & adcconf ) {
      configure( canconf, boardconf, adcconf );
    };

    /// destructor
    ~CuOFCan() {
      std::map<std::string, CtcpSocket *>::iterator end = cansock_.end();
      std::map<std::string, CtcpSocket *>::iterator it = cansock_.begin();
      for ( ; it != end; ++it ) {
	delete it->second;
      }
    };

    /// configur from files
    void configure( const std::string & canconf,
		    const std::string & boardconf,
		    const std::string & adcconf );

    /// configur from database
    void configureFromDB( const std::string & key );

    /// get number of loaded sockets
    inline short getNumberOfSockets() const { return nSockets_; };

    /// connect all sockets
    void connect();

    /// connect a give socket
    CtcpSocket * connect( const std::string & name );

    /// disconnect all sockets
    void release();

    /// disconnect a give socket
    CtcpSocket * release( const std::string & name );

    /// disconnect a give socket
    CtcpSocket * release( short id );

    /// build socket for can access
    CtcpSocket * getSocket( const std::string & name );

    /// subscript operator
    CuOFBoard& operator[] ( const std::string & sid );

    /// configured boards
    size_t size() { return boards_.size(); };

    /// begin
    std::map< std::string, CuOFBoard >::iterator begin() {
      return boards_.begin();
    };

    /// end
    std::map< std::string, CuOFBoard >::iterator end() {
      return boards_.end();
    };

    /// begin
    std::map< std::string, CtcpSocket * >::iterator canBegin() {
      return cansock_.begin();
    };

    /// end
    std::map< std::string, CtcpSocket * >::iterator canEnd() {
      return cansock_.end();
    };

    //

    /* typedef std::map< short, std::vector<std::string> > CrateMap; */
    /* typedef std::map< short, std::vector<std::string>  >::const_iterator CrateMapIterator; */
    /* typedef std::vector<std::string> Crate; */
    /* const Crate & operator[] (const int cid) const { */
    /*   CrateMapIterator it = crate_.find(cid); */
    /*   if ( it == crate_.end() ) */
    /* 	throw CanError("bad crate"); */
    /*   return it->second; */
    /* }; */

    int insertNewKey( const std::string & key );
    int writeBoardParamsFromDB( CuOFDb::DbObject & db,
				const std::string & key,
				const CuOFBoard & board );

    int writeMezzanineParamsFromDB( CuOFDb::DbObject & db,
				    const std::string & key, int mezz,
				    const CuOFBoard & board );
  };

};


#endif


