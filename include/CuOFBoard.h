#ifndef _CUOF_BOARD_
#define _CUOF_BOARD_

#include "CANmsg.h"
#include "Ctcpux.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include "CtcpSocket.h"
#include "CanErrors.h"

namespace dtcan {

  enum master { MASTER=0, BACKUP=1, BACKUP_KM=2, ERROR=3 };

  // struct holding ONET registers
  struct onet {
    char mezzanine;
    char nonet;
    unsigned char control;    /* REG 0 */
    unsigned char modulation; /* REG 1 */
    unsigned char bias;       /* REG 2 */
    unsigned char equalizer;  /* REG 3 */
    onet() : mezzanine(-1), nonet(-1), control(-1), modulation(-1), bias(-1), equalizer(-1) {};
  };
  /// streaming operator for onet
  std::ostream& operator<< (std::ostream& os, const onet & ot );


  // struct holding ADC registers mapping
  struct adcpair {
    char adc;
    char ch;
    adcpair() : adc(-1), ch(-1) {};
    adcpair( const char & a, const char & c ) : adc(a), ch(c) {};
  };
  /// streaming operator for adcpair
  std::ostream& operator<< (std::ostream& os, const adcpair & adc );


  // struct holding ADC registers per onet
  struct adcmap {
    char onet;
    adcpair temp;
    adcpair monb;
    adcpair flt;
    adcpair monp;
    adcmap() : onet(-1) {};
  };
  /// streaming operator for adcmap
  std::ostream& operator<< (std::ostream& os, const adcmap & adc );


  // struct holding ADC registers values
  struct adconet {
    char mezzanine;
    char onet;
    char monb;
    char flt;
    char monp;
    char tempi;
    float temp;
    adconet() : mezzanine(-1), onet(-1), monb(-1),
		flt(-1), monp(-1), tempi(-1), temp(-1) {};
  };
  /// streaming operator for adconet
  std::ostream& operator<< (std::ostream& os, const adconet & adc );


  // struct holding ADC registers (?)
  struct adc {
    char n;
    char config;
    char setup;
    adc() : n(-1), config(-1), setup(-1) {};
  };
  /// streaming operator for adc
  std::ostream& operator<< (std::ostream& os, const adc & adc );

  /// version time struct
  struct v_time {
    short year;
    short month;
    short day;
    short hour;
  };


  /// formatting for text
  template<class charT, class traits>
    std::basic_ostream<charT, traits>&
    textFiller(std::basic_ostream<charT, traits>& os)
    {
      os << std::setiosflags(std::ios::right) << std::setfill (' ') << std::setw(5);
      return os;
    }

  /// formatting for int
  template<class charT, class traits>
    std::basic_ostream<charT, traits>&
    digiFiller(std::basic_ostream<charT, traits>& os)
    {
      os << std::setfill (' ') << std::setw(2) << std::setiosflags(std::ios::right);
      return os;
    }


  void readFile( const std::string & path,
		 std::vector< std::vector<std::string> > & res );
  int convert( const char* val );

  /// CuOFBoard cass
  class CuOFBoard {

  public:
    static const size_t nmezzanines;
    static const size_t nonets;
    static const size_t ntotonets;
    static const size_t nregisters;
    static const size_t ndata;
    static const size_t nadc;

    /// adc mapping
    static adcmap adcmap_[];
    static void fillAdcMap( const std::string & reg,
			    int nonet, char adc, char chan );

    /// internal members
  private:
    char base_;
    char mask_;
    char isTrg_;
    bool killmaster_;
    std::string name_;
    CtcpSocket *cansocket_;
    CtcpSocket *cansocketKM_;
    CANTxMessage canTx_;
    CANRxMessageBuffer canRx_;
    char mbuf_[1024];
    size_t sizebuf_;
    unsigned char modulation_[4][8];
    unsigned char bias_[4][8];

    /// CAN acces through socket
    short read ( size_t nframes = 1 );
    short read ( size_t nframes, bool killmaster );

    /// constants: to be put in a db?
  public:
    static const char writeCode;
    static const char readCode;
    static const char markerRomProg;
    static const char bufActiveAtStartup;
    static const char timeRefreshL;
    static const char timeRefreshH;
    static const char adcBaseConf;
    static const char adcBaseSetup;
    static const char ramDefaultBlock;
    static const char readOkCode;
    static const char writeOkCode;
    static const short headcode;
    static bool force1st_;
    static bool force2nd_;



    std::vector<char> getActiveList() {
      std::vector<char> res;
      if ( mask_ & 0x1 ) res.push_back(1);
      if ( mask_ & 0x2 ) res.push_back(2);
      if ( mask_ & 0x4 ) res.push_back(3);
      if ( mask_ & 0x8 ) res.push_back(4);
      return res;
    };


    inline bool isActiveMezzanine( char m ) const {
      return ( mask_ & (0x01<<m ) );
    };

    inline bool isActiveMezzanine( const onet & m ) const {
      // std:: cout << "########################### " << mask_ << ' ' << m.mezzanine << ' ' << ( mask_ & (0x01<<m.mezzanine ) ) << std::endl;
      return ( mask_ & (0x01<<m.mezzanine ) );
    };

    inline bool isActiveMezzanine( const adconet & m ) const {
      // std:: cout << "########################### " << mask_ << ' ' << m.mezzanine << ' ' << ( mask_ & (0x01<<m.mezzanine ) ) << std::endl;
      return ( mask_ & (0x01<<m.mezzanine ) );
    };

    inline std::string name() const { return name_; };

    inline char base() const { return base_; };

    inline char mask() const { return mask_; };

    inline bool isTrg() const { return ( isTrg_ != 0 ); };

    inline bool isRo() const { return ( isTrg_ == 0 ); };

    inline short getSocketId() const { return cansocket_->id; };

    inline const int iwheel() const { return cansocket_->iwheel; };

    inline const std::string & wheel() const { return cansocket_->wheel; };

    inline const std::string & crate() const { return cansocket_->crate; };

    inline const short port() const { return cansocket_->port; };

    inline const char * host() const { return cansocket_->host; };


  public:
    /// default constructor: to be followed by a configure method
    CuOFBoard() {};

    /// default constructor for test apps: includes configuration from file
    explicit CuOFBoard( char base, char mask, char isTrg,
			const std::string & name,
			CtcpSocket * cansock, CtcpSocket * cansock_km );

    /// generic command
    std::vector<unsigned short> cmd( bool read, unsigned short iHigh,
				     unsigned short iLow,
				     unsigned short subIndex,
				     unsigned short data=0x00 );
    // store laser settings
    void setOnetSettings( int mezz, int nonet, unsigned char mod, unsigned char bias )
    {
      testMezzanine( mezz );
      testOnet( nonet );
      modulation_[mezz][nonet] = mod;
      bias_[mezz][nonet] = bias;
    }

    inline unsigned char getOnetBias( int mezz, int nonet ) const
      { return bias_[mezz][nonet]; }

    inline unsigned char getOnetModulation( int mezz, int nonet ) const
      { return modulation_[mezz][nonet]; }

    /// copy flash content to RAM for a given block
    int flashToRam( char block = CuOFBoard::ramDefaultBlock );

    /// copy flash content to RAM then to onets
    void restoreFromFlash( char block = CuOFBoard::ramDefaultBlock );

    // read firmware version
    v_time readFwVersion();

    // read firmware version
    v_time readSwVersion();

    /// copy RAM content to flash for a given block
    int ramToFlash( char block = CuOFBoard::ramDefaultBlock );

    /// compare RAM content to flash for a given block
    int compareRamToFlash( char block = CuOFBoard::ramDefaultBlock );

    /// select block to be loaded at startup
    int setRamBlockAtStartup( char block = CuOFBoard::ramDefaultBlock );

    /// retrieve which block to is loaded at startup
    int readRamBlockAtStartup();

    /// write board ID in the board flash
    int writeId();

    /// write board ID in the board flash
    int readId();

    /// enable killMaster
    master killMaster();

    /// restore master
    master restoreMaster();

    /// read master status
    master statusMaster();

    /// read backup status
    master statusBackup();

    /// config onet from DB
    void configOnetFromDB( char mezz, char nonet );

    /// config all mezzanine onets from DB
    void configOnetsFromDB( char mezz );

    /// config all board onets from DB
    void configOnetsFromDB();

    /// load all board onets settings
    void loadOnetFromRam();

    /// diff onet content with DB
    int diffOnetsWithDB();

    /// force use of primary fpga
    inline static void force1st() { force1st_ = true; force2nd_ = false; };

    /// force use of secondary fpga
    inline static void switchTo2nd() { force1st_ = false; force2nd_ = true; };

    /// release fpga requests and back to normal
    inline static void switchTo1st() { force1st_ = false; force2nd_ = false; };

    /// reset FPGA
    void reset();

    /// read silent
    unsigned short silent();

    /// enable/disable silent
    void silent( bool enable );

    /// read timer
    unsigned short timer();

    /// set timer
    unsigned short timer( unsigned short t_value );

    /// read CAN Registers
    unsigned short readCanRegisters( unsigned short reg );

    /// read CAN Registers
    std::vector<std::string> readCanRegisters();

    /// read CAN Counters
    unsigned short readCanCounters( unsigned short count );

    /// read CAN Counters
    std::map<std::string, size_t> readCanCounters();

    /// reset CAN Registers
    void resetCanCounters();

    //////////////////////////////////
    /// ONETS monitoring
    //////////////////////////////////

    /// read single onet from RAM content, struct contains info about mezz/nonet
    int readOnet( onet & reply );

    /// read single onet from RAM content, starts from an empty struct
    int readOnet( char mezz, char nonet, onet & reply );

    /// read a set of ONETs from RAM, structs contains info about mezz/nonet
    int readOnets( onet * reply, size_t len );

    /// read mezzanine ONETs from RAM content, fills structs
    int readMezzanineOnets( char mezz, onet reply[] );

    /// read all mezzanines ONETs from RAM, fills structs
    int readAllOnets( onet reply[] );

    //////////////////////////////////
    /// ONETS config
    //////////////////////////////////


    /// loop over the specified set of ONET selectors and write the values from RAM to ONET
    int writeRamToOnet( onet * params, size_t len);

    /// write ONETS values of a given ONET from RAM to ONET
    int writeRamToOnet( char mezz );

    /// write all mezzanines ONETS values from RAM to ONETs
    int writeRamToOnet();

    /// loop over the full set of ONET selectors and write the values to the ram
    int writeOnetParamsToRam( onet * params, size_t len);

    /// configure onets: write to RAM, then to ONETs registers
    int configOnetsFromRam( onet * params, size_t len);

    /// configure onets: write to RAM, then to ONETs registers for both 1st and 2nd
    int configureOnets( onet * params, size_t len);

    /// expert command
    int writeOnetDirect( dtcan::onet * params, size_t len );
    //////////////////////////////////
    /// ADC monitoring
    //////////////////////////////////

    /// read ADC registers
    int readAdcChan( char nadc, char chan );

    /// read ADC registers asociated to a given ONET
    int readOnetAdc( char mezz, char nonet, adconet & reply );

    /// read mezzanine ADC registers
    int readMezzanineAdc( char mezz, adconet reply[] );

    /// read all ADC registers
    int readAllAdc( adconet reply[] );

    int readOnetTemp( char mezz, char nonet, adconet & reply );
    int readAllTemp( adconet reply[] );


    //////////////////////////////////
    /// ADC conf
    //////////////////////////////////

    /// read adc configuration and setup
    int readAdcConf( short n, adc & reply );

    /// write configuration to adc registers
    int writeAdcConf( const adc & reply );

    /// write configuration to adc registers
    int writeAdcConf( adc * params, size_t len );

    //////////////////////////////////
    /// miscellanea
    //////////////////////////////////

    /// inlined methods

    /// return write command code
    inline char getWriteCode() const { return writeCode; };

    /// return read command code
    inline char getReadCode() const { return readCode; };

    /// converts adc read value into physical value
    inline float convertTemp( char t ) const
      {
	// return ( t * 2.048 / 256  - 1.140 ) / 0.0082 ;
	// return (  (0xff &t) * 4.096 / 256  - 1.140 ) / 0.0082 ;
	return ( (0xff &t) * 0.937 - 76.8 );
      }

    /// internal ECB code for a given register in a given ONET
    inline char encode( char mezz, char nonet, char reg ) const
      {
	return ( mezz*0x20 + ( nonet*0x4 + reg ) );
      };

    /// decode internal ECB code into register, ONET, mezzanine
    inline void decode( char val, char & mezz, 
			char  & nonet, char & reg ) const
      {
	reg = val%4;
	nonet = (val % 0x20 ) / 4;
	mezz = val / 0x20;
      };

    /// internal ECB code for copy RAM to ONET
    inline char encodeRam( char mezz, char nonet ) const
      {
	char retval = 0x01;
	retval |= (nonet << 2);
	retval |= (mezz << 5);
	return retval;
      };

    /// internal ECB code for copy RAM to ONET
    inline char encodeRam( char mezz ) const
      {
	return  0x02 | (mezz << 5);
      };

    /// internal ECB code for copy RAM to ONET
    inline char encodeRam() const
      {
	return  0x03;
      };

    /// retrieve internal ECB code for a given adc configuration register
    inline char adcConf( char num ) const
      {
	return adcBaseConf + num * 2;
      };

    /// retrieve internal ECB code for a given adc setup register
    inline char adcSetup( char num ) const
      {
	return adcBaseSetup + num * 2;
      };


    //////////////////////////////////
    /// tests: throw exception if error
    //////////////////////////////////

    inline void checkRead()
      {
	if ( mbuf_[8] != dtcan::CuOFBoard::readOkCode )
	  throw dtcanerror::readerror( mbuf_[8], mbuf_[9], mbuf_[10] );
      };


    inline void checkWrite()
      {
	if ( mbuf_[8] != dtcan::CuOFBoard::writeOkCode )
	  throw dtcanerror::writeerror( mbuf_[8], mbuf_[9], mbuf_[10] );
      };



    /// check if onet/mezzanine are inside valid range
    inline void test( char mezz, char nonet, char reg ) const
      {
	if (  mezz < 0 || mezz > 3 ) throw dtcanerror::badmezzanine( mezz );
	if ( nonet < 0 || nonet > 7 ) throw dtcanerror::badonet( nonet );
	if (   reg < 0 || reg > 3 ) throw dtcanerror::badregister( reg );
      };

    /// check if adc number is inside valid range
    inline void testAdc( char adc ) const
      {
	if ( adc < 0 || adc > 11 ) throw dtcanerror::badmezzanine( adc );
      };

    /// check if onet number is inside valid range
    inline void testMezzanine( char mezz ) const
      {
	if (  mezz < 0 || mezz > 3 ) throw dtcanerror::badmezzanine( mezz );
      };

    /// check if onet number is inside valid range
    inline void testOnet( char nonet ) const
      {
	if ( nonet < 0 || nonet > 7 ) throw dtcanerror::badonet( nonet );
      };

    ////////////////////////////////////////////
    /// this is the set of commands doing popen
    /// for firmware related issues
    ////////////////////////////////////////////
    std::string actel( const std::string & exec,
		const std::string & action,
		const std::string & file_dat,
		bool use2nd=false );
  };
};

#endif


