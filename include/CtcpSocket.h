#ifndef _CtcpSocket_CAN_
#define _CtcpSocket_CAN_

#include "Ctcpux.h"

namespace dtcan {

  /// this is the socket handler
  struct CtcpSocket {
    bool connected;
    bool primary;
    short id;
    short port;
    short iwheel;
    std::string wheel;
    std::string crate;
    char host[15];
    Ctcp *cansock_;
    CtcpSocket( short i, short p, const std::string & h,
		int iw, const std::string & w, const std::string & c, 
		bool isp=true )
    : connected(false), primary(isp), id(i), port(p), iwheel(iw),
      wheel(w), crate(c) {
      snprintf( host, 15, "%s", h.c_str() );
    };

    ~CtcpSocket() {
      if ( connected ) delete cansock_;
    };

    Ctcp *getSocket() {
      if ( !connected ) {
	cansock_ = new Ctcp(0);
	cansock_->Connect( host, port );
	connected = true;
      }
      return cansock_;
    };

    void releaseSocket() {
      if ( connected ) {
	delete cansock_;
	connected = false;
      }
    };
  };
  bool operator==( const CtcpSocket & c1,  const CtcpSocket & c2 );
  bool operator<( const CtcpSocket & c1,  const CtcpSocket & c2 );
};


#endif


