#ifndef _CUOF_CAN_MSG_
#define _CUOF_CAN_MSG_


struct CANRxMessageBuffer{
      // SID of the Received CAN Message.
      unsigned SID:11;
    // Filter which accepted this message.
      unsigned FILHIT:5;
    // Time stamp of the received message. This is
    // valid only if the Timestamping is enabled.
      unsigned CMSGTS:16;
    // Data Length Control. Specifies the size of the
    // data payload section of the CAN packet. Valid
    // values are 0x0 - 0x8.
      unsigned DLC:4;
    // Reserved bit. Should be always 0.
      unsigned RB0:1;
      unsigned :3;
    // Reserved bit. Should be always 0.
      unsigned RB1:1;
    // Remote Transmit Request bit. Should be set for
    // RTR messages, clear otherwise.
      unsigned RTR:1;
    // CAN TX and RX Extended ID field. Valid values
    // are in range 0x0 - 0x3FFFF.
      unsigned EID:18;
    // Identifier bit. If 0 means that message is SID.
    // If 1 means that message is EID type.
      unsigned IDE:1;
    // Susbtitute Remote request bit. This bit should
    // always be clear for an EID message. It is ignored
    // for an SID message.
      unsigned SRR:1;
      unsigned :2;
    // This is the data portion of the CAN TX message.
      char data[8];
};

struct CANTxMessageBuffer{
    // CAN TX Message Standard ID. This value should
    // be between 0x0 - 0x7FF.
      unsigned SID:11;
      unsigned :21;
   // Data Length Control. Specifies the size of the
    // data payload section of the CAN packet. Valid
    // values are 0x0 - 0x8.
      unsigned DLC:4;
    // Reserved bit. Should be always 0.
      unsigned RB0:1;
      unsigned :3;
    // Reserved bit. Should be always 0.
      unsigned RB1:1;
    // Remote Transmit Request bit. Should be set for
    // RTR messages, clear otherwise.
      unsigned RTR:1;
    // CAN TX and RX Extended ID field. Valid values
    // are in range 0x0 - 0x3FFFF.
      unsigned EID:18;
    // Identifier bit. If 0 means that message is SID.
    // If 1 means that message is EID type.
      unsigned IDE:1;
    // Susbtitute Remote request bit. This bit should
    // always be clear for an EID message. It is ignored
    // for an SID message.
      unsigned SRR:1;
      unsigned :2;
    // This is the data portion of the CAN TX message.
      char data[8];
};

struct CANTxMessageHeader {
  unsigned short code;
  unsigned short size;
};


struct CANTxMessage {
  CANTxMessageHeader header;
  CANTxMessageBuffer body;
};

#endif
