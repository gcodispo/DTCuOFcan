#source /nfshome/dtdqm/.bashrc

if [ $# -lt 1 ]; then
    echo "parameters (all optional) are: wheel sector chamber channel"
    exit
fi
WHEEL="$1"
if [[ "$WHEEL" == "m2" ]]; then
    W="-2"
fi
if [[ "$WHEEL" == "m1" ]]; then
    W="-1"
fi
if [[ "$WHEEL" == "0" ]]; then
    W="0"
fi
if [[ "$WHEEL" == "p1" ]]; then
    W="1"
fi
if [[ "$WHEEL" == "p2" ]]; then
    W="2"
fi


MB=$3
CH=$4
MEZZ=""
if [ "$MB" != "" ]; then
    let MEZZ=MB-1
fi

MOD="40 50 60 70 80 90 100 110 120"
BIAS="18 22 26 30 34 38 42 46"
rm modbiasscanlog$WHEEL.txt
P=$PWD
for M in $MOD 
do
  for B in $BIAS
  do
    SEC=$2
    echo "S$SEC MEZZ$MB MOD=$M BIAS=$B" >>modbiasscanlog$WHEEL.txt
    if [[ "$WHEEL" != "" ]]; then
	if [[ "$SEC" != "" ]]; then
	    ../iocan --board YB"$WHEEL"_TRG"$SEC --writeonet $MEZZ $CH $M $B"
	else
	    for SEC in 1 2 3 4 5 6 7 8 9 10 11 12; do
		../iocan --board YB"$WHEEL"_TRG$SEC --writeonet $M $B
	    done
	fi
    else
	../iocan --board trg --writeonet $M $B  
    fi
    echo "Written VCSELs S$SEC MEZZ$MB MOD=$M BIAS=$B" 
    cd /nfshome0/dtdqm/tSC/rtsw_sc_manager_1.36/exe
    ./sc_monitorx_64.exe &> /dev/null < testmon.txt
##    ./sc_monitorx.exe &> /dev/null < testmon.txt
    echo "TSC status reset, sampling..."
    sleep 5
##    ./sc_monitorx.exe &> /dev/null  < testmon.txt
    ./sc_monitorx_64.exe &> /dev/null  < testmon.txt
    cd $P
    tail  -n 12  /nfshome0/dtdqm/tSC/rtsw_sc_manager_1.36/exe/monitor_wh"$W".txt | awk '{print $1,$2," | ",$32,$33,$34," ",$35,$36,$37," | ",$38,$39,$40," ",$41,$42,$43," | ",$44,$45,$46," ",$47,$48,$49," | ",$50,$51,$52," ",$53,$54,$55}' >> modbiasscanlog$WHEEL.txt
    echo "TSC stasus dumped"
    cd $P
    #read -n1 key
  done
done
#../iocan --board YBp1_TRG$SEC --writeonet $MEZZ 110 25
#source vcselLG.sh
#cp modbiasscanlog$WHEEL.txt modbiasscanlog_W$WHEEL"S"$SEC"MB"$MB"CH"$CH".txt"
