#source /nfshome/dtdqm/.bashrc

echo "it works only from ssh -Y dtdqm@srv-s2g16-28-01"


if [ $# -lt 1 ]; then
    echo "YOU NEED TO WRITE THE FOLLOWING PARAMETERS: wheel (-2..2) sector(1..12, 13, 14) chamber(1..4) rob(0..6)"
    exit
fi
WHEELa="$1"
SECTOR="$2"
MB=$3
CH=$4
if [[ "$WHEELa" == -2 ]]; then
    WHEEL="m2"
    W="-2"
fi
if [[ "$WHEELa" == -1 ]]; then
    WHEEL="m1"
    W="-1"
fi
if [[ "$WHEELa" == 0 ]]; then
    WHEEL="0"
    W="0"
fi
if [[ "$WHEELa" == 1 ]]; then
    WHEEL="p1"
    W="1"
fi
if [[ "$WHEELa" == 2 ]]; then
    WHEEL="p2"
    W="2"
fi

echo $WHEEL
echo $SECTOR
echo $MB
echo $CH

IOSEC=$SECTOR
IOMB=$(expr $MB - 1)


    if [ "$IOMB" == 3 ] && [ "$SECTOR" == 4 ] 
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=11
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=11
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=4
            IOCH=0
        elif [ "$CH" == 3 ]
        then
            IOSEC=4
            IOCH=1
        elif [ "$CH" == 4 ]
        then
            IOSEC=4
            IOCH=2
        fi
   elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 13 ]
   then
        if [ "$CH" == 0 ]
        then
            IOSEC=9
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=9
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=4
            IOCH=4
        elif [ "$CH" == 3 ]
        then
            IOSEC=4
            IOCH=5
        elif [ "$CH" == 4 ]
        then
            IOSEC=4
            IOCH=6
        fi
    elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 10 ]
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=10
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=10
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=10
            IOCH=6
        elif [ "$CH" == 3 ]
        then
            IOSEC=9
            IOCH=6
        fi
    elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 14 ]
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=10
            IOCH=0
        elif [ "$CH" == 1 ]
        then
            IOSEC=10
            IOCH=1
        elif [ "$CH" == 2 ]
        then
            IOSEC=10
            IOCH=2
        elif [ "$CH" == 3 ]
        then
            IOSEC=11
            IOCH=6
        fi
     fi



#si es una MB3 el mapping de CUOF es diferente
    if [ "$IOMB" == 2 ]
    then
      IOCH=$(expr 7 - $CH)
            echo "el canal para esa rob es $IOCH. esto es para una MB3"
    elif [[ "$CH" < 3 ]]
    then
            IOCH=$(expr 7 - $CH)
            echo "el canal para esa rob es $IOCH. No es una mb3, rob menor q 3"
    else
            IOCH=$(expr 7 - $(expr $CH + 1))
            echo "el canal para esa rob es $IOCH.  No es una mb3, rob mayor q 3"
    fi

echo "ooooooooooooooo  yb  $WHEEL sec $IOSEC cuofmezz $IOMB  cuofch $IOCH"
echo "the choosen chamber is : $MB, for IOCAN it is: $IOMB"
echo "the sector choosen is : $SECTOR, for IOCAN is: $IOSEC"
echo "the choosen rob is : $CH, for IOCAN it is: $IOCH"

SP=_

rm urosmodbiasscanlog$SP$WHEEL$SP$SECTOR$SP$MB$SP$CH.txt
rm /nfshome0/dtdqm/uroslinkstatuslog.txt
#rm uroslinkstatuslogANT.txt
source ./../dtcanenv.sh

ssh vmepc-s2g16-11-01 "~dtdqm/uros/disablemonitoring.sh"

#MOD="40 50 60 70 80 90 100 110 120"
#BIAS="16 18 22 26 30 34 38 42 46"
MOD="10 20 40 60 80 100"
BIAS="10 20 40 60 80 100"
#MOD="60 "
#BIAS="60"




for M in $MOD 
do
  for B in $BIAS
  do
        cd /nfshome0/dtdqm/TestCan/scripts/
	echo "CH= $CH MOD= $M BIAS= $B" >>urosmodbiasscanlog$SP$WHEEL$SP$SECTOR$SP$MB$SP$CH.txt
	../iocan --board YB"$WHEEL"_RO"$IOSEC --writeonet $IOMB $IOCH $M $B"	    
        echo "Written VCSELs ch=$CH MOD=$M BIAS=$B" 
        sleep 2


      cd ~dtdqm/uros/dtupy/
      source env.sh
      cd /nfshome0/dtdqm/uros/dtupy/etc/keys/urostemp/
     # rm partition.txt
     # ln -s /nfshome0/dtdqm/TestCan/scripts/partitionZERO.txt partition.txt
     # if [ "$WHEELa" == -2 ] || [ "$WHEELa" == -1 ]
    #  then
     #     rm partition.txt
     #     ln -s  /nfshome0/dtdqm/TestCan/scripts/partitionNEG.txt partition.txt
     # elif [ "$WHEELa" == 2 ] || [ "$WHEELa" == 1 ]
     # then
      #    rm partition.txt
      #    ln -s  /nfshome0/dtdqm/TestCan/scripts/partitionPOS.txt partition.txt
     # fi
      cd ~dtdqm/uros/dtupy/scripts/
      ssh vmepc-s2g16-11-01 "~dtdqm/uros/dtupy/scripts/run_status_cris.sh" 
      python ~dtdqm/uros/dtupy/scripts/errorformat.py $W $SECTOR $MB $CH >> /nfshome0/dtdqm/TestCan/scripts/urosmodbiasscanlog$SP$WHEEL$SP$SECTOR$SP$MB$SP$CH.txt 
      echo "uROS status dumped"
  done
done

ssh vmepc-s2g16-11-01 "~dtdqm/uros/enablemonitoring.sh"
