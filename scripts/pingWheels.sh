#!/bin/sh


exe=/nfshome0/dtdqm/TestCan/iocan

declare -a boards=(
    YBm2_TRG3
    YBm2_TRG9
    YBm1_TRG3
    YBm1_TRG9
    YB0_TRG3
    YB0_TRG9
    YBp1_TRG3
    YBp1_TRG9
    YBp2_TRG3
    YBp2_TRG9
)

for board in "${boards[@]}" ; do
    ${exe} --readId --board ${board} | grep -v '\*\*\*' | grep -v '==='
done