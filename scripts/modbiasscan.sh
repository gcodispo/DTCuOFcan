#source /nfshome/dtdqm/.bashrc
SEC=$1
MB=$2
CH=$3
MEZZ=""
if [ "$MB" != "" ]; then
    let MEZZ=MB-1
fi
MOD="40 50 60 70 80 90 100 110 120"
BIAS="18 22 26 30 34 38 42 46"
rm modbiasscanlog.txt
P=$PWD
for M in $MOD 
do
  for B in $BIAS
  do
    echo "S$SEC MEZZ$MB MOD=$M BIAS=$B" >>modbiasscanlog.txt
    echo "S$SEC MEZZ$MB MOD=$M BIAS=$B" 
    ../iocan --board YBp1_TRG$SEC --writeonet $MEZZ $CH $M $B
    cd /nfshome0/dtdqm/tSC/rtsw_sc_manager_1.36/exe
    ./sc_monitorx.exe < testmon.txt
    sleep 5
    ./sc_monitorx.exe < testmon.txt
    tail  -n 12  monitor_wh1.txt | awk '{print $1,$2," | ",$32,$33,$34," ",$35,$36,$37," | ",$38,$39,$40," ",$41,$42,$43," | ",$44,$45,$46," ",$47,$48,$49," | ",$50,$51,$52," ",$53,$54,$55}' >> /nfshome0/dtdqm/TestCan/modbiasscanlog.txt
    cd $P
    #read -n1 key
  done
done
../iocan --board YBp1_TRG$SEC --writeonet $MEZZ 110 25
#source vcselLG.sh
cp modbiasscanlog.txt modbiasscanlog_S$SEC"MB"$MB"CH"$CH.txt
