#!/bin/sh
	
YB=$1
CRATE=$2
echo $YB $CRATE 

if [ "$CRATE" == "top" ] ; then
 ../iocan  --board YB${YB}_TRG1   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG2   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG3   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG4-5 --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG4   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG5   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG6   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO1    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO2    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO3    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO4    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO5    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO6    --adc 0 1  | grep mezz
fi

if [ "$CRATE" == "bot" ] ; then
date
 ../iocan  --board YB${YB}_TRG7    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG8    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG9    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG9-10 --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG10   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG11   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_TRG12   --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO7     --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO8     --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO9     --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO10    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO11    --adc 0 1  | grep mezz
 ../iocan  --board YB${YB}_RO12    --adc 0 1  | grep mezz
fi


exit