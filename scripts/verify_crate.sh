#!/bin/sh

CANEXE=/nfshome0/dtdqm/DTCuOFcan/iocan
ACTELEXE=/nfshome0/venturas/Lorenzo/test-cuof/ActelDirectC27_CuOf
FILE=/nfshome0/venturas/Lorenzo/test-cuof/JTAG.dat
isgood=''

case "$1" in
    'YBm2_UP')
	isgood=yes
	;;
    'YBm2_DW')
	isgood=yes
	;;
    'YBm1_UP')
	isgood=yes
	;;
    'YBm1_DW')
	isgood=yes
	;;
    'YB0_UP')
	isgood=yes
	;;
    'YB0_DW')
	isgood=yes
	;;
    'YBp1_UP')
	isgood=yes
	;;
    'YBp1_DW')
	isgood=yes
	;;
    'YBp2_UP')
	isgood=yes
	;;
    'YBp2_DW')
	isgood=yes
	;;
    *)
	break
	;;
esac

if  [ "$isgood" ]; then
    ${CANEXE} --crates ${1} --actel ${ACTELEXE} verify ${FILE}
else
    echo "Bad crates name"
fi
