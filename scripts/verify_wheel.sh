#!/bin/sh

CANEXE=/nfshome0/dtdqm/DTCuOFcan/iocan
ACTELEXE=/nfshome0/venturas/Lorenzo/test-cuof/ActelDirectC27_CuOf
FILE=/nfshome0/venturas/Lorenzo/test-cuof/JTAG.dat
isgood=''

case "$1" in
    'YBm2')
	isgood=yes
	;;
    'YBm1')
	isgood=yes
	;;
    'YB0')
	isgood=yes
	;;
    'YBp1')
	isgood=yes
	;;
    'YBp2')
	isgood=yes
	;;
    *)
	break
	;;
esac

if  [ "$isgood" ]; then
    ${CANEXE} --wheels ${1} --actel ${ACTELEXE} verify ${FILE}
else
    echo "Bad crates name"
fi
