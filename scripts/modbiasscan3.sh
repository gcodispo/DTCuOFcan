#source /nfshome/dtdqm/.bashrc

if [ $# -lt 1 ]; then
    echo "parameters (all optional) are: wheel sector chamber channel"
    exit
fi
##WHEEL="$1"
##if [[ "$WHEEL" == "m2" ]]; then
##    W="-2"
##fi
##if [[ "$WHEEL" == "m1" ]]; then
##    W="-1"
##fi
##if [[ "$WHEEL" == "0" ]]; then
##    W="0"
##fi
##if [[ "$WHEEL" == "p1" ]]; then
##    W="1"
##fi
##if [[ "$WHEEL" == "p2" ]]; then
##    W="2"
##fi

##echo $WHEEL

##MB=$3
CH=$1
CH2=$2
##MEZZ=""
##if [ "$MB" != "" ]; then
##    let MEZZ=MB-1
##fi

MOD="40 50 60 70 80 90 100 110 120"
BIAS="16 18 22 26 30 34 38 42 46"
##MOD="50 60"
##BIAS="40"
rm modbiasscanlogp2.txt
rm modbiasscanlogp1.txt
rm modbiasscanlog0.txt
rm modbiasscanlogm1.txt
rm modbiasscanlogm2.txt
P=$PWD
for M in $MOD 
do
  for B in $BIAS
  do
##    SEC=$2
##    echo $SEC
##    echo "S$SEC MEZZ$MB MOD=$M BIAS=$B" >>modbiasscanlog$WHEEL.txt
      for WHEEL in m2 m1 0 p1 p2; do
##    for WHEEL in m2; do
	echo "CH= $CH MOD= $M BIAS= $B" >>modbiasscanlog$WHEEL.txt
	for SEC in 1 2 3 4 5 6 7 8 9 10 11 12; do
##	for SEC in 1 2 3 4; do
	    for MEZZ in 0 1 2 3; do
	        ../iocan --board YB"$WHEEL"_TRG"$SEC --writeonet $MEZZ $CH $M $B"	    
	        ../iocan --board YB"$WHEEL"_TRG"$SEC --writeonet $MEZZ $CH2 $M $B"	    
##		echo "../iocan --board YB\"$WHEEL\"_TRG\"$SEC --writeonet $MEZZ $CH $M $B\""
	    done
	done
      done

      ../iocan --board YBm2_TRG4-5 --writeonet 0 $CH $M $B
      ../iocan --board YBm2_TRG4-5 --writeonet 0 $CH2 $M $B
      ../iocan --board YBm2_TRG9-10 --writeonet 0 $CH $M $B
      ../iocan --board YBm2_TRG9-10 --writeonet 0 $CH2 $M $B
      ../iocan --board YBm1_TRG4-5 --writeonet 0 $CH $M $B
      ../iocan --board YBm1_TRG4-5 --writeonet 0 $CH2 $M $B
      ../iocan --board YBm1_TRG9-10 --writeonet 0 $CH $M $B
      ../iocan --board YBm1_TRG9-10 --writeonet 0 $CH2 $M $B
      ../iocan --board YB0_TRG4-5 --writeonet 0 $CH $M $B
      ../iocan --board YB0_TRG4-5 --writeonet 0 $CH2 $M $B
      ../iocan --board YB0_TRG9-10 --writeonet 0 $CH $M $B
      ../iocan --board YB0_TRG9-10 --writeonet 0 $CH2 $M $B
      ../iocan --board YBp1_TRG4-5 --writeonet 0 $CH $M $B
      ../iocan --board YBp1_TRG4-5 --writeonet 0 $CH2 $M $B
      ../iocan --board YBp1_TRG9-10 --writeonet 0 $CH $M $B
      ../iocan --board YBp1_TRG9-10 --writeonet 0 $CH2 $M $B
      ../iocan --board YBp2_TRG4-5 --writeonet 0 $CH $M $B
      ../iocan --board YBp2_TRG4-5 --writeonet 0 $CH2 $M $B
      ../iocan --board YBp2_TRG9-10 --writeonet 0 $CH $M $B
      ../iocan --board YBp2_TRG9-10 --writeonet 0 $CH2 $M $B

##    if [[ "$WHEEL" != "" ]]; then
##	if [[ "$SEC" != "" ]]; then
##	    ../iocan --board YB"$WHEEL"_TRG"$SEC --writeonet $MEZZ $CH $M $B"
##	    echo "iocan 1"
##	else
##	    for SEC in 1 2 3 4 5 6 7 8 9 10 11 12; do
##		../iocan --board YB"$WHEEL"_TRG$SEC --writeonet $M $B
##		echo "iocan 2"
##	    done
##	fi
##    else
##	../iocan --board trg --writeonet $M $B  
##	echo "iocan 3"
##    fi
    echo "Written VCSELs ch=$CH MOD=$M BIAS=$B" 
    sleep 5
    cd ~/dtupy/TM7_sw_repo/mp7/tests/
    . env.sh
    cd ~/dtupy/TM7_sw_repo/TM7/
    python write_parity_YB-2.py > test_m2.txt
    python write_parity_YB-1.py > test_m1.txt
    python write_parity_YB0.py > test_0.txt
    python write_parity_YB+1.py > test_p1.txt
    python write_parity_YB+2.py > test_p2.txt
    cd $P
    tail -n 100 ~/dtupy/TM7_sw_repo/TM7/test_m2.txt >> modbiasscanlogm2.txt
    tail -n 100 ~/dtupy/TM7_sw_repo/TM7/test_m1.txt >> modbiasscanlogm1.txt
    tail -n 100 ~/dtupy/TM7_sw_repo/TM7/test_0.txt >> modbiasscanlog0.txt
    tail -n 100 ~/dtupy/TM7_sw_repo/TM7/test_p1.txt >> modbiasscanlogp1.txt
    tail -n 100 ~/dtupy/TM7_sw_repo/TM7/test_p2.txt >> modbiasscanlogp2.txt

##    cd /nfshome0/dtdqm/tSC/rtsw_sc_manager_1.36/exe
##    ./sc_monitorx_64.exe &> /dev/null < testmon.txt
####    ./sc_monitorx.exe &> /dev/null < testmon.txt
##    echo "TSC status reset, sampling..."
##    sleep 5
####    ./sc_monitorx.exe &> /dev/null  < testmon.txt
##    ./sc_monitorx_64.exe &> /dev/null  < testmon.txt
##    cd $P
##    tail  -n 12  /nfshome0/dtdqm/tSC/rtsw_sc_manager_1.36/exe/monitor_wh"$W".txt | awk '{print $1,$2," | ",$32,$33,$34," ",$35,$36,$37," | ",$38,$39,$40," ",$41,$42,$43," | ",$44,$45,$46," ",$47,$48,$49," | ",$50,$51,$52," ",$53,$54,$55}' >> modbiasscanlog$WHEEL.txt
    echo "TSC stasus dumped"
##    cd $P
    #read -n1 key
  done
done
#../iocan --board YBp1_TRG$SEC --writeonet $MEZZ 110 25
#source vcselLG.sh
#cp modbiasscanlog$WHEEL.txt modbiasscanlog_W$WHEEL"S"$SEC"MB"$MB"CH"$CH".txt"
