#!/usr/bin/env python

import socket
import binascii

TCP_IP = "10.192.36.85"
TCP_PORT = 8001
BUFFER_SIZE = 20
text_in="A55A10007C060000050000002B21000300000000"
MESSAGE_IN=binascii.unhexlify(text_in)
print text_in


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(MESSAGE_IN)
MESSAGE_OUT = s.recv(BUFFER_SIZE)
s.close()

text_out=binascii.hexlify(MESSAGE_OUT)
print "received data:", text_out
