#source /nfshome/dtdqm/.bashrc

echo "it works only from ssh -Y dtdqm@srv-s2g16-28-01"


if [ $# -lt 1 ]; then
    echo "YOU NEED TO WRITE THE FOLLOWING PARAMETERS: wheel (-2..2) sector(1..12, 13, 14) chamber(1..4) rob(0..6) MOD BIAS"
    exit
fi

WHEELa="$1"
SECTOR="$2"
MB=$3
CH=$4
MOD=$5
BIAS=$6
#EQ=$7

if [[ "$WHEELa" == -2 ]]; then
    WHEEL="m2"
    W="-2"
fi
if [[ "$WHEELa" == -1 ]]; then
    WHEEL="m1"
    W="-1"
fi
if [[ "$WHEELa" == 0 ]]; then
    WHEEL="0"
    W="0"
fi
if [[ "$WHEELa" == 1 ]]; then
    WHEEL="p1"
    W="1"
fi
if [[ "$WHEELa" == 2 ]]; then
    WHEEL="p2"
    W="2"
fi

echo $WHEEL
echo $SECTOR
echo $MB
echo $CH

IOSEC=$SECTOR
IOMB=$(expr $MB - 1)


    if [ "$IOMB" == 3 ] && [ "$SECTOR" == 4 ] 
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=11
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=11
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=4
            IOCH=0
        elif [ "$CH" == 3 ]
        then
            IOSEC=4
            IOCH=1
        elif [ "$CH" == 4 ]
        then
            IOSEC=4
            IOCH=2
        fi
   elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 13 ]
   then
        if [ "$CH" == 0 ]
        then
            IOSEC=9
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=9
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=4
            IOCH=4
        elif [ "$CH" == 3 ]
        then
            IOSEC=4
            IOCH=5
        elif [ "$CH" == 4 ]
        then
            IOSEC=4
            IOCH=6
        fi
    elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 10 ]
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=10
            IOCH=4
        elif [ "$CH" == 1 ]
        then
            IOSEC=10
            IOCH=5
        elif [ "$CH" == 2 ]
        then
            IOSEC=10
            IOCH=6
        elif [ "$CH" == 3 ]
        then
            IOSEC=9
            IOCH=6
        fi
    elif  [ "$IOMB" == 3 ] && [ "$SECTOR" == 14 ]
    then
        if [ "$CH" == 0 ]
        then
            IOSEC=10
            IOCH=0
        elif [ "$CH" == 1 ]
        then
            IOSEC=10
            IOCH=1
        elif [ "$CH" == 2 ]
        then
            IOSEC=10
            IOCH=2
        elif [ "$CH" == 3 ]
        then
            IOSEC=11
            IOCH=6
        fi
     fi



#si es una MB3 el mapping de CUOF es diferente
    if [ "$IOMB" == 2 ]
    then
      IOCH=$(expr 7 - $CH)
            echo "el canal para esa rob es $IOCH. esto es para una MB3"
    elif [[ "$CH" < 3 ]]
    then
            IOCH=$(expr 7 - $CH)
            echo "el canal para esa rob es $IOCH. No es una mb3, rob menor q 3"
    else
            IOCH=$(expr 7 - $(expr $CH + 1))
            echo "el canal para esa rob es $IOCH.  No es una mb3, rob mayor q 3"
    fi

echo "ooooooooooooooo  yb  $WHEEL sec $IOSEC cuofmezz $IOMB  cuofch $IOCH"
echo "the choosen chamber is : $MB, for IOCAN it is: $IOMB"
echo "the sector choosen is : $SECTOR, for IOCAN is: $IOSEC"
echo "the choosen rob is : $CH, for IOCAN it is: $IOCH"


source ./../dtcanenv.sh



cd /nfshome0/dtdqm/TestCan/scripts/
../iocan --board YB"$WHEEL"_RO"$IOSEC --writeonet $IOMB $IOCH $MOD $BIAS"	    
echo "Written VCSELs ch=$CH MOD=$MOD BIAS=$BIAS" 

